# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package My::Test::Utils;

use strict;
use warnings;

use ExtUtils::Manifest;
use File::Find;

sub all_files {

   my $regexp = shift // qr!^\W*(.*)$!;
   my @files  = ();

   if (-f -r 'MANIFEST') {
      for (keys %{ ExtUtils::Manifest::maniread () }) {
         -f && -r && $_ =~ $regexp && push @files, $1;
      }
   }
   else {
      File::Find::find (
         {
            'no_chdir' => 1,
            'wanted'   => sub { -f && -r && $_ =~ $regexp && push @files, $1 },
         },
         '.'
      );
   }

   return @files;

}

sub all_modules {
   return map { $_ =~ s/\W+/::/g; $_ } all_files (qr/^\W*lib\W+(.+)\.pm$/i);
}

1;

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
