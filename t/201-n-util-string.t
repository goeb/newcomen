#!/usr/bin/perl -T

# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

use strict;
use warnings;

use Test::More;
use Newcomen::Util::String qw( split_ssv );

BEGIN {

   eval {
      require Test::LectroTest::Compat;
      require Test::LectroTest::Generator;
      require Test::LectroTest::Property;
   };

   if ($@) {
      plan ('skip_all' => 'Test::LectroTest required.');
   }
   else {
      import Test::LectroTest::Compat;
      import Test::LectroTest::Generator qw( :common );
      import Test::LectroTest::Property qw( NO_FILTER );
   }

}

plan ('tests' => 2);

my $p1 = Test::LectroTest::Property -> new (
   'name'   => 'split_ssv (slashes)',
   'inputs' => [
      'x'   => String ('length' => [3,]),
      'y'   => String ('length' => [3,])
   ],
   'test'   => sub {
      my ($t, $x, $y) = @_;
      my $z = $x . '/' . $y;
      return $t -> retry () if $z =~ m!\A/+\z!;
      my @p = split_ssv ($z);
      $z =~ s!/+!/!g;
      $z =~ s!(?:\A/|/\z)!!g;
      my $c = () = $z =~ m!/!g;
      ($z eq join '/', @p) and (scalar @p == $c + 1) and not grep { m!/! } @p;
   },
);

my $p2 = Test::LectroTest::Property -> new (
   'name'   => 'split_ssv (no slashes)',
   'inputs' => [ 'x' => String ('length' => [1,]) ],
   'test'   => sub {
      my ($t, $x) = @_;
      return $t -> retry () if $x =~ m!/!;
      my @p = split_ssv ($x);
      (scalar @p == 1) and ($p [0] eq $x);
   },
);

holds ($p1, 'trials' => 500);
holds ($p2, 'trials' => 500);

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
