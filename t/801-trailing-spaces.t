#!/usr/bin/perl -T

# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

use strict;
use warnings;
use lib 't/lib';

use My::Test::Utils;
use Test::More;

BEGIN {

   eval { require Test::EOL };

   if ($@ or not $ENV {'TEST_AUTHOR'}) {
      plan ('skip_all' => 'Test::EOL and TEST_AUTHOR=1 required.');
   }
   else {
      import Test::EOL;
   }

}

my @files = My::Test::Utils::all_files (
   qr/^\W*((?:lib|pod|t)\W+.*\.(?:pl|pm|pod|t|txt)|bin\W+.*|utils\W+[^.]+)$/i
);

plan ('tests' => scalar @files);
eol_unix_ok ($_, "File endings in $_", { 'trailing_whitespace' => 1 }) for @files;

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
