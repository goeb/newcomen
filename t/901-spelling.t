#!/usr/bin/perl

# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

# No spellchecker will be found in taint mode!

use strict;
use warnings;

use Test::More;

BEGIN {

   eval  { require Test::Spelling };

   if ($@ or not $ENV {'TEST_AUTHOR'}) {
      plan ('skip_all' => 'Test::Spelling and TEST_AUTHOR=1 required.');
   }
   else {
      import Test::Spelling;
   }

}

add_stopwords (qw(
   arrayref arrayrefs
   autoclean
   backend backend's
   basenames
   Bitbucket
   changenotify
   checkmanifest
   cjm
   cpan
   dir dirs
   eol
   formatter formatters
   getter
   Gitlab
   Goebel
   hashref hashref's hashrefs
   HookOrder
   iso
   lectrotest
   libcapture  libclone  libdata    libdigest     libdist  libfile      libmodule
   libmonkey   libmoose  libmoosex  libnamespace  libpath  libtemplate  libtest
   libtext     libuuid   libyaml
   mailto
   MERCHANTABILITY
   MultiMarkdown multimarkdown
   namespace
   nonmoose
   notabs
   nowarnings
   parameterized
   renderer renderers
   rmap
   SHA sha
   strictconstructor
   Subdirectories subdirectories
   subtype de
   Toolkit's
   unclosed
   UUID UUIDs uuid
   URL's url
   UTF utf
   NEWCOMEN Newcomen Newcomen's newcomen
   YAML
   ymd
   zilla
));

all_pod_files_spelling_ok ('bin', 'lib', 'pod', 'utils');

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
