#!/usr/bin/perl -T

# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

use strict;
use warnings;
use lib 't/lib';

use My::Test::Utils;
use Test::More;
use Test::NoWarnings;

BEGIN {

   my @mods = My::Test::Utils::all_modules ();

   plan ('tests' => @mods + 3);

   use_ok ('Newcomen', 2017040102);
   use_ok ($_) for (@mods);

}

is   ($Newcomen::VERSION, 2017040102, '$Newcomen::VERSION ok');
diag ("Testing Newcomen $Newcomen::VERSION, Perl $], $^X");

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
