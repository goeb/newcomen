#!/usr/bin/perl -T

# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

use strict;
use warnings;

use Test::More;

BEGIN {

   eval { require Test::Pod::Spelling::CommonMistakes };

   if ($@ or not $ENV {'TEST_AUTHOR'}) {
      plan ('skip_all' => 'Test::Pod::Spelling::CommonMistakes and TEST_AUTHOR=1 required.');
   }
   else {
      import Test::Pod::Spelling::CommonMistakes;
   }

}

all_pod_files_ok ();

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
