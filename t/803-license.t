#!/usr/bin/perl -T

# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

use strict;
use warnings;
use lib 't/lib';

use Fcntl 'O_RDONLY';
use My::Test::Utils;
use Test::More;
use Tie::File;

BEGIN {

   if (not $ENV {'TEST_AUTHOR'}) {
      plan ('skip_all' => 'TEST_AUTHOR=1 required.');
   }

}

my $pm_header;
my $pm_footer_1;
my $pm_footer_2;

my @mods_n_pods = My::Test::Utils::all_files ( qr/^\W*((lib|pod)\W+.+\.(pm|pod|tpl))$/i    );
my @tests       = My::Test::Utils::all_files ( qr/^\W*(t\W+.+\.t)$/i                       );
my @test_mods   = My::Test::Utils::all_files ( qr/^\W*(t\W+.+\.pm)$/i                      );
my @scripts     = My::Test::Utils::all_files ( qr/^\W*(bin\Wnewcomen)$/i                   );
my @utils       = My::Test::Utils::all_files ( qr/^\W*(utils\W+(pod2html|newcomen\..+))$/i );
my @configs     = My::Test::Utils::all_files ( qr/^\W*(intro\W+config\W+config\.pl)$/i     );

plan (
   'tests' => 5 * (@mods_n_pods +  @scripts) + 2 * (@tests + @utils + @test_mods) + 3 * @configs
);

my @content;

for (@mods_n_pods) {

   tie (@content, 'Tie::File', $_, 'mode' => O_RDONLY, 'autochomp' => 0)
      or die "Could not read file $_: $!";

   like ($content [  0], qr/^# Copyright 20\d\d((-|, )20\d\d)* Stefan Goebel.\n$/, "Module © ok: $_");
   like ($content [-22], qr/^Copyright 20\d\d((-|, )20\d\d)* Stefan Goebel.\n$/,   "Module © ok: $_");

   is (join ('', @content [  1 ..  13]), $pm_header,   "Module header ok: $_");
   is (join ('', @content [-28 .. -23]), $pm_footer_1, "Module footer ok: $_");
   is (join ('', @content [-21 .. -1 ]), $pm_footer_2, "Module footer ok: $_");

}

for (@scripts) {

   tie (@content, 'Tie::File', $_, 'mode' => O_RDONLY, 'autochomp' => 0)
      or die "Could not read file $_: $!";

   like ($content [  2], qr/^# Copyright 20\d\d((-|, )20\d\d)* Stefan Goebel.\n$/, "Script © ok: $_");
   like ($content [-22], qr/^Copyright 20\d\d((-|, )20\d\d)* Stefan Goebel.\n$/,   "Script © ok: $_");

   is (join ('', @content [  3 ..  15]), $pm_header,   "Module header ok: $_");
   is (join ('', @content [-28 .. -23]), $pm_footer_1, "Module footer ok: $_");
   is (join ('', @content [-21 .. -1 ]), $pm_footer_2, "Module footer ok: $_");

}

for (@tests, @utils) {

   tie (@content, 'Tie::File', $_, 'mode' => O_RDONLY, 'autochomp' => 0)
      or die "Could not read file $_: $!";

   like ($content [2], qr/^# Copyright 20\d\d((-|, )20\d\d)* Stefan Goebel.\n$/, "Test/Util © ok: $_");

   is (join ('', @content [3 .. 15]), $pm_header, "Test or util header ok: $_");

}

for (@test_mods) {

   tie (@content, 'Tie::File', $_, 'mode' => O_RDONLY, 'autochomp' => 0)
      or die "Could not read file $_: $!";

   like ($content [0], qr/^# Copyright 20\d\d((-|, )20\d\d)* Stefan Goebel.\n$/, "Module © ok: $_");

   is (join ('', @content [  1 .. 13]), $pm_header, "Test module header ok: $_");

}

for (@configs) {

   tie (@content, 'Tie::File', $_, 'mode' => O_RDONLY, 'autochomp' => 0)
      or die "Could not read file $_: $!";

   like ($content [-22], qr/^Copyright 20\d\d((-|, )20\d\d)* Stefan Goebel.\n$/, "Config © ok: $_");

   is (join ('', @content [-28 .. -23]), $pm_footer_1, "Config footer ok: $_");
   is (join ('', @content [-21 .. -1 ]), $pm_footer_2, "Config footer ok: $_");

}

BEGIN {

$pm_header = <<__EOT__;
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.
__EOT__

$pm_footer_1 = <<__EOT__;
=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

__EOT__

$pm_footer_2 = <<__EOT__;

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
__EOT__

}

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
