use utf8;

{

'plugins'                  => {
   'Blog'                  => 1,
   'CleanDirectories'      => 1,
   'Rewrite::Clean'        => 1,
   'Static'                => 1,
   'Newcomen'              => 1,
},

'blog'                     => {

   'crawler'               => {
      'dirs'               => 'articles',
      'match'              => '\.(?:txt|mmd)$',
   },

   'defaults'              => {
      'renderer'           => 'TemplateToolkit',
      'writer'             => 'Rendered',
   },

   'excerpt'               => {
      'excerpt'            => ['<!--excerpt-->', '<!--/excerpt-->'],
   },

   'formatters'            => {
      '\.mmd$'             => ['MultiMarkdown', 'RelativePaths'],
      '\.txt$'             => [                 'RelativePaths'],
   },

   'single'                => {
      'url'                => '<categories/0>/<slug>.html',
      'page_info'          => {
         'title'           => '<title>',
      },
   },

   'source'                => {
      'filename_meta'      => 'articles/(?:(?<category>.*)?/)?(?:\d+-)?(?<slug>.*)\.(?:txt|mmd)$',
      'lists'              => {
         'category'        => 'categories',
         'tag'             => 'tags',
      },
      'time'               => {
         'input_format'    => '%Y-%m-%d %H:%M',
         'output_format'   => '%Y-%m-%d %H:%M',
      },
   },

   'menu'                  => {
      '000_Index'          => {
         'url'             => 'index.html',
         'text'            => 'Blog Index',
      },
   },

   'index'                 => {
      'exclude'            => {
         'on_index'        => '^no$',
      },
      'main'               => {
         'exclude'         => {
            'on_main'      => '^no$',
         },
         'page_info'       => {
            'title'        => 'Blog index, page <page> of <pages>',
         },
         'index'           => {
            'url'          => 'index.html',
         },
         'pages'           => {
            'url'          => 'page-<page>/index.html',
         }
      },
      'lists'              => {
         'categories'      => {
            'page_info'    => {
               'title'     => 'Category: <list_item>, page <page> of <pages>',
            },
            'index'        => {
               'url'       => '<list_path>/index.html',
            },
            'pages'        => {
               'url'       => '<list_path>/page-<page>/index.html',
            },
         },
         'tags'            => {
            'hierarchical' => 0,
            'page_info'    => {
               'title'     => 'Tag: <list_item>, page <page> of <pages>',
            },
            'index'        => {
               'url'       => 'tag/<list_path>/index.html',
            },
            'pages'        => {
               'url'       => 'tag/<list_path>/page-<page>/index.html',
            },
         },
         'archive'         => {
            'source_list'  => 'time/lists/day',
            'page_info'    => {
               'title'     => 'Archive: <ymd>, page <page> of <pages>',
            },
            'index'        => {
               'url'       => 'archive/<ymd>/index.html',
            },
            'pages'        => {
               'url'       => 'archive/<ymd>/page-<page>/index.html',
            },
         },
      },
   },

   'feed'                  => {
      'defaults'           => {
         'uuid_ns'         => 'ac1522b0-4fbb-4712-a73f-b558e5c27334',
         'feed_info'       => {
            'author'       => 'Some Author',
            'base'         => 'http://example.com/',
         },
         'renderer'        => {
            'name'         => 'TemplateToolkit',
            'options'      => {
               'template'  => 'atom.tt',
            },
         },
      },
      'exclude'            => {
         'on_feed'         => '^no$',
      },
      'main'               => {
         'url'             => 'index.atom',
         'feed_info'       => {
            'title'        => "The blog's main feed.",
         },
      },
      'lists'              => {
         'categories'      => {
            'url'          => '<list_path>/index.atom',
            'feed_info'    => {
               'title'     => 'The feed for category <list_item>',
            },
         },
         'tags'            => {
            'url'          => 'tag/<list_path>/index.atom',
            'feed_info'    => {
               'title'     => 'The feed for tag <list_item>',
            },
         },
      },
   },

},

'cleandirectories'         => {
   'dirs'                  => 'output',
   'verbose'               => 0,
},

'formatter'                => {
   'use_cache'             => 1,
},

'static'                   => {
   'match'                 => '.',
   'dirs'                  => 'static',
},

'renderer'                 => {
   'templatetoolkit'       => {
      'template'           => 'default.tt',
      'COMPILE_DIR'        => 'cache/tt',
      'COMPILE_EXT'        => '.ctt',
      'ENCODING'           => 'utf8',
      'INCLUDE_PATH'       => 'templates',
      'RECURSION'          => 1,
   },
},

'writer'                   => {
   'output_dir'            => 'output',
},

'custom'                   => {
   'title'                 => 'An example blog',
   'subtitle'              => '...powered by Newcomen',
},

};

####################################################################################################

=head1 NAME

config.pl - Example configuration file for Newcomen.

=head1 DESCRIPTION

The example project this configuration belongs to is part of the B<Newcomen> distribution. The
project may be used as a starting point to create a simple blog page. It is located in the F<intro>
folder of the distribution.

This configuration file is suitable for use with the L<Newcomen::Config::Perl> configuration
backend (currently the only existing configuration backend).

In this documentation, nested data structures may be referenced by paths. For example, given the
following hashref:

   {
      'foo' => {
         'bar' => 'something',
      },
   }

the value of I<foo/bar> is C<'something'>. The paths used may be relative to the root of the
configuration hashref or relative to some sub-hashref, this will be mentioned in the specific
sections of this documentation.

=head1 OPTIONS

The following is a description of the options used in the example configuration file distributed
with B<Newcomen>. It contains basic information about the settings used, for more detailed
information please follow the links to the plugin documentation.

=head2 Pragmas

   #-#
   use utf8;

Usually no pragmas are required. However, if your configuration file contains UTF-8 characters, the
I<utf8> pragma has to be enabled. It is included by default, though the default configuration only
contains plain ASCII text.

=head2 Configuration Hashref

This configuration file has to return a hashref (see L<Newcomen::Config::Perl>). All the following
options are included in one single hashref, and this hashref has to be the last statement in the
file, so it will be returned when the file is processed (no explicit C<return> is necessary). The
following opening brace starts the hashref:

   #-#
   {

The hashref contains all the following options until the closing brace.

B<Note:> It is probably easier to see the actual nesting/structure of the options in the actual
configuration file.

=head2 Enabling Plugins

   #-#
   'plugins'                  => {
      'Blog'                  => 1,
      'CleanDirectories'      => 1,
      'Rewrite::Clean'        => 1,
      'Static'                => 1,
      'Newcomen'              => 1,
   },

Every plugin has to be enabled if it should be used for processing a project, see
L<Newcomen::Manual::Configuration>. The example configuration enables the plugins
L<Newcomen::Plugin::Blog> (which in turn enables all the other blog plugins),
L<Newcomen::Plugin::CleanDirectories>, L<Newcomen::Plugin::Rewrite::Clean>,
L<Newcomen::Plugin::Static> and L<Newcomen::Plugin::Newcomen>.

To enable a plugin, specify its basename (i.e. the module name without the C<'Newcomen::Plugin::'>
prefix) as the key in the I<plugins> hashref. Every true value will enable it, every false value
will disable it (this is the default).

B<Note:> Do not disable the L<Newcomen::Plugin::Newcomen> plugin!

=head2 Blog Configuration

The configuration of all the different blog plugins is specified in the I<blog> hashref. Usually,
the plugin name matches the child hashref, e.g. the configuration for the crawler plugin
(L<Newcomen::Plugin::Blog::Crawler>) is set in the hashref I<blog/crawler>.

The following starts the blog options:

   #-#
   'blog'                     => {

From now on, all options are relative to I<blog>, unless mentioned otherwise, until the I<blog>
section is closed.

=head3 Crawler Options

   #-#
      'crawler'               => {
         'dirs'               => 'articles',
         'match'              => '\.(?:txt|mmd)$',
      },

These are options for the L<Newcomen::Plugin::Blog::Crawler> plugin. By default, the crawler will
search for article sources in the directory F<articles> (specified relative to the project's root
directory). All files with the extension C<'.txt'> and C<'.mmd'> will be treated as article source
files.

B<Note:> A description of the source file format can be found in the documentation of the
L<parse_file()|Newcomen::Util::File/parse_file> function of L<Newcomen::Util::File>.

=head3 Backend Defaults

   #-#
      'defaults'              => {
         'renderer'           => 'TemplateToolkit',
         'writer'             => 'Rendered',
      },

These options are used by the L<Newcomen::Plugin::Blog::Defaults> plugin to set default backends for
the pages. By default, L<Newcomen::Renderer::TemplateToolkit> will be used to render a page, and
L<Newcomen::Writer::Rendered> will be used to write a page to disk. No backend options are set, so
the default options will be used, some defaults are set below. Also check the documentation of the
backends for additional information.

=head3 Article Excerpts

   #-#
      'excerpt'               => {
         'excerpt'            => ['<!--excerpt-->', '<!--/excerpt-->'],
      },

Used by the L<Newcomen::Plugin::Blog::Excerpt> plugin. Keys in this hashref define the name of the
excerpt's key in the source's meta data. The values are arrayrefs: the first element specifies the
start of the excerpt text in the article source, the second element the end of the excerpt.

In the example, all text between the two strings C<< '<!--excerpt-->' >> and
C<< '<!--/excerpt-->' >> will be available in the source's meta data under the key I<excerpt>.

B<Note:> For content formatted with the L<Newcomen::Formatter::MultiMarkdown> formatter, the stop
marker should be put in its own paragraph if it is used without a start marker. The excerpt will be
extracted B<after> the formatting, this may result in unclosed and/or unopened tags in the excerpt.

=head3 Formatters

   #-#
      'formatters'            => {
         '\.mmd$'             => ['MultiMarkdown', 'RelativePaths'],
         '\.txt$'             => [                 'RelativePaths'],
      },

The L<Newcomen::Plugin::Blog::Formatters> plugin assigns formatter backends to source file content
based on the configuration in this hashref. The keys in the hashref will be used in a regular
expression. If it matches a source file's name, the formatter backend (or backends) specified by the
value in the hashref will be used for the source.

In the example, all articles in files with the C<'.mmd'> extension will be formatted by the
L<Newcomen::Formatter::MultiMarkdown> formatter backend first, and after that by the
L<Newcomen::Formatter::RelativePaths> formatter (allowing you to reference other pages by a relative
path in article sources). The latter will also be applied to all files with the C<'.txt'> extension
(see above for the L<crawler configuration|/Crawler Options>).

B<Note:> All regular expressions will be checked for a given piece of content. If more than one
matches, all specified formatters will be applied! Make sure the regular expressions are as specific
as possible if this is not intended. If multiple formatters should be used, note that the order in
which they are applied is determined by sorting the regular expressions (using Perl's standard
I<sort()> function).

=head3 Single Article Pages

   #-#
      'single'                => {
         'url'                => '<categories/0>/<slug>.html',
         'page_info'          => {
            'title'           => '<title>',
         },
      },

These options are used by the L<Newcomen::Plugin::Blog::Single> plugin. The I<url> setting is
mandatory, if this is empty, no single article pages will be created. In the example configuration,
single article pages will be placed in a directory named after the article's first category (this
may be empty if none is specified), the file name will be the article slug (see
L<Newcomen::Plugin::Blog::Source::Slug>). Please see the L<Newcomen::Plugin::Blog::Single>
documentation for available placeholders.

The I<page_info> hashref contains arbitrary data to be included in the page's meta data. By default,
the I<title> option is defined, which will add the meta data I<info_title> to the single article
pages, and its value will be set to the I<title> found in the sources' meta data. The same
placeholders as for the I<url> option may be used.

=head3 Article Sources

  #-#
     'source'                => {
        'filename_meta'      => 'articles/(?:(?<category>.*)?/)?(?:\d+-)?(?<slug>.*)\.(?:txt|mmd)$',
        'lists'              => {
           'category'        => 'categories',
           'tag'             => 'tags',
        },
        'time'               => {
           'input_format'    => '%Y-%m-%d %H:%M',
           'output_format'   => '%Y-%m-%d %H:%M',
        },
     },

The L<Newcomen::Plugin::Blog::Source> plugin uses the I<filename_meta> option to add meta data to
the article sources based on the file names. Perl's named capture groups are used for this, please
see L<perlre>. The example configuration creates I<category> meta data based on the directory, and
the I<slug> will be set based on the file name. The file name may start with digits followed by a
hyphen, this prefix will be ignored. The extension will also be ignored. Note that the extension is
included in the regular expression, if you change the L<crawler settings|/Crawler Options>, you will
also need to modify the I<filename_meta> option accordingly. Also included is the article source
directory (C<'articles/'>), also set in the L<crawler settings|/Crawler Options>.

I<source/lists> will be used by the L<Newcomen::Plugin::Blog::Source::Lists> plugin to create lists
in the sources' meta data. In the example, the meta data I<category> and I<tag> will be used to
create the lists I<categories> and I<tags>, respectively. To create the lists, the original data
will simply be split on commas (spaces around commas are allowed). These lists are required to
create the category and tag index pages (see L<below|/Categories, Tags And Archive Pages>).

The hashref I<source/time> contains options used by L<Newcomen::Plugin::Blog::Source::Time> to parse
the I<published> and I<modified> meta data if available for an article (i.e. included in the article
source). The I<input_format> option will be used for parsing, the I<output_format> option will be
used to reformat the parsed time. See the plugin's documentation for more details.

=head3 Menu Configuration

   #-#
      'menu'                  => {
         '000_Index'          => {
            'url'             => 'index.html',
            'text'            => 'Blog Index',
         },
      },

Defines a menu entry for the blog's start page. See L<Newcomen::Plugin::Blog::Menu> for more details
on defining menus.

=head3 Index Pages

   #-#
      'index'                 => {

The options for all the index plugins are included in the I<index> hashref. The following options
are relative to this hashref.

=head4 Excluding Articles

   #-#
         'exclude'            => {
            'on_index'        => '^no$',
         },

The L<Newcomen::Plugin::Blog::Index::Exclude> plugin may be used to remove articles from B<all>
index pages, based on arbitrary meta data. In the example configuration, all articles with the meta
data I<on_index> set to C<'no'> will not be listed on any index page.

=head4 The Main Index

   #-#
         'main'               => {
            'exclude'         => {
               'on_main'      => '^no$',
            },
            'page_info'       => {
               'title'        => 'Blog index, page <page> of <pages>',
            },
            'index'           => {
               'url'          => 'index.html',
            },
            'pages'           => {
               'url'          => 'page-<page>/index.html',
            }
         },

The blog's main index is created by the L<Newcomen::Plugin::Blog::Index::Main> plugin based on these
options. I<main/page_info> works like the I<page_info> option of the single article pages, see
L<above|/Single Article Pages> (and check the L<Newcomen::Plugin::Blog::Index::Main> documentation
for available placeholders). I<main/index> includes settings for the first index page, I<main/pages>
for all subsequent pages. In the example, only the URLs for the index pages are set. The
L<Newcomen::Plugin::Blog::Index::Main::Exclude> plugin will be used to remove all posts with the
meta data I<on_main> set to C<'no'> from the main index page (this does not affect the list index
pages, e.g. the category and date listings).

=head4 Categories, Tags And Archive Pages

   #-#
         'lists'              => {
            'categories'      => {
               'page_info'    => {
                  'title'     => 'Category: <list_item>, page <page> of <pages>',
               },
               'index'        => {
                  'url'       => '<list_path>/index.html',
               },
               'pages'        => {
                  'url'       => '<list_path>/page-<page>/index.html',
               },
            },
            'tags'            => {
               'hierarchical' => 0,
               'page_info'    => {
                  'title'     => 'Tag: <list_item>, page <page> of <pages>',
               },
               'index'        => {
                  'url'       => 'tag/<list_path>/index.html',
               },
               'pages'        => {
                  'url'       => 'tag/<list_path>/page-<page>/index.html',
               },
            },
            'archive'         => {
               'source_list'  => 'time/lists/day',
               'page_info'    => {
                  'title'     => 'Archive: <ymd>, page <page> of <pages>',
               },
               'index'        => {
                  'url'       => 'archive/<ymd>/index.html',
               },
               'pages'        => {
                  'url'       => 'archive/<ymd>/page-<page>/index.html',
               },
            },
         },

These example options will result in index pages for categories, tags, and daily/monthly/yearly
archives. The pages will be created by the L<Newcomen::Plugin::Blog::Index::Lists> plugin. The
options are similar to the options for the main index page (see L<above|/The Main Index>). For index
pages to be created, every article source must include appropriate meta data. In the example, the
meta data I<categories>, I<tags> and I<time/lists/day> will be used, determined by the key names in
the hashref (for I<lists/categories> and I<lists/tags>), unless overridden by a I<source_list>
option (in I<lists/archive>). This meta data must be a list (arrayref), or the article will not be
included. The I<blog/source/lists> example options (see L<above|/Article Sources>) will cause these
lists for the categories and tags to be created automatically from I<category> and I<tag> meta data.
The I<time/lists/day> list will be created by the L<Newcomen::Plugin::Blog::Index::Lists::Date>
plugin automatically. Please see the documentation of the different plugins for more details and
additional options.

=head4 End Of The Index Configuration

   #-#
      },

This closing brace marks the end of the I<blog/index> hashref.

=head3 Atom Feeds

   #-#
      'feed'                  => {

The options for all the feed plugins are included in the I<feed> hashref. The following options
are relative to this hashref.

=head4 Feed Defaults

   #-#
         'defaults'           => {
            'uuid_ns'         => 'ac1522b0-4fbb-4712-a73f-b558e5c27334',
            'feed_info'       => {
               'author'       => 'Some Author',
               'base'         => 'http://example.com/',
            },
            'renderer'        => {
               'name'         => 'TemplateToolkit',
               'options'      => {
                  'template'  => 'atom.tt',
               },
            },
         },

These are some default options used by feed plugins. The I<renderer> setting will be used by the
L<Newcomen::Plugin::Blog::Feed::Defaults> plugin to set the renderer options for feed pages. The
template F<atom.tt> will be used to render the pages. For more options see the Template Toolkit
specific renderer defaults below.

The I<uuid_ns> options is used by several feed plugins to create a unique UUID when required.
B<Important:> Please change this setting to a unique value for your project before publishing any
feeds, do not use the example value! This has to be a valid UUID.

The I<feed_info> hashref contains additional meta data to be included on the feed pages. In the
example an author name and the base URL of the feed are defined. Check the F<atom.tt> template to
see how these values are used in the template.

B<Note:> I<feed_info/base> is used in the example template as C<xml:base> value. Links in the
generated feed will probably not work until you set this to the proper value for your website!

=head4 Exclude Articles From Feeds

   #-#
         'exclude'            => {
            'on_feed'         => '^no$',
         },

The L<Newcomen::Plugin::Blog::Feed::Exclude> plugin may be used to remove articles from B<all>
feeds, based on arbitrary meta data. In the example configuration, all articles with the meta
data I<on_feed> set to C<'no'> will not be included in the generated feeds.

=head4 The Main Feed

   #-#
         'main'               => {
            'url'             => 'index.atom',
            'feed_info'       => {
               'title'        => "The blog's main feed.",
            },
         },

Setting for the main feed, i.e. the feed that belongs to the main index page. This feed will be
created by the plugin L<Newcomen::Plugin::Blog::Feed::Main>. In the example, the feed's URL will be
set to F<index.atom>, and a title will be set to be used in the templates. The I<feed_info> option
works like the I<page_info> options for index pages, see L<above|/The Main Index>.

=head4 Category And Tag Feeds

   #-#
         'lists'              => {
            'categories'      => {
               'url'          => '<list_path>/index.atom',
               'feed_info'    => {
                  'title'     => 'The feed for category <list_item>',
               },
            },
            'tags'            => {
               'url'          => 'tag/<list_path>/index.atom',
               'feed_info'    => {
                  'title'     => 'The feed for tag <list_item>',
               },
            },
         },

These settings will cause the creation of Atom feeds for category and tag index pages by the plugin
L<Newcomen::Plugin::Blog::Feed::Lists>. The keys in the I<lists> hashref must be the same keys used
in the I<blog/index/lists> hashref to create a feed for these specific index pages, see
L<above|/Categories, Tags And Archive Pages>. Again, the I<feed_info> option works like the
I<page_info> options for index pages, see L<above|/The Main Index>. Only the feeds' URLs and titles
are set in the example.

=head4 End Of The Feed Configuration

   #-#
      },

This closing brace marks the end of the I<blog/feed> hashref.

=head3 End Of The Blog Configuration

   #-#
   },

This closing brace marks the end of the I<blog> hashref, all stuff that follows is again at the top
level of the main configuration hashref.

=head2 Automatically Clean The Output Directory

   #-#
   'cleandirectories'         => {
      'dirs'                  => 'output',
      'verbose'               => 0,
   },

The L<Newcomen::Plugin::CleanDirectories> plugin may be used to automatically empty one or more
directories before the build process. In the example, the F<output> directory (specified relative to
the project's root directory, also check the L<writer options|/Default Writer Options>) will be
emptied. The I<verbose> setting is disabled in the example, since it would interfere with the I<-m>
command line option of the I<newcomen> script. If you don't use this (or even if you use it and
don't care), you may enable it.

B<Warning:> All contents of the specified directory will be deleted!

=head2 Default Formatter Options

   #-#
   'formatter'                => {
      'use_cache'             => 1,
   },

Formatter default options may be specified in the I<formatter> hashref. The only option set in the
example is I<use_cache>. This is not an option for a specific formatter backend, it only tells the
formatter frontend (see L<Newcomen::Formatter>) to use caching for formatter results. This is the
default behaviour. It is included in the example so it can easily be changed in case it causes any
problems.

=head2 Copy Static Files To The Output Directory

   #-#
   'static'                   => {
      'match'                 => '.',
      'dirs'                  => 'static',
   },

The L<Newcomen::Plugin::Static> plugin prepares static files from one or more directories to be
copied to the output directory. The I<dirs> option in the example sets the source directory to the
F<static> directory (relative to the project's root directory). The match option will be used in a
regular expression. Every matching file will be included. In the example configuration, all files
will be included.

=head2 Default Renderer Options

   #-#
   'renderer'                 => {
      'templatetoolkit'       => {
         'template'           => 'default.tt',
         'COMPILE_DIR'        => 'cache/tt',
         'COMPILE_EXT'        => '.ctt',
         'ENCODING'           => 'utf8',
         'INCLUDE_PATH'       => 'templates',
         'RECURSION'          => 1,
      },
   },

Renderer options are set in the I<renderer> hashref. Currently, only one renderer backend is
available: L<Newcomen::Renderer::TemplateToolkit>, with its options specified in the
I<renderer/templatetoolkit> hashref. Most of the options are L<Template Toolkit|Template> options,
with the exception of the I<template> option. In the example configuration, the template directory
is set to F<templates>, and templates will be cached in the directory F<cache/tt> (all paths are
relative to the project's root directory). The template to be used by default is F<default.tt>. It
has to be UTF-8 encoded. In the example project recursive processing of the templates is allowed
since this is required for the nested menu structure to be rendered.

=head2 Default Writer Options

   #-#
   'writer'                   => {
      'output_dir'            => 'output',
   },

Options for the writer backends may be included in this hashref. Every backend may support different
options, please see individual backends for details if required. In the example, only the
I<output_dir> option is set. This specifies the directory to which all output files will be written,
relative to the project's root directory. All writer backends currently available will respect this
option.

=head2 Custom Options

   #-#
   'custom'                   => {
      'title'                 => 'An example blog',
      'subtitle'              => '...powered by Newcomen',
   },

These options are not used by any plugin. You may rename I<custom> to anything you like, or even
include options at the top level of the configuration hashref, as long as these options do not clash
with plugin options (it's probably the best to keep them in I<custom>). You can access every option
in the templates, so things like a site's title and subtitle may be declared here (as in the
example) to allow these to be changed easily without touching the template itself.

=head2 End Of The Configuration Hashref

   #-#
   };

That's all, folks!

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
