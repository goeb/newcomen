title     : More On Article Sources, And Relative Paths
published : 2014-02-01 22:00

---

<p>
   The source file of this article is a <code>.txt</code> file. It will be included in the blog like
   the <code>.mmd</code> files, and uses the exact same
   <a href="{rel{Blog::Single,articles/001-article-sources.mmd}}">format</a>. However, the
   <a href="http://fletcherpenney.net/multimarkdown/">MultiMarkdown</a> formatter will not be
   applied to this article’s content.
</p>

<p>
   Which formatters are applied depends on the configuration. In the example configuration, the
   MultiMarkdown formatter will be applied to <code>.mmd</code> files only. On the other hand, the
   <code>RelativePaths</code> formatter will be applied to all articles:
</p>

<p>
   This is the path of the <a href="{rel{Blog::Single,articles/999-static-page.mmd}}">“Static
   Page”</a> (as linked in the menu on the left), relative to the current page:
</p>

<pre><code>{rel{Blog::Single,articles/999-static-page.mmd}}</code></pre>

<p>
   This relative path will change depending on the page this article is included on. For example, it
   will be different on <a href="{rel{Blog::Index::Lists,archive,2014,1}}">yearly</a> and
   <a href="{rel{Blog::Index::Lists,archive,2014/02,1}}">monthly</a> index pages.
</p>

<!--/excerpt-->

<p>Relative paths are included in article sources using the following format (the format can be
configured freely):</p>

<pre><code>{rel&#8203;{&lt;ID&gt;}}</code></pre>

<p>
   The <code>&lt;ID&gt;</code> setting depends on the plugin that created the target page. You may
   want to check some of the articles for examples. Please check the API documentation of the
   plugins for more details.
</p>

<p>
   <strong>Note:</strong> If you copy the example above, you need to remove the character after the
   <code>rel</code>. It’s a zero-width space, included to prevent this example itself from being
   replaced.
</p>

<!-- :indentSize=3:tabSize=3:noTabs=true:mode=html:maxLineLen=100: -->
