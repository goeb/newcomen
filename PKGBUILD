# Maintainer: Stefan Göbel <newcomen —at— subtype —dot— de>

# NOTE: You probably want to use the PKGBUILD in the utils/pkgbuilds/newcomen
# directory, which will download and the latest distribution archive to build
# the package!
#
# This PKGBUILD will build the distribution directory/archive from the sources
# using Dist::Zilla, and build an Arch Linux package from the generated stuff.
#
# The distribution archive will also be copied to the PKGBUILD directory after
# package creation.

pkgname='newcomen'
pkgver='2019022601'
pkgrel='1'
arch=('any')
pkgdesc='A static content generator.'
url="https://subtype.de/newcomen/"
license=('PerlArtistic' 'GPL')
options=('!emptydirs')
depends=(
   'perl-capture-tiny'
   'perl-clone'
   'perl-data-rmap'                             # AUR
   'perl-data-simplepath'                       # AUR
   'perl-file-changenotify'                     # AUR
   'perl-module-find'
   'perl-moose'                                 # AUR
   'perl-moosex-aliases'                        # AUR
   'perl-moosex-nonmoose'                       # AUR
   'perl-moosex-role-parameterized'             # AUR
   'perl-moosex-strictconstructor'              # AUR
   'perl-namespace-autoclean'
   'perl-path-class'
   'perl-template-toolkit'
   'perl-text-multimarkdown'                    # AUR
   'perl-uuid-tiny'                             # AUR
   'perl-yaml'
)
makedepends=(
   'perl-dist-zilla'                            # AUR
   'perl-dist-zilla-plugin-run'                 # AUR
   'perl-dist-zilla-plugins-cjm'                # AUR
   'perl-pod-projectdocs'                       # AUR
   'rsync'
)
checkdepends=(
   'aspell'
   'aspell-en'
   'perl-test-checkmanifest'                    # AUR
   'perl-test-eol'                              # AUR
   'perl-test-lectrotest'                       # AUR
   'perl-test-notabs'                           # AUR
   'perl-test-nowarnings'
   'perl-test-pod-spelling-commonmistakes'      # AUR
   'perl-test-spelling'                         # AUR
   'perl-test-version'                          # AUR
)

_pkgbuild_dir=${_pkgbuild_dir:-$PWD}
_source_files=(
   'bin'
   'doc'
   'intro'
   'lib'
   'pod'
   't'
   'utils'
   'Changes'
   'dist.ini'
   'LICENSE'
   'MANIFEST.SKIP'
   'PKGBUILD'
   'README'
)

export PATH="$PATH:/usr/bin/vendor_perl"

prepare() {

   local _dest="$srcdir/$pkgname"
   local _file=''

   mkdir -p "$_dest"

   for _file in "${_source_files[@]}" ; do
      cp -avx "$_pkgbuild_dir/$_file" "$_dest/"
   done

   cd "$srcdir/$pkgname"
   dzil clean

}

build() {
   (
      unset PERL5LIB PERL_MM_OPT PERL_LOCAL_LIB_ROOT
      export PERL_MM_USE_DEFAULT='1' PERL_AUTOINSTALL='--skipdeps'
      cd "$srcdir/$pkgname"
      dzil build
      cd "$srcdir/$pkgname/Newcomen-$pkgver"
      perl Makefile.PL INSTALLDIRS='vendor'
      make
   )
}

check() {
   (
      cd "$srcdir/$pkgname/Newcomen-$pkgver"
      unset PERL5LIB PERL_MM_OPT PERL_LOCAL_LIB_ROOT
      export PERL_MM_USE_DEFAULT='1' TEST_AUTHOR='1'
      make test
   )
}

package() {

   (

      cd "$srcdir/$pkgname/Newcomen-$pkgver"
      unset PERL5LIB PERL_MM_OPT PERL_LOCAL_LIB_ROOT
      make install INSTALLDIRS='vendor' DESTDIR="$pkgdir"

      install -dm 0755 "$pkgdir/usr/share/doc/newcomen"
      cp -avx "$srcdir/$pkgname/Newcomen-$pkgver/doc"   \
         "$pkgdir/usr/share/doc/newcomen"
      cp -avx "$srcdir/$pkgname/Newcomen-$pkgver/intro" \
         "$pkgdir/usr/share/doc/newcomen"

      find "$pkgdir" -name .packlist     -delete
      find "$pkgdir" -name perllocal.pod -delete
      find "$pkgdir" -type d -empty      -delete

      rm -rf "$pkgdir/usr/share/perl5/vendor_perl/Newcomen/Manual"

   )

   cp -av "$srcdir/$pkgname/Newcomen-$pkgver.tar.gz" "$_pkgbuild_dir"

}

#:indentSize=3:tabSize=3:noTabs=true:mode=shellscript:maxLineLen=79:
