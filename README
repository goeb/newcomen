Newcomen - A Static Content Generator
========================================================================

Newcomen provides a framework for plugins to generate arbitrary output
from arbitrary input. Included are plugins to create a static blog site,
or a regular website. Currently MultiMarkdown is supported as markdown
format for the sources, and Template Toolkit is used as template engine.
See <http://subtype.de/newcomen/> for more details.


INSTALLATION
------------------------------------------------------------------------

Newcomen is not yet available on CPAN.

Run the following commands to install Newcomen:

   perl Makefile.PL
   make
   make test
   make install

Or, if Module::Build is available:

   perl Build.PL
   ./Build
   ./Build test
   ./Build install


SUPPORT AND DOCUMENTATION
------------------------------------------------------------------------

After the installation, you should be able to access the documentation
using the perldoc command:

   perldoc Newcomen

Additional resources are available at the following URLs:

* Newcomen homepage:  <http://subtype.de/newcomen/>
* Bug tracker:        <https://gitlab.com/goeb/newcomen/issues/>
* Git repository:     <https://gitlab.com/goeb/newcomen/>
* Repository mirror:  <https://bitbucket.org/goeb/newcomen/>


COPYRIGHT AND LICENCE
------------------------------------------------------------------------

Copyright 2013-2019 Stefan Goebel

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along
with this program. If not, see <http://www.gnu.org/licenses/>.
