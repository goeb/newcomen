# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen;
our $VERSION = 2017040102;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;
use Newcomen::Config;
use Newcomen::Core;

has 'config'  => (
   'is'       => 'ro',
   'isa'      => 'Newcomen::Config',
   'default'  => sub { Newcomen::Config -> new () },
   'handles'  => ['add_config'],
);

has '_core'   => (
   'is'       => 'ro',
   'isa'      => 'Newcomen::Core',
   'builder'  => '_build_core',
   'lazy'     => 1,
   'init_arg' => undef,
);

with 'Newcomen::Role::Attribute::Catalog';
with 'Newcomen::Role::Attribute::Crawler';
with 'Newcomen::Role::Attribute::Formatter';
with 'Newcomen::Role::Attribute::Plugins';
with 'Newcomen::Role::Attribute::Renderer';
with 'Newcomen::Role::Attribute::Site';
with 'Newcomen::Role::Attribute::Writer';

my $hooks = [

   # This arrayref defines the order of hooks. Everything that does not start with an underscore is
   # the name of a hook. run() will simply call the run_hooks() method of the U::Plugins class to
   # run these. Everything that starts with an underscore is the name of a method in this class, it
   # will be called as "$self -> $name ()". These methods will then call other hooks, check the
   # comments below for the names of these.
   #
   # The only hook that is not called from this class is the hook_rewrite_target(). This one is
   # called from the constructor of the U::Page class, i.e. it is called for every page created. All
   # other hooks are defined and called in this class.
   #
   # Note: Using '=>' as comma with auto quoting LHS. No qw() since this doesn't allow comments.

   hook_pre_init               =>
   hook_init                   =>
   hook_post_init              =>

   hook_pre_crawl              =>
   _run_crawl                  =># hook_crawl() : (U::Source)
   hook_post_crawl             =>

   hook_pre_sources            =>
   _run_source_filters         =># hook_(prepare|process|refine)_source(U::Source) : Bool
   hook_post_sources           =>

   hook_pre_build_collections  =>
   _run_build_collections      =># hook_build_collections() : (U::Collection)
   hook_post_build_collections =>

   hook_pre_clean_collections  =>
   _run_clean_collections      =># hook_clean_collection(U::Source, U::Collection, Int) : Bool
   hook_post_clean_collections =>

   hook_pre_collections        =>
   _run_collection_filters     =># hook_(prepare|process|refine)_collection(U::Collection) : Bool
   hook_post_collections       =>

   hook_pre_build_pages        =>
   _run_build_pages            =># hook_build_pages() : (U::Page) =>
   hook_post_build_pages       =>#   from U::Page: hook_rewrite_target(String, String) : String

   hook_pre_pages              =>
   _run_page_filters           =># hook_(prepare|process|refine)_page(U::Page) : Boolean
   hook_post_pages             =>

   hook_pre_format             =>
   _run_format                 =># hook_formatters(U::Content, U::Page, Int)
   hook_post_format            =>

   hook_pre_render             =>
   _run_render                 =># hook_renderer(U::Page)
   hook_post_render            =>

   hook_pre_write              =>
   _run_write                  =># hook_writer(U::Page)
   hook_post_write             =>

   hook_pre_deploy             =>
   hook_deploy                 =>
   hook_post_deploy            =>

   hook_pre_cleanup            =>
   hook_cleanup                =>
   hook_post_cleanup           =>

];

sub run {
   my $self = shift;
   for my $hook (@$hooks) {
      if ($hook =~ /^_/) {
         $self -> $hook ();
         next;
      }
      $self -> _plugins () -> run_hooks ($hook);
   }
}

sub _run_crawl {
   my $self = shift;
   for my $plugin ($self -> _plugins () -> sort ('hook_crawl')) {
      for ($self -> _plugins () -> plugin ($plugin) -> hook_crawl ()) {
         $self -> _crawler () -> add ($_);
      }
   }
}

sub _run_source_filters {
   my $self = shift;
   for my $filter (qw( hook_prepare_source hook_process_source hook_refine_source )) {
      my @remove = ();
      for my $source ($self -> _crawler () -> sources ()) {
         unless ($self -> _plugins () -> run_filter ($filter, $source)) {
            push @remove, $source -> id ()
         }
      }
      $self -> _crawler () -> delete ($_) for (@remove);
   }
}

sub _run_build_collections {
   my $self = shift;
   for my $plugin ($self -> _plugins () -> sort ('hook_build_collections')) {
      for ($self -> _plugins () -> plugin ($plugin) -> hook_build_collections ()) {
         $self -> _catalog () -> add ($_);
      }
   }
}

sub _run_clean_collections {
   my $self = shift;
   for my $coll ($self -> _catalog () -> collections ()) {
      next unless $coll -> count ();
      my @remove = ();
      for my $index (0 .. $coll -> count () - 1) {
         my $source = $coll -> source ($index);
         unless (
            $self -> _plugins () -> run_filter ('hook_clean_collection', $source, $coll, $index)
         ) {
            unshift @remove, $index;
         }
      }
      $coll -> remove ($_) for (@remove);
   }
}

sub _run_collection_filters {
   my $self = shift;
   for my $filter (qw( hook_prepare_collection hook_process_collection hook_refine_collection )) {
      my @remove = ();
      for my $coll ($self -> _catalog () -> collections ()) {
         push @remove, $coll -> id () unless $self -> _plugins () -> run_filter ($filter, $coll);
      }
      $self -> _catalog () -> delete ($_) for (@remove);
   }
}

sub _run_build_pages {
   my $self = shift;
   for my $plugin ($self -> _plugins () -> sort ('hook_build_pages')) {
      for ($self -> _plugins () -> plugin ($plugin) -> hook_build_pages ()) {
         $self -> _site () -> add ($_);
      }
   }
}

sub _run_page_filters {
   my $self = shift;
   for my $filter (qw( hook_prepare_page hook_process_page hook_refine_page )) {
      my @remove = ();
      for my $page ($self -> _site () -> pages ()) {
         unless ($self -> _plugins () -> run_filter ($filter, $page)) {
            push @remove, $page -> target ();
         }
      }
      $self -> _site () -> delete ($_) for (@remove);
   }
}

sub _run_format {
   my $self = shift;
   for my $page ($self -> _site () -> pages ()) {
      next unless $page -> count ();
      for my $index (0 .. $page -> count () - 1) {
         my $content = $page -> content ($index);
         $self -> _plugins () -> run_hooks ('hook_formatters', $content, $page, $index);
      }
      $page -> format ();
   }
}

sub _run_render {
   my $self = shift;
   for my $page ($self -> _site () -> pages ()) {
      $self -> _plugins () -> run_hooks ('hook_renderer', $page);
      $page -> render ();
   }
}

sub _run_write {
   my $self = shift;
   for my $page ($self -> _site () -> pages ()) {
      $self -> _plugins () -> run_hooks ('hook_writer', $page);
      $page -> write ();
   }
}

sub _build_core {

   Newcomen::Core -> clean_all ();
   return Newcomen::Core -> instance ('config' => shift -> config ());

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen - A static content generator - main class.

=head1 SYNOPSIS

   use Newcomen;

   my $newcomen = Newcomen -> new ();

   $newcomen -> add_config ('perl');
   $newcomen -> run ();

=head1 DESCRIPTION

B<Note:> Please see L<Newcomen::Manual> for both end user documentation and general information for
developers.

This is the main class of B<Newcomen>. It provides the interface for an actual executable script to
process an B<Newcomen> project. See the L</SYNOPSIS> for a basic example, or the script
F<bin/newcomen> included in the distribution.

=head1 CLASS METHODS

=head2 new

   my $newcomen = Newcomen -> new ('config' => $config);

Constructor. The I<config> parameter is optional. If it is provided, it has to be an existing
instance of L<Newcomen::Config>. If it is not specified, I<Newcomen> will create a new instance.
This L<Newcomen::Config> instance will be used globally for all the configuration of the different
B<Newcomen> components, plugins etc.

=head1 INSTANCE METHODS

=head2 Configuration

=head3 add_config

   $newcomen -> add_config ($backend_name, $backend_options);

Same as L<Newcomen::Config>'s L<add_config()|Newcomen::Config/add_config> method, see there for
details. Included in this class for convenience. It operates on the same L<Newcomen::Config>
instance that is returned by the L<config()|/config> method, see L<below|/config>.

=head3 config

   my $config = $newcomen -> config ();

Returns the L<Newcomen::Config> instance. This is either the instance provided to the constructor
(see L<new()|/new>), or the automatically created one if none was provided.

=head2 Processing A Project

=head3 run

   $newcomen -> run ();

Starts the build process of the current B<Newcomen> project, according to the configuration.

=head1 MISCELLANEOUS

The names and order of the hooks that may be implemented by plugins is defined (almost) entirely in
this class. See L<Newcomen::Manual::Hooks> for more details.

=head1 SEE ALSO

L<Newcomen::Config>, L<Newcomen::Manual>, L<Newcomen::Manual::Hooks>

=head1 VERSION

This is version C<2017040102>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2017 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
