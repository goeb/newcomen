# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Writer;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;

with 'Newcomen::Role::Attribute::Core';
with 'Newcomen::Role::Attribute::Config';
with 'Newcomen::Role::Frontend' => {
   'namespace' => 'Newcomen::Writer',
   'type'      => 'Newcomen::Writer::Backend',
};

sub BUILD {
   shift -> _config () -> add_default ({
      'writer'        => {
         'output_dir' => undef,
         'override'   => undef,
      },
   });
}

sub write_page {

   my $self = shift;
   my $page = shift;

   return unless defined $page -> writer ();

   my $name = $page -> writer () -> name ();
   my $full = "Newcomen::Writer::$name";
   confess "No such writer: $name" unless $self -> backend ($full);

   $self -> backend ($full) -> write ($page, $page -> writer () -> options ());

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Writer - Writer frontend.

=head1 SYNOPSIS

   use Newcomen::Writer;

   my $writer = Newcomen::Writer -> new ();

   $writer -> write_page ($page);

=head1 DESCRIPTION

I<Newcomen::Writer> is the frontend for the page writing modules. The actual writing (if any, the
method to store a page depends on the backend) will be done by backends. The writer backend to use
and its options are set on a per-page basis (see L<Newcomen::Page>).

Plugins may use L<Newcomen::Role::Attribute::Writer> to access the main writer instance, but usually
this should not be necessary.

=head1 OPTIONS

   {
      'writer'        => {
         'output_dir' => undef,
         'override'   => undef,
      },
   }

These default options may be used by backends unless overridden by backend specific options.
I<output_dir> specifies the output directory, relative to the project's root directory. I<override>
may be set to a true value if writer backends should override existing files. See the individual
backends for more options.

B<Note:> It is up to the backends to respect these options. See the appropriate backend
documentation to find out if these options are used or not.

=head1 CLASS METHODS

=head2 new

   my $writer = Newcomen::Writer -> new ();

Constructor. Expects no parameters.

=head1 INSTANCE METHODS

=head2 Writing

=head3 write_page

   $writer -> write_page ($page);

Expects a single L<Newcomen::Page> instance as parameter. If there is a writer backend set for the
page, the backend will be called to write it. What exactly this means depends on the backend, see
the individual backends for details. The return value of this method is not specified. Details and
backend options may differ for different backends, see the appropriate documentation for details.
Backend names must be module basenames only (i.e. without the C<'Newcomen::Writer::'> prefix), and
names are case sensitive. The backend must exist, else this method will I<die()>.

=head2 Backends

The methods L<backend()|Newcomen::Role::Frontend/backend>,
L<backend_exists()|Newcomen::Role::Frontend/backend_exists> and
L<backends()|Newcomen::Role::Frontend/backends> are available to access the writer backends, though
usually this should not be necessary. For a description of these methods please see
L<Newcomen::Role::Frontend>.

=head1 MISCELLANEOUS

Backends must use L<Newcomen::Writer::Backend> as their base class. They must be placed in the
I<Newcomen::Writer> namespace, and implement a L<_do_write()|Newcomen::Writer::Backend/do_write>
method. For more details, see L<Newcomen::Writer::Backend>.

=head1 SEE ALSO

L<Newcomen::Page>, L<Newcomen::Role::Attribute::Writer>, L<Newcomen::Role::Frontend>,
L<Newcomen::Writer::Backend>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
