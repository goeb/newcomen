# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Collection;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::Aliases;
use MooseX::StrictConstructor;
use Newcomen::Data;
use Newcomen::Util::Traits;

has [qw( creator id )] => (
   'is'                => 'ro',
   'isa'               => 'Str',
   'required'          => 1,
);

has 'meta'             => (
   'is'                => 'ro',
   'isa'               => 'Newcomen::Data',
   'init_arg'          => undef,
   'default'           => sub { Newcomen::Data -> new () },
   'handles'           => [qw( delete exists get set )],
);

has '_sources'         => (
   'is'                => 'ro',
   'isa'               => 'ArrayRef[Newcomen::Source]',
   'default'           => sub {[]},
   'init_arg'          => 'sources',
   'traits'            => ['Array'],
   'handles'           => Newcomen::Util::Traits::std_array_handles (),
);

alias 'source'  => 'item';
alias 'sources' => 'items';

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Collection - Manages an ordered list of source items and meta data.

=head1 SYNOPSIS

   use Newcomen::Collection;

   my $collection = Newcomen::Collection -> new (
      'creator' => $creator,
      'id'      => $collection_id
   );

   # Add a source:
   $collection -> push ($source);

   # Get first source item:
   my $source = $collection -> source (0);

   # Iterate over the source items:
   for my $source ($collection -> sources ()) {
      # do something...
   }

   # Set some meta data:
   $collection -> set (['some', 'data'], 'value');

   # Retrieve the meta data:
   my $value = $collection -> get ('some/data');

=head1 DESCRIPTION

A collection is a container for an ordered list of L<Newcomen::Source> instances and related meta
data. See the developer section in L<Newcomen::Manual> for a description of how B<Newcomen> works
and how collections fit in.

=head1 CLASS METHODS

=head2 new

   my $collection = Newcomen::Collection -> new (
      'creator' => $creator,
      'id'      => $collection_id,
      'sources' => $sources
   );

Constructor. The I<creator> parameter is used to indicate which plugin created the collection. It is
usually set to the plugin basename (i.e. the plugin name without the C<'Newcomen::Plugin::'>
prefix). The I<id> parameter is used to identify the collection. If the collection is added to the
catalog (see L<Newcomen::Catalog>), this ID must be globally unique. It is recommended to prefix the
ID with the plugin basename to avoid clashes with other plugins' collections. Both I<creator> and
I<id> are required parameters. The I<sources> parameter is optional. If it is specified, it must be
an arrayref containing L<Newcomen::Source> instances. These will be the initial content of the
collection. If it is not specified, the collection will not contain any source items initially. The
collection's meta data will always be empty initially.

=head1 INSTANCE METHODS

=head2 General

=head3 creator

   my $creator = $collection -> creator ();

Returns the creator of the collection. The value can only be set during construction (see
L<new()|/new>).

=head3 id

   my $id = $collection -> id ();

Returns the ID of the collection. The value can only be set during construction (see L<new()|/new>).

=head2 Meta Data

Meta data is stored in an L<Newcomen::Data> instance.

=head3 delete, exists, get, set

The methods I<delete()>, I<exists()>, I<get()> and I<set()> are mapped to the L<Newcomen::Data>
methods of the same name, see there for details.

=head3 meta

   my $meta = $collection -> meta ();

Returns the L<Newcomen::Data> instance storing the collection's meta data.

=head2 Source Items

I<Newcomen::Collection> uses an arrayref to store the collections as an ordered list. It uses the
L<array methods|Newcomen::Util::Traits/Array_Methods> of L<Newcomen::Util::Traits>, with the
following additions:

=over

=item *

The I<Newcomen::Collection> method I<source()> is an alias for the
L<item()|Newcomen::Util::Traits/item> method.

=item *

The I<Newcomen::Collection> method I<sources()> is an alias for the
L<items()|Newcomen::Util::Traits/items> method.

=back

Please see L<Newcomen::Util::Traits> for a complete list of methods and more details.

=head1 SEE ALSO

L<Newcomen::Catalog>, L<Newcomen::Data>, L<Newcomen::Manual>, L<Newcomen::Source>,
L<Newcomen::Util::Traits>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
