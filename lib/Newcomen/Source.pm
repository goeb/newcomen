# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Source;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;
use Newcomen::Data;

has [qw( creator id )] => (
   'is'                => 'ro',
   'isa'               => 'Str',
   'required'          => 1,
);

has 'content'          => (
   'is'                => 'rw',
   'isa'               => 'Maybe[Str]',
   'default'           => undef,
);

has 'meta'             => (
   'is'                => 'ro',
   'isa'               => 'Newcomen::Data',
   'init_arg'          => undef,
   'default'           => sub { Newcomen::Data -> new () },
   'handles'           => [qw( delete exists get set )],
);

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Source - Represents a single data source of the project.

=head1 SYNOPSIS

   use Newcomen::Source;

   my $source = Newcomen::Source -> new (
      'creator' => $creator,
      'id'      => $source_id,
      'content' => $content,
   );

   # Set some meta data:
   $source -> set (['some', 'data'], 'value');

=head1 DESCRIPTION

An I<Newcomen::Source> instance represents a data source of the B<Newcomen> project, including its
content, and arbitrary meta data. See the developer section in L<Newcomen::Manual> for a description
of how B<Newcomen> works and how I<Newcomen::Source> fits in.

=head1 CLASS METHODS

=head2 new

   my $source = Newcomen::Source -> new (
      'creator' => $creator,
      'id'      => $source_id,
      'content' => $content,
   );

Constructor.

The I<creator> parameter identifies the plugin that creates the source instance. It is suggested,
though not required, to use the module name or module basename (i.e. the module name without the
C<'Newcomen::Plugin::'> prefix).

The I<id> parameter identifies the source. Usually this is the (relative or absolute) path of the
file for sources on the file system. If the source is added to the crawler (L<Newcomen::Crawler>),
every source must have a unique ID.

Both the I<creator> and I<id> parameters are required, and these values can not be changed later on.

The optional I<content> parameter may be used to set the initial content of the source item. If
provided, it must be a string (or C<undef>, if required). The content can be changed later on. If
not provided, the initial value will be C<undef>.

=head1 INSTANCE METHODS

=head2 General

=head3 creator, id

   my $creator   = $source -> creator ();
   my $source_id = $source -> id ();

Returns the creator ID, or the source ID, respectively. The values are set via the constructor (see
L<new()|/new> above for a description), and can not be changed later on.

=head3 content

   # Get the source's content:
   my $content = $source -> content ();

   # Set the content:
   $source -> content ($content);

Getter/setter for the source's content. The parameter for the setter may be any string, or C<undef>,
if required. The content may also be set using the constructor, see L<new()|/new>. It is recommended
to not change the content once it has been set, an L<Newcomen::Content> instance should be created
instead. See L<Newcomen::Manual> for the general concepts behind these classes.

=head2 Meta Data

Meta data is stored in an L<Newcomen::Data> instance.

=head3 delete, exists, get, set

The methods I<delete()>, I<exists()>, I<get()> and I<set()> are mapped to the L<Newcomen::Data>
methods of the same name, see there for details.

=head3 meta

   my $meta = $source -> meta ();

Returns the L<Newcomen::Data> instance storing the source's meta data.

=head1 SEE ALSO

L<Newcomen::Content>, L<Newcomen::Crawler>, L<Newcomen::Data>, L<Newcomen::Manual>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
