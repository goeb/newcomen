# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugins;
our $VERSION = 2014052501;

use namespace::autoclean;
use sort 'stable';
use Module::Find ();
use Moose '-meta_name' => '_moose_meta';
use MooseX::Aliases;
use MooseX::StrictConstructor;
use Newcomen::Util::Traits;

with 'Newcomen::Role::Attribute::Core';
with 'Newcomen::Role::Attribute::Config';

{

   my $methods = Newcomen::Util::Traits::std_hash_handles ();

   delete $methods -> {$_} for (qw( delete set wipe ));
   $methods -> {'_set'} = 'set';

   has '_plugins' => (
      'is'        => 'ro',
      'isa'       => 'HashRef[Newcomen::Plugin::Base]',
      'builder'   => '_build_plugins',
      'lazy'      => 1,
      'init_arg'  => undef,
      'traits'    => ['Hash'],
      'handles'   => $methods,
   );

}

alias 'names'   => 'keys';
alias 'plugin'  => 'get';
alias 'plugins' => 'values';

sub run_hooks {

   my $self  = shift;
   my $hooks = ref $_ [0] eq 'ARRAY' ? shift : [shift];
   my @args  = @_;

   for my $hook (@$hooks) {
      $self -> plugin ($_) -> $hook (@args) for ($self -> sort ($hook));
   }

}

sub run_filter {

   my $self   = shift;
   my $filter = shift;
   my @args   = @_;

   for my $plugin ($self -> sort ($filter)) {
      return undef unless $self -> plugin ($plugin) -> $filter (@args);
   }

   return 1;

}

sub sort {

   my $self = shift;
   my $hook = shift;
   my %sort = ();

   for my $plugin ($self -> elements ()) {

      my $name = $plugin -> [0];
      my $inst = $plugin -> [1];

      $sort {$name} = $inst -> hook_order ($hook) if $name -> can ($hook);

   }

   my @keys = sort keys %sort;
   return sort { $sort {$a} <=> $sort {$b} } @keys;

}

sub _build_plugins {

   my $self    = shift;
   my $config  = $self -> _config ();
   my @plugins = grep { $_ ne 'Newcomen::Plugin::Base' } Module::Find::useall ('Newcomen::Plugin');
   my $plugins = {};

   for my $plugin (sort @plugins) {
      my $name = substr ($plugin, length 'Newcomen::Plugin::');
      next unless $config -> get (['plugins', $name]) and not $plugins -> {$plugin};
      $plugins -> {$plugin} = $plugin -> new ();
   }

   return %$plugins ? $plugins : confess 'No plugins loaded';

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugins - Manages the Newcomen plugins.

=head1 SYNOPSIS

   use Newcomen::Plugins;

   my $plugins = Newcomen::Plugins -> new ();

   # Get a single plugin instance:
   my $plugin = $plugins -> plugin ($plugin_name);

   # Run a hook:
   $plugins -> run_hooks ($hook_name);

=head1 DESCRIPTION

This is a container that provides access to the plugin instances. On first access, the plugins that
are enabled (see the configuration section in L<Newcomen::Manual>) will be instantiated, in
alphabetical order (Perl's I<sort()> function is used, with no custom sorting, it may depend on the
locale). Plugins may be accessed using their full (!) module name as the key. I<Newcomen::Plugins>
also provides methods to run hooks and filter hooks for all plugins implementing them, and a method
to get a sorted list of plugins implementing a specific hook (to run it manually from another
component if the hook is not using a standard or filter hook syntax).

Note: Though only plugins that are enabled in the configuration will be instantiated, all plugins
found will be I<use>d.

Plugins themselves may use L<Newcomen::Role::Attribute::Plugins> to access the main instance of this
class (usually not required).

=head1 CLASS METHODS

=head2 new

   my $plugins = Newcomen::Plugins -> new ();

Constructor. Expects no parameters.

=head1 INSTANCE METHODS

=head2 Plugin Access

I<Newcomen::Plugins> uses a hashref to store the plugin instances, with the full module names as
keys, and the instances as values. It uses the L<hash methods|Newcomen::Util::Traits/Hash_Methods>
of L<Newcomen::Util::Traits>, with the following exceptions and additions:

=over

=item *

The methods L<delete()|Newcomen::Util::Traits/delete>, L<set()|Newcomen::Util::Traits/set> and
L<wipe()|Newcomen::Util::Traits/wipe> are not available.

=item *

The I<Newcomen::Plugins> method I<names()> is an alias for the L<keys()|Newcomen::Util::Traits/keys>
method.

=item *

The I<Newcomen::Plugins> method I<plugin()> is an alias for the L<get()|Newcomen::Util::Traits/get>
method.

=item *

The I<Newcomen::Plugins> method I<plugins()> is an alias for the
L<values()|Newcomen::Util::Traits/values> method.

=back

Please see L<Newcomen::Util::Traits> for a complete list of methods and more details.

=head2 Hook Related

=head3 run_hooks

   # Run a single hook:
   $plugins -> run_hooks ($hook, @parameters);

   # Run multiple hooks with one call:
   $plugins -> run_hooks ([$hook_1, $hook_2], @parameters);

Runs the hook(s) specified by the first parameter (either a single hook name or an arrayref
containing multiple hook names). If multiple hooks are supplied, they will be run in the specified
order. Hook return values are ignored. All parameters following the first one will be passed to the
hooks. Plugins will be called in the order defined by them (see L<sort()|/sort>).

=head3 run_filter

   $plugins -> run_filter ($filter, @parameters);

This method will run the hook (specified by the first parameter) of all plugins implementing it. If
one plugin's hook returns a false value, processing will stop (i.e. pending plugin's hooks will not
be called) and the method itself will return a false value. If all hooks return a true value,
I<run_filter()> will return a true value after all hooks have been run. All parameters following
the hook name will simply be passed to the plugin hooks. Plugins will be called in the order defined
by them (see L<sort()|/sort>).

=head3 sort

   my @order = $plugins -> sort ($hook);

Returns a list of plugins implementing the hook specified as first parameter. Hook order is defined
by the plugins, see L<Newcomen::Plugin::Base>. If two (or more) plugins define the same priority for
one hook, the order of these plugins is determined by standard string comparison of the module names
(may be locale dependent).

=head1 MISCELLANEOUS

Plugins must use L<Newcomen::Plugin::Base> as their base class. They must be placed in the
I<Newcomen::Plugin> namespace. For more details, see L<Newcomen::Plugin::Base>.

=head1 SEE ALSO

L<Newcomen::Manual>, L<Newcomen::Plugin::Base>, L<Newcomen::Role::Attribute::Plugins>,
L<Newcomen::Util::Traits>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
