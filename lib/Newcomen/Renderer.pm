# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Renderer;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;

with 'Newcomen::Role::Frontend' => {
   'namespace' => 'Newcomen::Renderer',
   'type'      => 'Newcomen::Renderer::Backend',
};

sub render_page {

   my $self = shift;
   my $page = shift;

   return unless defined $page -> renderer ();

   my $name = $page -> renderer () -> name ();
   my $full = "Newcomen::Renderer::$name";
   confess "No such renderer: $name" unless $self -> backend ($full);

   $page -> rendered (
      $self -> backend ($full) -> render ($page, $page -> renderer () -> options ())
   );

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Renderer - Renderer frontend.

=head1 SYNOPSIS

   use Newcomen::Renderer;

   my $renderer = Newcomen::Renderer -> new ();

   $render -> render_page ($page);

=head1 DESCRIPTION

I<Newcomen::Renderer> is the frontend for the page rendering modules. The actual rendering will be
done by backends. The rendering backend to use and its options are set on a per-page basis (see
L<Newcomen::Page>).

Plugins may use L<Newcomen::Role::Attribute::Renderer> to access the main renderer instance, but
usually this should not be necessary.

=head1 CLASS METHODS

=head2 new

   my $renderer = Newcomen::Renderer -> new ();

Constructor. Expects no parameters.

=head1 INSTANCE METHODS

=head2 Rendering

=head3 render_page

   $renderer -> render_page ($page);

Expects a single L<Newcomen::Page> instance as parameter. If there is a renderer backend set for the
page, the backend will be called to render it. The rendered content will automatically be set for
the page (see L<Newcomen::Page>'s L<rendered()|Newcomen::Page/rendered> method). The return value of
this method is not specified. Details and backend options may differ for different backends, see the
appropriate documentation for details. Backend names must be module basenames only (i.e. without the
C<'Newcomen::Renderer::'> prefix), and names are case sensitive. The backend must exist, else this
method will I<die()>.

=head2 Backends

The methods L<backend()|Newcomen::Role::Frontend/backend>,
L<backend_exists()|Newcomen::Role::Frontend/backend_exists> and
L<backends()|Newcomen::Role::Frontend/backends> are available to access the renderer backends,
though usually this should not be necessary. For a description of these methods please see
L<Newcomen::Role::Frontend>.

=head1 MISCELLANEOUS

Backends must use L<Newcomen::Renderer::Backend> as their base class. They must be placed in the
I<Newcomen::Renderer> namespace, and implement a
L<_do_render()|Newcomen::Renderer::Backend/do_render> method. For more details, see
L<Newcomen::Renderer::Backend>.

=head1 SEE ALSO

L<Newcomen::Page>, L<Newcomen::Renderer::Backend>, L<Newcomen::Role::Attribute::Renderer>,
L<Newcomen::Role::Frontend>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
