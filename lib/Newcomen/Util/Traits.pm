# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Util::Traits;
our $VERSION = 2014052501;

use strict;
use warnings;
use Exporter ();

our @ISA       = qw( Exporter );
our @EXPORT_OK = qw( std_array_handles std_hash_handles );

sub std_array_handles {{

   'clear'    => 'clear',
   'count'    => 'count',
   'empty'    => 'is_empty',
   'find'     => 'first',
   'first'    => 'first_index',
   'grep'     => 'grep',
   'insert'   => 'insert',
   'item'     => 'get',
   'items'    => 'elements',
   'map'      => 'map',
   'ordered'  => 'sort',
   'paginate' => 'natatime',
   'push'     => 'push',
   'remove'   => 'delete',
   'sort'     => 'sort_in_place',
   'splice'   => 'splice',
   'unshift'  => 'unshift',

}}

sub std_hash_handles {{

   'defined'  => 'defined',
   'delete'   => 'delete',
   'elements' => 'kv',
   'exists'   => 'exists',
   'get'      => 'get',
   'keys'     => 'keys',
   'set'      => 'set',
   'total'    => 'count',
   'values'   => 'values',
   'void'     => 'is_empty',
   'wipe'     => 'clear',

}}

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Util::Traits - Moose related utility functions.

=head1 SYNOPSIS

   package MyPackage;

   use namespace::autoclean;
   use Moose;
   use Newcomen::Util::Traits;

   has 'array'  => (
      'is'      => 'ro',
      'isa'     => 'ArrayRef',
      'default' => sub {[]},
      'traits'  => ['Array'],
      'handles' => Newcomen::Util::Traits::std_array_handles (),
   );

   has 'hash'   => (
      'is'      => 'ro',
      'isa'     => 'HashRef',
      'default' => sub {{}},
      'traits'  => ['Hash'],
      'handles' => Newcomen::Util::Traits::std_hash_handles (),
   );

   __PACKAGE__ -> meta () -> make_immutable ();

   1;

Using that package:

   use MyPackage;

   my $instance = MyPackage -> new ();

   # Add a value to the array attribute:
   $instance -> push ($value);

   # Add an entry to the hash attribute:
   $instance -> set ('key', $value);

=head1 DESCRIPTION

This package provides utility functions for L<Moose> classes. It is not a L<Moose> class itself.
Note that all functions may be imported into the package using I<Newcomen::Util::Traits>:

   use Newcomen::Util::Traits qw( std_array_handles std_hash_handles );

No function is exported by default.

If you followed a link from another module's documentation to this page and are looking for the
array and/or hash methods, see L</INSTANCE METHODS> below. In that case you may ignore the section
L</FUNCTIONS>. Keep in mind that these instance methods are directly available as instance methods
of the class that led you here (any exceptions are mentioned in the class' documentation).

=head1 FUNCTIONS

=head2 std_array_handles, std_hash_handles

   has 'array'  => (
      'is'      => 'ro',
      'isa'     => 'ArrayRef',
      'default' => sub {[]},
      'traits'  => ['Array'],
      'handles' => std_array_handles (),
   );

   has 'hash'   => (
      'is'      => 'ro',
      'isa'     => 'HashRef',
      'default' => sub {{}},
      'traits'  => ['Hash'],
      'handles' => std_hash_handles (),
   );

The two functions I<std_array_handles()> and I<std_hash_handles()> may be used to add a default set
of handles to a L<Moose> attribute (either an arrayref attribute or a hashref attribute,
respectively). Both functions return a hashref containing the mapping of method names to L<Moose>'s
native traits (see L<Moose::Meta::Attribute::Native::Trait::Array> for array traits and
L<Moose::Meta::Attribute::Native::Trait::Hash> for hash traits). The handles are named in a way that
allows a class to include one arrayref attribute using the array handles and one hashref attribute
using the hash handles at the same time without clashing method names. See below for the list of
methods that will be available to the classes.

=head1 INSTANCE METHODS

The following methods will be available in the classes using one of I<Newcomen::Util::Traits>'
functions, i.e. these are B<not> I<Newcomen::Util::Traits>' methods!

=head2 Array Methods

=head3 clear

   $instance -> clear ();

Handle for the I<clear()> native trait.

Removes all elements from the array. This method does not accept any parameters, the return value is
not defined.

=head3 count

   my $number = $instance -> count ();

Handle for the I<count()> native trait.

Returns the number of elements in the array. This method does not accept any parameters.

=head3 empty

   if ($instance -> empty ()) {
      # do something...
   }

Handle for the I<is_empty()> native trait.

Returns a true value if the array does not contain any elements, a false value if there is at least
one element. This method does not accept any parameters.

=head3 find

   my $element = $instance -> find (sub { ... });

Handle for the I<first()> native trait.

Returns the first matching element of the array. The parameter has to be a subroutine reference. It
will be called against each array element until one matches or all elements have been checked. If no
matching element can be found, the method will return C<undef>.

=head3 first

   my $index = $instance -> first (sub { ... });

Handle for the I<first_index()> native trait.

Like L<find()|/find> (see above), but instead of returning the matching element itself this method
returns the matching element's index in the array, or C<-1> if there is no match.

=head3 grep

   my @matching = $instance -> grep (sub { ... });

Handle for the I<grep()> native trait.

Like Perl's I<grep()> function, this method returns a list of all matching elements of the array.
The parameter has to be a subroutine reference that will be called against each array element.

=head3 insert

   $instance -> insert ($index, $value);

Handle for the I<insert()> native trait.

Inserts a new element (second parameter) at the specified index (first parameter) into the array.
Returns the new value at the specified index.

=head3 item

   my $item = $instance -> item ($index);

Handle for the I<get()> native trait.

Returns the element of the array at the specified index (the only parameter). A negative index may
be used, like in Perl's array handling. If the specified element does not exist, the method will
return C<undef>.

=head3 items

   my @items = $instance -> items ();

Handle for the I<elements()> native trait.

Returns all the items in the array as a list. This method does not accept any parameters.

=head3 map

   my @stuff = $instance -> map (sub { ... });

Handle for the I<map()> native trait.

Like Perl's I<map()> function, this method applies the subroutine (the only parameter) to every
element of the array and returns the new array.

=head3 ordered

   my @ordered = $instance -> ordered (sub { ... });

Handle for the I<sort()> native trait.

Returns the elements of the array in sorted order. As an optional parameter a subroutine may be
specified to decide about item order (like Perl's I<sort()> function). Unlike Perl's I<sort()>
function, the two elements to be compared will be available as C<$_[0]> and C<$_[1]> inside the
subroutine. If no subroutine is specified, sorting defaults to ASCII order.

=head3 paginate

   my $iterator = $instance -> paginate (5);

   while (my @values = $iterator -> ()) {
      # do something...
   }

Handle for the I<natatime()> native trait.

Returns an array iterator, which itself returns a number of items (specified by the first parameter
of the I<paginate()> call) at each call. I.e. in the example above the first iteration will handle
the first five array items, the second iteration the next five and so on until no more items are
left. This works like I<natatime()> from L<List::MoreUtils>. As an optional second parameter, a
coderef may be specified. It will be called for each group of items in the array, like described
before.

=head3 push

   my $size = $instance -> push ($value1, $value2);

Handle for the I<push()> native trait.

Adds the new value or values at the end of the array. An arbitrary number of parameters may be
specified (at least one). The method returns the number of elements in the array after the
operation.

=head3 remove

   my $value = $instance -> remove ($index);

Handle for the I<delete()> native trait.

This method deletes the element at the specified index (the only parameter) from the array and
returns the deleted value. If there is no element at the specified index, it will return C<undef>.

=head3 sort

   $instance -> sort (sub { ... });

Handle for the I<sort_in_place()> native trait.

Works like the L<ordered()|/ordered> method (see above), but instead of returning the sorted array
the array itself will be modified. This method accepts the same parameters as L<ordered()|/ordered>,
but does not define a return value.

=head3 splice

   my @removed = $instance -> splice ($index, $length, @values);

Handle for the I<splice()> native trait.

This method works like Perl's I<splice()> function. It will return the last element removed in
scalar context (C<undef> if nothing was removed), or all elements removed in list context. At least
one parameter has to be specified.

=head3 unshift

   my $size = $instance -> unshift ($value1, $value2);

Handle for the I<unshift()> native trait.

Adds the new value or values at the beginning of the array. An arbitrary number of parameters may be
specified. The method returns the number of elements in the array after the operation.

=head2 Hash Methods

=head3 defined

   if ($instance -> defined ($key)) {
      # do something...
   }

Handle for the I<defined()> native trait.

Returns a true value if the value for the specified key (the only parameter) is defined, else a
false value.

=head3 delete

   my @deleted = $instance -> delete ($key1, $key2);

Handle for the I<delete()> native trait.

Deletes the elements with the specified keys from the hash. In list context this method returns the
list of values (not the keys!) that have been deleted. In scalar context it returns just the last
value deleted. At least one key has to be specified as parameter, specifying more than one key is
allowed.

=head3 elements

   for ($instance -> elements ()) {
      my $key   = $_ -> [0];
      my $value = $_ -> [1];
   }

Handle for the I<kv()> native trait.

Returns a list of all key/value pairs in the hash. Every element of the returned list is itself an
arrayref, with the first element in this arrayref being the key, the second element being the value.
This method does not expect any parameters.

=head3 exists

   if ($instance -> exists ($key)) {
      # do something...
   }

Handle for the I<exists()> native trait.

Returns a true value if the specified key (the only parameter) exists in the hash, else a false
value.

=head3 get

   my @values = $instance -> get ($key1, $key2);

Handle for the I<get()> native trait.

At least one key has to be supplied as parameter. In list context, the method returns a list of all
values for the specified keys. In scalar context, only the value for the last key will be returned.
If a key does not exist, the value for that key will be C<undef>.

=head3 keys

   my @keys = $instance -> keys ();

Handle for the I<keys()> native trait.

Returns a list containing all keys of the hash (the order is not defined). This method does not
expect any parameters.

=head3 set

   my @values = $instance -> set ($key1 => $value1, $key2 => $value2);

Handle for the I<set()> native trait.

This method sets the specified values for the specified keys. At least one key/value pair has to be
supplied, and the number of parameters must be even. The return value is a list of all values set,
in the order they were specified.

=head3 total

   my $number = $instance -> total ();

Handle for the I<count()> native trait.

Returns the number of values in the hash. This method does not expect any parameters.

=head3 values

   my @values = $instance -> values ();

Handle for the I<values()> native trait.

Returns a list containing all values of the hash (the order is not defined). This method does not
expect any parameters.

=head3 void

   if ($instance -> void ()) {
      # do something...
   }

Handle for the I<is_empty()> native trait.

Returns a true value if there is no element in the hash, else a false value. This method does not
expect any parameters.

=head3 wipe

   $instance -> wipe ();

Handle for the I<clear()> native trait.

Removes all content from the hash. This method does not expect any parameters, and does not define a
return value.

=head1 SEE ALSO

L<List::Util>, L<List::MoreUtils>, L<Moose>, L<Moose::Meta::Attribute::Native::Trait::Array>,
L<Moose::Meta::Attribute::Native::Trait::Hash>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
