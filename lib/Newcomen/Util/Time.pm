# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Util::Time;
our $VERSION = 2014052501;

use strict;
use warnings;
use Exporter ();
use POSIX ();
use Time::Piece;

our @ISA       = qw( Exporter );
our @EXPORT_OK = qw( parse_time );

sub parse_time {

   my $time    = shift;
   my $parse   = shift;
   my $format  = shift;
   my $piece   = Time::Piece -> strptime ($time, $parse);
   my $tz_offs = $piece -> localtime -> tzoffset () -> minutes ();
   my $tz_sign = $tz_offs < 0 ? '-' : '+';
   my $tz_hour = sprintf ('%02d', POSIX::floor (abs ($tz_offs) / 60));
   my $tz_min  = sprintf ('%02d', abs ($tz_offs) - $tz_hour * 60);

   my $result = {

      'year'        => $piece -> year        (),
      'month'       => $piece -> mon         (),
      'day'         => $piece -> mday        (),
      'hour'        => $piece -> hour        (),
      'minute'      => $piece -> min         (),
      'second'      => $piece -> sec         (),
      'day_of_week' => $piece -> day_of_week (),
      'day_of_year' => $piece -> day_of_year (),
      'week'        => $piece -> week        (),
      'month_short' => $piece -> monname     (),
      'month_name'  => $piece -> fullmonth   (),
      'day_short'   => $piece -> wdayname    (),
      'day_name'    => $piece -> fullday     (),
      'is_dst'      => $piece -> isdst       (),
      'epoch'       => $piece -> epoch       (),
      'iso'         => $piece -> datetime    (),

      'offset'      => "$tz_sign$tz_hour:$tz_min",

      'Month'       => sprintf ('%02d', $piece -> mon  ()),
      'Day'         => sprintf ('%02d', $piece -> mday ()),
      'Hour'        => sprintf ('%02d', $piece -> hour ()),
      'Minute'      => sprintf ('%02d', $piece -> min  ()),
      'Second'      => sprintf ('%02d', $piece -> sec  ()),

   };

   $result -> {'string'} = $piece -> strftime ($format) if $format;

   return $result;

}

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Util::Time - Date and time related utility functions.

=head1 SYNOPSIS

   use Newcomen::Util::Time;

   # Parse a time string:
   my $time = Newcomen::Util::Time::parse_time ($time, $parse_format, $output_format);

=head1 DESCRIPTION

This package provides utility functions dealing with dates and times. Note that all functions may be
imported into the package using I<Newcomen::Util::Time>:

   use Newcomen::Util::List qw( parse_time );

No function is exported by default.

=head1 FUNCTIONS

=head2 parse_time

   my $time = parse_time ($time, $parse_format, $output_format);

Expects three parameters: the first one has to be the string to be parsed, the second one the time
format specification to be used for parsing the string (see L<Time::Piece>'s
L<strptime()|Time::Piece/Date_Parsing> method, which is used to parse the string, for more details).
The third parameter is optional. If it is provided, it is used as date format specification for
L<Time::Piece>'s I<strftime()> method, and the result of this is included in the return values.

The function returns a hashref that contains information about the parsed date/time. The keys are:
I<year>, I<month>, I<day>, I<hour>, I<minute>, I<second>, I<day_of_week>, I<day_of_year>, I<week>,
I<month_name>, I<month_short>, I<day_name>, I<day_short>, I<is_dst>, I<epoch>, I<iso>, I<Day>,
I<Month>, I<Hour>, I<Minute>, I<Second>. The values for the keys starting with a capital letter will
include a leading zero for values less than ten. The I<*_short> values will contain the abbreviated
names, while I<*_name> values will be full names. If the third parameter is used, the value of the
reformatted date/time will be available under the key I<string>, otherwise this key is omitted. The
key I<offset> will contain the timezone offset in the format I<< <+/-><hours>:<minutes> >>, e.g.
C<'+04:00'>, for the local timezone to GMT.

=head1 SEE ALSO

L<Time::Piece>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
