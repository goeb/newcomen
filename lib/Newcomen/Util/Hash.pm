# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Util::Hash;
our $VERSION = 2014052501;

use strict;
use warnings;
use Clone;
use Exporter ();

our @ISA       = qw( Exporter );
our @EXPORT_OK = qw( merge );

sub merge {

   die 'Expected two hashrefs' unless ref $_ [0] eq 'HASH' and ref $_ [1] eq 'HASH' and $#_ == 1;

   my $left  = Clone::clone shift;
   my $right = Clone::clone shift;

   for my $key (keys %$right) {
      if (ref $left -> {$key} eq 'HASH' and ref $right -> {$key} eq 'HASH') {
         $left -> {$key} = merge ($left -> {$key}, $right -> {$key});
      }
      else {
         $left -> {$key} = $right -> {$key};
      }
   }

   return $left;

}

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Util::Hash - Hash related utility functions.

=head1 SYNOPSIS

   use Newcomen::Util::Hash;

   # Merge two hashes:
   my $merged = Newcomen::Util::Hash::merge ($hashref_1, $hashref_2);

=head1 DESCRIPTION

This package provides utility functions dealing with hashes. Note that all functions may be imported
into the package using I<Newcomen::Util::Hash>:

   use Newcomen::Util::Hash qw( merge );

No function is exported by default.

=head1 FUNCTIONS

=head2 merge

   my $merged = merge ($hashref_1, $hashref_2);

This function basically works like L<Hash::Merge::Simple>'s I<merge()> function. It will merge the
two hashes (passed by their references) and return the result (as a hashref). It expects exactly two
parameters. Before merging, the hashes will be cloned (using L<Clone>'s I<clone()> function), so
modifying the result will not alter the originals. Elements in the second hash will override those
in the first hash if required. Do not put any circular hashref stuff in the hashes, it may not work!

=head1 SEE ALSO

L<Clone>, L<Hash::Merge::Simple>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
