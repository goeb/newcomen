# Copyright 2013-2017 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Util::File;
our $VERSION = 2017033001;

use strict;
use warnings;
use Exporter ();
use Newcomen::Util::List;
use YAML::Any ();

our @ISA       = qw( Exporter );
our @EXPORT_OK = qw( stat_hash parse_file );

sub stat_hash {

   my $file = shift;
   my %stat = ();

   %stat = (Newcomen::Util::List::zip (
      qw( dev ino mode nlink uid gid rdev size atime mtime ctime blksize blocks ), stat ($file)
   ));

   return \%stat;

}

sub parse_file {

   my $file = shift;
   my $text = undef;
   my $yaml = '';

   open (my $fh, '<:encoding(UTF-8)', $file) or die "Can't open file $file: $!";

   while (my $line = <$fh>) {
      chomp (my $chomped = $line);
      if (($chomped eq '---' or $chomped eq '===') and not defined $text) {
         $text = '';
         next;
      }
      ${ defined $text ? \$text : \$yaml } .= $line;
   }

   close ($fh) or die "Can't close file $file: $!";

   return ({}, $yaml) unless defined $text;

   my @meta = YAML::Any::Load ($yaml);

   unless (@meta and (ref $meta [0] eq 'HASH' or not defined $meta [0])) {
      die "Invalid meta data in $file";
   }

   return ($meta [0] // {}, $text);

}

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Util::File - File related utility functions.

=head1 SYNOPSIS

   use Newcomen::Util::File;

   # Parse a file:
   my (meta_data, $content) = Newcomen::Util::File::parse_file ($file);

   # Get stat info:
   my $stat = Newcomen::Util::File::stat_hash ($file);

=head1 DESCRIPTION

This package provides a few utility functions dealing with files. Note that all functions may be
imported into the package using I<Newcomen::Util::File>:

   use Newcomen::Util::File qw( parse_file stat_hash );

No function is exported by default.

=head1 FUNCTIONS

=head2 parse_file

   my ($meta_data, $content) = parse_file ($file);

This function extracts the meta data and content from the file specified by its name as the only
parameter. The return value will be a list with two elements, the first one being the meta data (in
a hashref), the second one the actual content (a string).

The file has to be encoded in UTF-8 format, it will be opened with the IO layer
C<< '<:encoding(UTF-8)' >>.

If a file contains meta data, it has to be at the beginning of the file, separated from the content
by three dashes (C<'---'>) or equals signs (C<'==='>) on a line by themselves (leading or trailing
spaces are not allowed). The meta data has to be in YAML format, and the YAML document must be an
associative list (other data structures within this list are allowed), or empty.

Everything after the meta data end marker (C<'---'> or C<'==='>) will be considered content and is
not parsed by the YAML parser, but returned as is with leading and trailing whitespace stripped from
the string.

If no meta data end marker can be found in the file, it is assumed that the file does not contain
any meta data, only actual content. The meta data returned will be an empty hashref in that case.

A valid example, including meta data:

   title:  The document's title
   author: Some Author

   ===

   This is the actual document's content.

This method will I<die()> if the file can not be read, or if the meta data format can not be parsed
properly.

=head2 stat_hash

   my $stat = stat_hash ($file);

Simple wrapper around Perl's I<stat()> function. It returns a hashref containing the information.
The keys are: C<dev>, C<ino>, C<mode>, C<nlink>, C<uid>, C<gid>, C<rdev>, C<size>, C<atime>,
C<mtime>, C<ctime>, C<blksize> and C<blocks>.

=head1 VERSION

This is version C<2017033001>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2017 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
