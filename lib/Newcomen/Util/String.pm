# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Util::String;
our $VERSION = 2014052501;

use strict;
use warnings;
use Data::SimplePath;
use Exporter ();

our @ISA       = qw( Exporter );
our @EXPORT_OK = qw( replace split_csv split_ssv );

sub replace {
   my $string = shift;
   my $values = shift;
   my $spvals = Data::SimplePath -> new ($values);
   while (my @vars = $string =~ /<([^>]+)>/g) {
      for my $var (@vars) {
         my $replacement = replace ($spvals -> get ($var) // '', $values);
         $string =~ s/<\Q$var\E>/$replacement/g;
      }
   }
   return $string;
}

sub split_csv { my @parts = split /\s*,\s*/, shift; return grep { $_ ne '' } @parts; }
sub split_ssv { my @parts = split /\/+/,     shift; return grep { $_ ne '' } @parts; }

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Util::String - String related utility functions.

=head1 SYNOPSIS

   use Newcomen::Util::String;

   # Replace <stuff> in the string:
   my $result = Newcomen::Util::String::replace ($string, $hashref);

   # Split strings on comma or slash:
   my @parts_csv = split_csv ($string);
   my @parts_ssv = split_ssv ($string);

=head1 DESCRIPTION

This package provides a few utility functions dealing with strings. Note that all functions may be
imported into the package using I<Newcomen::Util::String>:

   use Newcomen::Util::String qw( replace split_csv split_ssv );

No function is exported by default.

=head1 FUNCTIONS

=head2 replace

   my $result = replace ($string, $hashref);

This function replaces all occurrences of C<< <key> >> in the string (first parameter) by their
values specified in the hashref (second parameter) and returns the resulting string.

For example, the (sub)string C<< '<foo>' >> will be replaced by C<< $hashref -> {'foo'} >>. The
function will process the string until no more C<< m/<.+>/ >> are found. If a I<key> does not exist
in the hashref, C<< '<key>' >> will be replaced by an empty string. If a replacement value itself
contains variable placeholders, these will be replaced recursively, too. There are no checks to
prevent infinite recursion, so be careful in that case. The key must not contain a C<< '>' >>
character.

Note that the hashref may be a complex data structure, e.g. a hash of hashes. L<Data::SimplePath> is
used to access the values. For example, the string C<< '<foo/bar>' >> would be replaced by the value
of C<< $hashref -> {'foo'} {'bar'} >> (as returned by L<Data::SimplePath>'s I<get()> method).

=head2 split_csv

   my @parts = split_csv ($string);

Splits the specified string at commas and returns the resulting list, excluding any empty strings.
Spaces around the commas will be stripped. E.g. splitting the string C<'x, y, , z'> would return the
list C<('x', 'y', 'z')>.

=head2 split_ssv

   my @parts = split_ssv ($string);

Splits the specified string at slashes and returns the resulting list, excluding any empty strings,
e.g. splitting the string C<'/x/y//z/'> would return the list C<('x', 'y', 'z')>.

=head1 SEE ALSO

L<Data::SimplePath>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
