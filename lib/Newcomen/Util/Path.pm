# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Util::Path;
our $VERSION = 2014101201;

use strict;
use warnings;
use Exporter ();

our @ISA       = qw( Exporter );
our @EXPORT_OK = qw( resolute contains_dir contains_file );

sub contains_dir {

   my $parent = shift;
   my $child  = shift;

   return ($parent -> contains ($child) and -d $child -> stringify ());

}

sub contains_file {

   my $parent = shift;
   my $child  = shift;

   return ($parent -> contains ($child) and -f $child -> stringify ());

}

sub resolute { $_ [0] -> resolve () -> absolute () }

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Util::Path - Path related utility functions.

=head1 SYNOPSIS

   use Newcomen::Util::Path;

   # Get the resolved absolute path:
   my $abs_path = Newcomen::Util::Path::resolute ($path);

   # Check if some file is located beneath this path:
   if (contains_file ($abs_path, $file)) {
      # do something...
   }

=head1 DESCRIPTION

This package provides utility functions dealing with paths. Note that all functions may be imported
into the package using I<Newcomen::Util::Path>:

   use Newcomen::Util::List qw( contains_dir contains_file resolute );

No function is exported by default.

=head1 FUNCTIONS

=head2 contains_dir, contains_file

   if (contains_dir ($ancestor, $descendant)) {
      # do something...
   }

   # Same for contains_file()...

These functions will check if the directory or file represented by the L<Path::Class> instance
specified as the second parameter is a direct or indirect child of the directory represented by the
L<Path::Class::Dir> instance specified as the first parameter. For both directories and files it
will be checked if they actually exist on the file system.

=head2 resolute

   my $abs_path = resolute ($path);

Expects a L<Path::Class::Dir> or L<Path::Class::File> instance as first and only parameter, and
returns a new instance with the path resolved and converted to an absolute path, by chaining the two
L<Path::Class> methods I<resolve()> and I<absolute()>. See L<Path::Class::Dir> and/or
L<Path::Class::File> for details.

=head1 SEE ALSO

L<Path::Class>, L<Path::Class::Dir>, L<Path::Class::File>

=head1 VERSION

This is version C<2014101201>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
