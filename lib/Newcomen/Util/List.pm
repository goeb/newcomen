# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Util::List;
our $VERSION = 2014052501;

use strict;
use warnings;
use Exporter ();

our @ISA       = qw( Exporter );
our @EXPORT_OK = qw( dbl zip );

sub dbl {     map { ($_)         x 2 }       @_           }
sub zip { @_ [map {  $_, $_ + @_ / 2 } 0 .. (@_ / 2 - 1)] }

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Util::List - List related utility functions.

=head1 SYNOPSIS

   use Newcomen::Util::List;

   # Create a list with all elements of the source list doubled:
   my @doubled = Newcomen::Util::List::dbl (@list);

   # Zip two lists:
   my @zipped = Newcomen::Util::List::zip (@list_1, @list_2);

=head1 DESCRIPTION

This package provides a few utility functions dealing with lists. Note that all functions may be
imported into the package using I<Newcomen::Util::List>:

   use Newcomen::Util::List qw( dbl zip );

No function is exported by default.

=head1 FUNCTIONS

=head2 dbl

   my @doubled = dbl (@list);

Returns a list that contains all elements of the input list twice, i.e. if the input list is
C<(1, 2, 3)>, the resulting list will be C<(1, 1, 2, 2, 3, 3)>.

=head2 zip

   my @zipped = zip (@list_1, @list_2);

Zips the two input lists. For example, zipping the lists C<('a', 'b', 'c')> and C<(1, 2, 3)> will
result in the list C<('a', 1, 'b', 2, 'c', 3)>.

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
