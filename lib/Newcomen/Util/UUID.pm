# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Util::UUID;
our $VERSION = 2014052501;

use strict;
use warnings;
use Exporter ();
use UUID::Tiny ();

our @ISA       = qw( Exporter );
our @EXPORT_OK = qw( uuid_v5  );

sub uuid_v5 {

   my $namespace = shift;
   my $string    = shift;

   return UUID::Tiny::create_uuid_as_string (UUID::Tiny::UUID_V5, $namespace, $string);

}

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Util::UUID - UUID related utility functions.

=head1 SYNOPSIS

   use Newcomen::Util::UUID;

   # Generate a version 5 UUID:
   my $uuid = Newcomen::Util::UUID::uuid_v5 ($namespace, $string);

=head1 DESCRIPTION

This package provides utility functions dealing with UUIDs. Note that all functions may be imported
into the package using I<Newcomen::Util::UUID>:

   use Newcomen::Util::UUID qw( uuid_v5 );

No function is exported by default.

=head1 FUNCTIONS

=head2 uuid_v5

   my $uuid = uuid_v5 ($namespace, $string);

Generates a version 5 (SHA-1-based) UUID, generated from the namespace (first parameter, this has to
be a valid UUID string) and an arbitrary string (second parameter). Currently uses L<UUID::Tiny> to
do the work. Returns the string representation of the generated UUID.

B<Note:> No error checking is performed, make sure the first parameter is valid!

=head1 SEE ALSO

L<UUID::Tiny>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
