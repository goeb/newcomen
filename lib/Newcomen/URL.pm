# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::URL;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;
use Newcomen::Data;

has '_map'     => (
   'is'        => 'ro',
   'isa'       => 'Newcomen::Data',
   'init_arg'  => undef,
   'default'   => sub { Newcomen::Data -> new () },
   'handles'   => {
      'map'    => 'data',
      'delete' => 'delete',
      'exists' => 'exists',
      'get'    => 'get',
      'merge'  => 'merge',
      'set'    => 'set',
   }
);

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::URL - Manages an URL map.

=head1 SYNOPSIS

   use Newcomen::URL;

   my $urls = Newcomen::URL -> new ();

   # Define an URL:
   $urls -> set (['some', 'index'], 'some/index.html');

   # Retrieve an URL:
   my $url = $urls -> get (['some', 'index']);

=head1 DESCRIPTION

I<Newcomen::URL> provides a storage to map arbitrary (and maybe nested) keys to an URL. Right now it
is really only a wrapper around L<Newcomen::Data>, and does nothing more than that (it does in fact
less, since the interface doesn't even provide all L<Newcomen::Data> methods).

=head1 CLASS METHODS

=head2 new

   my $urls = Newcomen::URL -> new ();

Constructor. Expects no parameters. The URL map will be empty initially.

=head1 INSTANCE METHODS

The following methods are available and behave exactly like their L<Newcomen::Data> counterparts:
I<delete()>, I<exists()>, I<get()>, I<merge()>, I<set()>. Additionally, the I<Newcomen::URL> method
I<map()> is mapped to L<Newcomen::Data>'s L<data()|Newcomen::Data/data> method. There is no method
to access the L<Newcomen::Data> instance directly.

=head1 SEE ALSO

L<Newcomen::Data>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
