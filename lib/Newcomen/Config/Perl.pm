# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Config::Perl;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;

extends 'Newcomen::Config::Backend';

override '_do_config' => sub {

   my $self = shift;
   my $file = shift -> get (['file']) // confess 'No configuration file specified';

   do $file or confess "Could not load configuration file $file";

};

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Config::Perl - Configuration backend to read a Perl configuration file.

=head1 SYNOPSIS

   use Newcomen::Config;

   my $config = Newcomen::Config -> new ();

   # Use the Newcomen::Config::Perl backend to load user configuration:
   $config -> add_config ('perl', { 'file' => 'config/config.pl' });

=head1 DESCRIPTION

Configuration backends are usually not called directly. See L<Newcomen::Config> for the frontend
documentation.

This configuration backend will read a configuration file written in Perl. The file has to return a
hashref containing the user configuration. The following example would be a valid configuration
file:

   {
      plugins => {
         'Newcomen' => 1,
      },
   };

=head2 Warning

B<<
The Perl code in the configuration file will be executed. Any valid Perl command may be run. Never
use a configuration file from an untrusted source without checking it first. You have been warned!
>>

=head1 OPTIONS

The only option accepted is I<file> (see L<SYNOPSIS|/SYNOPSIS> for an example), which must contain
the location of the configuration file to be used. The location may be an absolute or - preferably -
a relative path. If no path is specified, if the file can not be read or is invalid for some reason,
the backend will I<die()>.

=head1 CLASS METHODS

=head2 new

   my $backend = Newcomen::Config::Perl -> new ();

Constructor, expects no parameters. See L<Newcomen::Config::Backend>'s
L<new()|Newcomen::Config::Backend/new> method for a full description.

=head1 INSTANCE METHODS

=head2 config

   my $config = $backend -> config ($backend_options);

Returns the configuration hashref that is returned by the configuration file. The file will be
processed using Perl's I<do()> function. If this fails for some reason, this backend will I<die()>.
I<config()> expects the backend options (either an L<Newcomen::Data> instance or C<undef>) to be
supplied as the only parameter, valid backend options and their default values are described
L<above|/OPTIONS>.

=head1 SEE ALSO

L<Newcomen::Config>, L<Newcomen::Config::Backend>, L<Newcomen::Data>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
