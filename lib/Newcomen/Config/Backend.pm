# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Config::Backend;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;
use Newcomen::Data;
use Newcomen::Util qw( is_a );

sub config {

   my $self = shift;
   my $opts = shift // Newcomen::Data -> new ();

   confess 'Usage: config($options)' unless is_a ($opts, 'Newcomen::Data') and not @_;

   my $config = $self -> _do_config ($opts);
   confess 'No hashref returned by configuration backend' unless $config and ref $config eq 'HASH';

   return $config;

}

sub _do_config { confess '_do_config() not implemented by backend' }

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Config::Backend - Base class for configuration backends.

=head1 SYNOPSIS

   package Newcomen::Config::MyBackend;

   use namespace::autoclean;
   use Moose;

   extends 'Newcomen::Config::Backend';

   override '_do_config' => sub {

      my $self    = shift;
      my $options = shift;
      my $config  = {};

      # Get configuration...

      return $config;

   };

   __PACKAGE__ -> meta () -> make_immutable ();

   1;

=head1 DESCRIPTION

This is the base class to be extended by configuration backends. It does not define any attributes.
It does define the methods L<config()|/config> and L<_do_config()|/_do_config>. L<config()|/config>
will call the L<_do_config()|/_do_config> method, which has to be overridden by the actual backend
implementation (see L<SYNOPSIS|/SYNOPSIS>). The L<_do_config()|/_do_config> method of the base class
will simply I<die()>!

=head1 CLASS METHODS

=head2 new

   my $backend = Newcomen::Config::MyBackend -> new ();

Constructor. I<new()> will be called once for every (!) backend found when the configuration is
first accessed. A backend does not have to be actually used to be instantiated! If it is actually
used to load some user configuration, L<config()|/config> will be called (and in turn call
L<_do_config()|/_do_config>). Only this one backend instance will be used for every call for a
specific backend. The constructor will be called without any parameters.

=head1 INSTANCE METHODS

=head2 config

   my $config = $backend -> config ($backend_options);

The I<config()> method of a backend will be called when L<Newcomen::Config>'s
L<add_config()|Newcomen::Config/add_config> method is called. See there for details on that method.
It will return a hashref that contains the user configuration to be added to the
L<Newcomen::Config> instance. The backend options passed to
L<add_config()|Newcomen::Config/add_config> will be passed to the I<config()> method as the only
parameter. This must be an L<Newcomen::Data> instance or C<undef>. The I<config()> method must not
be overridden by a backend implementation!

=head1 MISCELLANEOUS

=head2 _do_config

The actual backends must implement a I<_do_config()> method, I<_do_config()> of this base class will
I<die()>. The L<Newcomen::Data> instance containing the options (or a newly created empty one, if no
options were supplied) will be passed as the only parameter to this method. I<_do_config()> has to
return a hashref containing the user configuration to be added to the configuration instance (the
hashref may be empty).

=head1 SEE ALSO

L<Newcomen::Config>, L<Newcomen::Data>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
