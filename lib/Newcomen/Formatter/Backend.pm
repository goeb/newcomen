# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Formatter::Backend;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;
use Newcomen::Data;
use Newcomen::Util qw( is_a );

sub format {

   my $self    = shift;
   my $text    = shift;
   my $opts    = shift;
   my $content = shift;
   my $page    = shift;
   my $index   = shift;

   confess 'Usage: format($text,$options,$content,$page,$index)' unless
      not @_ and defined $text and ref $text eq '' and defined $index and $index =~ /^\d+$/
      and is_a ($opts,    'Newcomen::Data')
      and is_a ($content, 'Newcomen::Content')
      and is_a ($page,    'Newcomen::Page');

   $text = $self -> _do_format ($text, $opts, $content, $page, $index);
   confess 'No string returned by formatter backend' unless defined $text and ref $text eq '';

   return $text;

}

sub unique_id {

   my $self = shift;
   my $id   = $self -> _do_unique_id (@_);

   if (defined $id) {
      confess "Formatter's unique ID invalid" unless ref $id eq '' and $id =~ /^[a-z0-9_-]+$/ai;
   }

   return $id;

}

sub _do_unique_id { return undef }

sub _do_format { confess '_do_format() not implemented by backend' }

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Formatter::Backend - Base class for formatter backends.

=head1 SYNOPSIS

   package Newcomen::Formatter::MyBackend;

   use namespace::autoclean;
   use Moose;

   extends 'Newcomen::Formatter::Backend';

   override '_do_format' => sub {

      my $self    = shift;
      my $text    = shift;
      my $options = shift;
      my $content = shift;
      my $page    = shift;
      my $index   = shift;

      # Format text...

      return $text;

   };

   override '_do_unique_id' => sub {

      # Same parameters as above...

      # Calculate the ID...

      return $id;

   };

   __PACKAGE__ -> meta () -> make_immutable ();

   1;

=head1 DESCRIPTION

This is the base class to be extended by formatter backends. It does not define any attributes.

It does define the methods L<format()|/format> and L<_do_format()|/_do_format>. L<format()|/format>
will call the L<_do_format()|/_do_format> method, which has to be overridden by the actual backend
implementation (see L<SYNOPSIS|/SYNOPSIS>). The L<_do_format()|/_do_format> method of the base class
will simply I<die()>!

It also defines the methods L<unique_id()|/unique_id> and L<_do_unique_id()|/_do_unique_id>. Again,
the former will automatically call the latter, which has to be overridden by the backend
implementations (see L<SYNOPSIS|/SYNOPSIS>). These methods are used by the frontend (see
L<Newcomen::Formatter>) for caching the formatter results. L<_do_unique_id()|/_do_unique_id> of the
base class will return C<undef>, which will disable caching for the backend.

=head1 CLASS METHODS

=head2 new

   my $backend = Newcomen::Formatter::MyBackend -> new ();

Constructor, expects no parameters. I<new()> will be called once for every (!) backend found. A
backend does not have to be actually used to be instantiated! If it is actually used,
L<format()|/format> will be called. The one backend instance will be used to format all content.

=head1 INSTANCE METHODS

=head2 format

   $text = $backend -> format ($text, $backend_options, $content, $page, $index);

The I<format()> method of a backend will be called when a piece of text should be formatted. It will
return the formatted text. The original text to be formatted must be passed as first parameter, the
backend options as second parameter (an L<Newcomen::Data> instance), the L<Newcomen::Content>
instance as third parameter, the L<Newcomen::Page> instance as fourth parameter and the index of the
content on the page as fifth parameter.

The I<format()> method must not be overridden by a backend implementation!

=head2 unique_id

   $id = $backend -> unique_id ($text, $backend_options, $content, $page, $index);

The I<unique_id()> method will be called by the L<formatter frontend|Newcomen::Formatter> to
retrieve a unique ID for a specific formatting operation. The unique ID may be derived from the
backend options, page target, meta data, etc., but usually not from the text itself. If the backend
option returns this unique ID, it may be used to cache the formatted text. The backend ensures that
for the same unique ID the results of the formatting itself are always the same for the same input
text. A backend may return C<undef>, which means no caching should be used. If a unique ID is
returned, it will consist only of the characters C<[A-Za-z0-9_-]>, it will not be an empty string.

=head1 MISCELLANEOUS

=head2 _do_format

The actual backends must implement a I<_do_format()> method, I<_do_format()> of this base class will
I<die()>. The parameters are the same as for the L<format()|/format> method described above.

I<_do_format()> has to return a string containing the formatted text.

=head2 _do_unique_id

If a backend wants its formatting results to be cached, it has to implement the I<_do_unique_id()>
method. This must return an ID used to identify a specific formatting operation, based on the
supplied parameters. These parameters are the same as described for L<unique_id()|/unique_id> above,
see there for more details.

=head1 SEE ALSO

L<Newcomen::Content>, L<Newcomen::Data>, L<Newcomen::Formatter>, L<Newcomen::Page>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
