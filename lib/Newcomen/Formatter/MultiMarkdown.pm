# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Formatter::MultiMarkdown;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;
use Text::MultiMarkdown;
use Newcomen::Util::Hash;
use Newcomen::Util;

extends 'Newcomen::Formatter::Backend';
with    'Newcomen::Role::Attribute::Core';
with    'Newcomen::Role::Attribute::Config';

sub BUILD {
   shift -> _config () -> add_default ({
      'formatter'        => {
         'multimarkdown' => {
            'use_cache'  => 1,
         },
      },
   });
}

override '_do_unique_id' => sub {

   my $self    = shift;
   my $text    = shift;
   my $options = shift;
   my $content = shift;
   my $page    = shift;
   my $index   = shift;

   my $config = $self -> _config () -> get (['formatter', 'multimarkdown']) // {};
   $config = Newcomen::Util::Hash::merge ($config, $options -> data ());

   return undef if exists $config -> {'use_cache'} and not $config -> {'use_cache'};
   delete $config -> {'use_cache'};

   return Newcomen::Util::data_sha1 ($config);

};

override '_do_format' => sub {

   my $self    = shift;
   my $text    = shift;
   my $options = shift;
   my $content = shift;
   my $page    = shift;
   my $index   = shift;

   my $config = $self -> _config () -> get (['formatter', 'multimarkdown']) // {};
   $config = Newcomen::Util::Hash::merge ($config, $options -> data ());

   delete $config -> {'use_cache'};

   $text = Text::MultiMarkdown::markdown ($text, $config);

   $text =~ s!<p>(\s*<(?:address|article|aside|footer|header))!$1!igms;
   $text =~ s!(</(?:address|article|aside|footer|header)>\s*)</p>!$1!igms;

   return $text;

};

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Formatter::MultiMarkdown - MultiMarkdown formatter.

=head1 DESCRIPTION

Formatter backends are usually not called directly. See L<Newcomen::Formatter> for the frontend
documentation.

This module provides a formatter backend to apply MultiMarkdown formatting to a piece of text. It
uses L<Text::MultiMarkdown> to do the actual formatting.

B<Note:> The formatted text will be post-processed to remove C<< <p> >> tags around the following
HTML tags:

C<< <address> <article> <aside> <footer> <header> >>

A simple search/replace regular expression is used to get rid of the C<< <p> >> immediately
preceding or following the aforementioned tags. It may not work in all cases, please file a detailed
bug report if required.

=head1 OPTIONS

   {
      'formatter'        => {
         'multimarkdown' => {
            'use_cache'  => 1,
         },
      },
   }

These are the default options set by this backend. They may be overridden by user configuration.

The I<use_cache> option (defaults to C<1>) may be used to disable the caching for this formatter
backend - if required - by setting it to a false value.

Apart from that, the I<formatter/multimarkdown> hashref may contain any options supported by
L<Text::MultiMarkdown>. For example:

   {
      'formatter'                   => {
         'multimarkdown'            => {
            'empty_element_suffix'  => '>',
            'tab_width'             => 2,
            'use_wikilinks'         => 1,
         },
      },
   }

The I<formatter/multimarkdown> hashref will be passed to L<markdown()|Text::MultiMarkdown/markdown>
(of the L<Text::MultiMarkdown> module) as specified in the configuration, with the I<use_cache> key
deleted if it was included. If I<formatter/multimarkdown> is C<undef>, an empty hashref will be
used, but you should only set it to C<undef> if you know what you are doing.

=head1 CACHING

Results of this backend may be cached by the L<formatter frontend|Newcomen::Formatter>. The
formatting only depends on the L<options|/OPTIONS>, for effective caching the same options should be
used for all pages. Note that even if L<Text::MultiMarkdown>'s behaviour does not change for some
options (for example, when including default values in the options hashref), different options will
cause separate cached values, so don't do this. Keeping this in mind, it is save to include this
formatter early in the formatting chain as far as the caching is concerned.

Caching may be disabled for this backend (see L<above|/OPTIONS>), it may also be disabled globally
(see L<Newcomen::Formatter>).

=head1 CLASS AND INSTANCE METHODS

See L<Newcomen::Formatter::Backend>.

=head1 SEE ALSO

L<Text::MultiMarkdown>, L<Newcomen::Content>, L<Newcomen::Formatter>,
L<Newcomen::Formatter::Backend>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
