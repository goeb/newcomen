# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Formatter::Specification::List;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::Aliases;
use MooseX::StrictConstructor;
use Newcomen::Formatter::Specification;
use Newcomen::Util qw( is_a );
use Newcomen::Util::Traits;

has '_formatters' => (
   'is'           => 'ro',
   'isa'          => 'ArrayRef[Newcomen::Formatter::Specification]',
   'init_arg'     => undef,
   'default'      => sub {[]},
   'traits'       => ['Array'],
   'handles'      => Newcomen::Util::Traits::std_array_handles (),
);

alias 'formatter'  => 'item';
alias 'formatters' => 'items';

sub add {

   my $self = shift;

   for my $arg (@_) {

      my $formatters = ref $arg eq 'ARRAY' ? $arg : [$arg];

      for my $formatter (@$formatters) {

         next unless $formatter and (ref $formatter eq '' or ref $formatter eq 'HASH');

         my $name = ref $formatter eq '' ? $formatter : $formatter -> {'name'   };
         my $opts = ref $formatter eq '' ? {}         : $formatter -> {'options'} // {};

         next unless $name and (ref $opts eq 'HASH' or is_a ($opts, 'Newcomen::Data'));

         unless ($self -> find (sub { $_ -> name () eq $name })) {
            my $spec = Newcomen::Formatter::Specification -> new ('name' => $name);
            $spec -> options () -> merge ($opts) if $opts;
            $self -> push ($spec);
         }

      }

   }

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Formatter::Specification::List - A list of formatter backend specifications.

=head1 SYNOPSIS

   use Newcomen::Formatter::Specification::List;

   my $formatters = Newcomen::Formatter::Specification::List -> new ();

   # Add some formatters:
   $formatters -> add ($some_formatter, $another_formatter);

   # Get formatter with index 5:
   my $formatter = $formatters -> formatter (5);

=head1 DESCRIPTION

This class manages an ordered list of L<Newcomen::Formatter::Specification> instances. It is used to
set the formatters of an L<Newcomen::Content> instance. See the documentation of these two classes
for more details. Methods in this class all operate on the formatter specification list. The order
of items in the list is important, as formatters will be run in this order.

=head1 CLASS METHODS

=head2 new

   my $formatters = Newcomen::Formatter::Specification::List -> new ();

Constructor. Expects no parameters. The formatter specification list will be empty initially.

=head1 INSTANCE METHODS

I<Newcomen::Formatter::Specification::List> uses an arrayref to store the
L<Newcomen::Formatter::Specification> instances as an ordered list. It uses the
L<array methods|Newcomen::Util::Traits/Array_Methods> of L<Newcomen::Util::Traits>, with the
following additions:

=over

=item *

The method I<formatter()> is an alias for the L<item()|Newcomen::Util::Traits/item> method.

=item *

The method I<formatters()> is an alias for the L<items()|Newcomen::Util::Traits/items> method.

=back

Please see L<Newcomen::Util::Traits> for a complete list of methods and more details.

=head2 add

   # Add formatters by name:
   $formatters -> add ('MyFormatter', 'MyOtherFormatter');

   # This works, too:
   $formatters -> add (['MyFormatter', 'MyOtherFormatter']);

   # Or supply name and options:
   $formatters -> add ({
      'name'    => 'MyFormatter',
      'options' => { 'option' => 'value' },
   });

Adds one or more new formatter specifications to the end of the list.

An arbitrary number of parameters may be specified. Each parameter must either be a string
specifying the formatter name, a hashref with at least the I<name> key to specify the formatter
name and an optional I<options> key to specify formatter options (hashref or L<Newcomen::Data>
instance), or an arrayref containing one or more of the aforementioned names or hashrefs. The
L<Newcomen::Formatter::Specification> instance will be created automatically from the supplied data.
If a formatter specification by that name is already included in the list, this method will not add
it again.

The return value of this method is not defined.

=head1 SEE ALSO

L<Newcomen::Content>, L<Newcomen::Data>, L<Newcomen::Formatter>,
L<Newcomen::Formatter::Specification>, L<Newcomen::Util::Traits>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
