# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Formatter::Specification;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;

with 'Newcomen::Role::Backend::Specification';

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Formatter::Specification - A formatter backend specification.

=head1 SYNOPSIS

   use Newcomen::Formatter::Specification;

   my $spec = Newcomen::Formatter::Specification -> new (
      'name' => $backend_name
   );

   # Set some option:
   $spec -> set (['some', 'option'], 'value');

=head1 DESCRIPTION

A formatter backend specification contains a formatter backend name and its options. This is for
example used in L<Newcomen::Content> instances to store the formatters that should be used for the
content.

=head1 CLASS METHODS

=head2 new

   my $spec = Newcomen::Formatter::Specification -> new (
      'name' => $backend_name
   );

Constructor. The name parameter is mandatory, it specifies the formatter backend by its basename
(i.e. the module name without the C<'Newcomen::Formatter::'> prefix). The name can not be changed
later on. I<Newcomen::Formatter::Specification> does not check if a backend by the specified name
exists.

=head1 INSTANCE METHODS

Please see L<Newcomen::Role::Backend::Specification> for a list of instance methods and their
description.

=head1 SEE ALSO

L<Newcomen::Content>, L<Newcomen::Data>, L<Newcomen::Role::Backend::Specification>,
L<Newcomen::Formatter>, L<Newcomen::Formatter::Specification::List>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
