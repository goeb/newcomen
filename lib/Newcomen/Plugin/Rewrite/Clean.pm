# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::Rewrite::Clean;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;
use Newcomen::Util::String;

extends 'Newcomen::Plugin::Base';

sub hook_rewrite_target {

   my $self    = shift;
   my $target  = shift;
   my $creator = shift;
   my @parts   = Newcomen::Util::String::split_ssv ($target);
   my @clean   = ();

   for my $part (@parts) {

      $part =~ s/[^a-zA-Z0-9._-]/-/ag;
      $part =~ s/-+\.|\.-+/./g;
      $part =~ s/\.+/./g;
      $part =~ s/-+/-/g;
      $part =~ s/^-+|[.-]+$//g;

      confess "Invalid target: $target" unless $part;
      push @clean, $part;

   }

   return join ('/', @clean);

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::Rewrite::Clean - Clean up page targets.

=head1 DESCRIPTION

This plugin will clean up the page targets. It will first split the supplied target at slashes,
ignoring any empty parts (!). Then, for every singe part of the split string, the following rules
will be applied:

=over

=item *

All characters other than C<[a-zA-Z0-9._-]> will be replaced by a single dash (C<->).

=item *

All dashes (C<->) before or after dots (C<.>) will be removed.

=item *

Multiple consecutive dashes (C<->) or dots (C<.>) will be replaced by single ones.

=item *

All dashes (C<->) at the beginning and all dashes or dots (C<.>) at the end of the part will be
removed. A dot at the beginning is allowed (this allows, for example, F<.htaccess> files to be
created)!

=back

If a part should be empty after these steps, this plugin will I<die()>. If all parts are cleaned up,
the rewritten target will be the single cleaned up parts joined with slash as separator.

=head1 HOOKS

This plugin implements the I<hook_rewrite_target()> hook (default priority).

=head1 SEE ALSO

L<Newcomen::Page>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
