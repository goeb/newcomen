# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::Rewrite::Prefix;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;

extends 'Newcomen::Plugin::Base';
with    'Newcomen::Role::Attribute::Config';

override '_build_default_config' => sub {{

   'rewrite'   => {
      'prefix' => '',
   }

}};

override '_build_hook_order' => sub {{ 'hook_rewrite_target' => 900 }};

sub hook_rewrite_target {

   my $self    = shift;
   my $target  = shift;
   my $creator = shift;
   my $prefix  = $self -> _config () -> get (['rewrite', 'prefix']) // '';

   return $prefix . $target;

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::Rewrite::Prefix - Set a common prefix for all page targets.

=head1 DESCRIPTION

This plugin may be used to set a common prefix for all page targets. The prefix will be added late
(!) during the I<hook_rewrite_target()> hook, see L<HOOKS|/HOOKS>.

=head1 OPTIONS

   {
      'rewrite'   => {
         'prefix' => '',
      },
   }

These are the default options set by this plugin. They may be overridden by user configuration.

The I<rewrite/prefix> option specifies the prefix for all page targets. By default, this is an empty
string. It may be set to any string, and the value will be prepended to the page targets, without
any separator. The value C<undef> will be treated as an empty string.

=head1 HOOKS

This plugin implements the I<hook_rewrite_target()> hook (priority C<900>).

=head1 SEE ALSO

L<Newcomen::Page>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
