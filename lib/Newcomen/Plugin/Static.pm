# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::Static;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;
use Newcomen::Collection;
use Newcomen::Content;

extends 'Newcomen::Plugin::Base';
with    'Newcomen::Role::Attribute::Catalog';
with    'Newcomen::Role::Attribute::Config';
with    'Newcomen::Role::Attribute::Crawler';
with    'Newcomen::Role::Attribute::URL';
with    'Newcomen::Role::Crawler::Filesystem';

override '_build_default_config' => sub {{

   'static'   => {
      'match' => undef,
      'dirs'  => undef,
   },

}};

sub hook_crawl {

   my $self  = shift;

   my $root  = $self -> _config () -> get (['ROOT_DIR']);
   my $dirs  = $self -> _config () -> get (['static', 'dirs' ]) // [];
   my $match = $self -> _config () -> get (['static', 'match']) // '(?!)';

   return $self -> _crawl_fs ($root, $dirs, sub { -f $_ [0] and $_ [1] =~ /$match/ });

}

sub hook_build_collections {

   my $self = shift;
   my $coll = Newcomen::Collection -> new ('creator' => 'Static', 'id' => 'Static');

   for my $src (grep { $_ -> creator () eq 'Static' } $self -> _crawler () -> sources()) {
      $coll -> push ($src);
   }

   return ($coll);

}

sub hook_build_pages {

   my $self       = shift;
   my $collection = $self -> _catalog () -> collection ('Static');
   my @page_list  = ();

   return unless $collection;

   for my $src ($collection -> sources ()) {

      my $page = Newcomen::Page -> new (
         'target'     => $src -> get (['path', 'source', 'data']),
         'creator'    => 'Static',
         'content'    => [ Newcomen::Content ->  new ('source' => $src) ],
         'collection' => $collection,
      );

      $page -> set (['source_file'], $src -> get (['path', 'source', 'absolute']));

      $self -> _url () -> set (['Static', $src -> id ()], $page -> target ());

      push @page_list, $page;

   }

   return @page_list;

}

sub hook_writer {

   my $self = shift;
   my $page = shift;

   $page -> set_writer ('Copy');

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::Static - Copy static files to the destination directory.

=head1 DESCRIPTION

This plugin will gather source items (L<Newcomen::Source> instances) from the file system, prepare a
collection (an L<Newcomen::Collection> instance) and pages (L<Newcomen::Page> instances) ready to be
written by the L<Newcomen::Writer::Copy> writer backend. This will effectively just copy static
files from one or more source directories to a target directory, while complying to the B<Newcomen>
concepts.

B<Note:> Empty directories will not be copied.

=head2 Sources

Source items will be gathered using the L<_crawl_fs()|Newcomen::Role::Crawler::Filesystem/crawl_fs>
method of L<Newcomen::Role::Crawler::Filesystem> according to the configuration, see
L<OPTIONS|/OPTIONS>. See L<Newcomen::Role::Crawler::Filesystem> for the properties of the source
items, including the meta data included. The creator ID of the items will be set to C<'Static'>.

=head2 Collections

One collection will always be created. Every source item with the ID C<'Static'> will be added, in
no particular order. The collection may be empty if there are no source items. The collection's ID
and creator attributes will both be set to C<'Static'>.

=head2 Pages

One page will be created for every item in the collection with the ID C<'Static'>. Every created
page will contain exactly one content item, referencing the static source item to be copied to the
page's target. The target will be the path of the source file relative to the source's data
directory (not the project's root directory). The creator of the page will be set to C<'Static'>,
and the page's collection attribute will be set to the source collection instance. The writer of all
pages with the creator set to C<'Static'> will be set to the L<Newcomen::Writer::Copy> backend
(without any further options), unless another plugin already set a writer for a page. The page will
include the meta data I<source_file>, set to the absolute path of the source file.

=head2 URLs

Any page's (possibly rewritten) target will be added to the URL map (see L<Newcomen::URL>) under the
key C<< ['Static', $source_id] >>, with the C<$source_id> being the ID of the L<Newcomen::Source>
instance belonging to the page. This ID is set by L<Newcomen::Role::Crawler::Filesystem>'s
L<_crawl_fs()|Newcomen::Role::Crawler::Filesystem/crawl_fs> method, it is the path of the source
file relative to the project's root directory.

=head1 OPTIONS

   {
      'static'   => {
         'match' => undef,
         'dirs'  => undef,
      },
   }

These are the default options set by the plugin. They may be overridden by user configuration.

I<static/match> will be used in a regular expression to decide if a source file should be considered
for inclusion or not. It will be matched against the file name relative to the project's root
directory. All matching files will be included. If this is C<undef> (the default), it will be set to
a never matching regexp, preventing any file from being copied. Set it to C<'.'> to copy every file.

I<static/dirs> must be either a string specifying one directory or an arrayref containing zero or
more strings specifying the directories in which to look for source files. The directories' paths
must be specified relative to the project's root directory. Setting this to an empty arrayref or
C<undef> (the default) will effectively prevent any file from being copied.

=head1 META DATA

=head2 Sources

The source items created by this plugin will include all the information set by the
L<Newcomen::Role::Crawler::Filesystem> role. No additional data will be added.

=head2 Pages

   {
      'source_file' => $source_file,
   }

I<source_file> will be set to the absolute path of the source file of the page (i.e. the file to be
copied to the output directory).

=head1 HOOKS

The following hooks are implemented: I<hook_crawl()>, I<hook_build_collections()>,
I<hook_build_pages()> and I<hook_writer()>. All hooks are using the default hook priority.

=head1 SEE ALSO

L<Newcomen::Collection>, L<Newcomen::Page>, L<Newcomen::Role::Crawler::Filesystem>,
L<Newcomen::Source>, L<Newcomen::URL>, L<Newcomen::Writer::Copy>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
