# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::Blog::Crawler;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;

extends 'Newcomen::Plugin::Base';
with    'Newcomen::Role::Attribute::Config';
with    'Newcomen::Role::Crawler::Filesystem';

override '_build_default_config' => sub {{

   'blog'        => {
      'crawler'  => {
         'dirs'  => undef,
         'match' => undef,
      },
   }

}};

sub hook_crawl {

   my $self  = shift;

   my $root  = $self -> _config () -> get (['ROOT_DIR']);
   my $dirs  = $self -> _config () -> get (['blog', 'crawler', 'dirs' ]) // [];
   my $match = $self -> _config () -> get (['blog', 'crawler', 'match']) // '(?!)';

   return $self -> _crawl_fs ($root, $dirs, sub { -f -T $_ [0] and $_ [1] =~ /$match/ });

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::Blog::Crawler - Filesystem crawler for the blog plugins.

=head1 DESCRIPTION

This plugin provides the crawler for the blog plugins. It will create L<Newcomen::Source> instances
for all files matching the configured pattern found in the configured data directories. See below
for the configuration options and their defaults.

=head2 Sources

Please see L<Newcomen::Role::Crawler::Filesystem> for the properties of the source items created.
The sources' creator ID will be set to C<'Blog::Crawler'>.

=head1 OPTIONS

   {
      'blog'        => {
         'crawler'  => {
            'dirs'  => undef,
            'match' => undef,
         },
      },
   }

These are the default options set by the plugin, they may be overridden by user configuration.

I<blog/crawler/dirs> must either be a single string specifying one directory, or an arrayref of
strings specifying zero or more directories in which to look for data sources. Directory names must
be relative to the project's root. If this is C<undef> (the default), it will be set to an empty
arrayref (and no sources will be created).

I<blog/crawler/match> must be a string, it will be used in a regular expression and must match
the path of the source file relative to the project's root directory. Setting this to C<undef> (the
default) means no files will be included.

B<Note:> For a file to be included, not only must it match the regular expression, it must also pass
Perl's file tests I<-f> and I<-T>, the latter one being a test for text files. Also note that empty
files will not be filtered out at this point.

=head1 META DATA

=head2 Sources

The source items created by this plugin will include all the information set by the
L<Newcomen::Role::Crawler::Filesystem> role. No additional data will be added.

=head1 HOOKS

This plugin implements the I<hook_crawl()> hook (default priority).

=head1 SEE ALSO

L<Newcomen::Role::Crawler::Filesystem>, L<Newcomen::Source>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
