# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::Blog::Single;
our $VERSION = 2014052501;

use namespace::autoclean;
use Clone;
use Data::Rmap ();
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;
use Newcomen::Collection;
use Newcomen::Content;
use Newcomen::Page;
use Newcomen::Util::Hash qw( merge );
use Newcomen::Util::String qw( replace );

extends 'Newcomen::Plugin::Base';
with    'Newcomen::Role::Attribute::Catalog';
with    'Newcomen::Role::Attribute::Config';
with    'Newcomen::Role::Attribute::Crawler';
with    'Newcomen::Role::Attribute::URL';

override '_build_default_config' => sub {{

   'blog'             => {
      'single'        => {
         'page_info'  => {},
         'url'        => undef,
         'formatters' => undef,
         'renderer'   => undef,
         'writer'     => undef,
      },
   }

}};

sub hook_build_collections {

   my $self = shift;
   my $coll = Newcomen::Collection -> new ('creator' => 'Blog::Single', 'id' => 'Blog::Single');

   for my $src (grep { $_ -> creator () eq 'Blog::Crawler' } $self -> _crawler () -> sources()) {
      $coll -> push ($src);
   }

   return ($coll);

}

sub hook_build_pages {

   my $self    = shift;
   my $url_tpl = $self -> _config () -> get (['blog', 'single', 'url']) // '';
   my $coll    = $self -> _catalog () -> collection ('Blog::Single');
   my @pg_lst  = ();

   return () unless $coll;

   for my $src ($coll -> sources ()) {

      my $url   = undef;
      my $title = undef;
      my $tpl   = $src -> exists (['single_url']) ? $src -> get (['single_url']) : $url_tpl;
      my $meta  = $src -> meta () -> data ();

      next unless $tpl;

      my $more = {
         'year'  => $src -> get (['time', 'published', 'year' ]) // '',
         'month' => $src -> get (['time', 'published', 'Month']) // '',
         'day'   => $src -> get (['time', 'published', 'Day'  ]) // '',
      };

      my $replacements = merge ($more, $meta);

      $url = replace ($tpl, $replacements);
      next unless $url;

      my $page = Newcomen::Page -> new (
         'target'     => $url,
         'creator'    => 'Blog::Single',
         'content'    => [ Newcomen::Content ->  new ('source' => $src) ],
         'collection' => $coll,
      );

      $src -> set (['single_url'], $page -> target ());

      my $default_info = $self -> _config () -> get (['blog', 'defaults', 'page_info']) // {};
      my $single_info  = $self -> _config () -> get (['blog', 'single',   'page_info']) // {};
      my $merged_info  = merge ($default_info, $single_info);
      Data::Rmap::rmap { $_ = replace ($_, $replacements) } $merged_info;

      $page -> set (["info_$_"  ], $merged_info -> {$_}) for keys %$merged_info;
      $page -> set (['page_type'], 'single');

      $self -> _url () -> set (['Blog::Single', $src -> id ()], $page -> target ());

      push @pg_lst, $page;

   }

   return @pg_lst;

}

sub hook_formatters {

   my $self    = shift;
   my $content = shift;
   my $page    = shift;
   my $index   = shift;

   return unless $page -> creator () eq 'Blog::Page::Single';

   $content -> formatters () -> add (
      $self -> _config () -> get (['blog', 'single', 'formatters']) // []
   );

}

sub hook_renderer {

   my $self = shift;
   my $page = shift;

   $page -> set_renderer ($self -> _config () -> get (['blog', 'single', 'renderer']));

}

sub hook_writer {

   my $self = shift;
   my $page = shift;

   $page -> set_writer ($self -> _config () -> get (['blog', 'single', 'writer']));

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::Blog::Single - Creates single article pages.

=head1 DESCRIPTION

This plugin is responsible for creating the blog's single article pages.

=head2 Sources

This plugin will process all L<Newcomen::Source> instances with the crawler ID C<'Blog::Crawler'>.
The meta data of the source items may include the key I<single_url>. It may be used to override the
global setting I<blog/single/url> on a per-article basis, see L<OPTIONS|/OPTIONS>. If a page is
created for a source item, the I<single_url> meta data will be set (i.e. created, or overridden if
it did already exist).

=head2 Collections

This plugin will create one L<Newcomen::Collection> instance. Its creator ID and its collection ID
will both be set to C<'Blog::Single'>. The collection will include all source items with the crawler
ID C<'Blog::Crawler'>. It may be empty if there are no such source items. No meta data will be set.

=head2 Content

An L<Newcomen::Content> instance will be created for every page, referencing the single blog source
of the page. See below. No meta data will be set for the content instances.

=head2 Pages

One page will be created for every L<Newcomen::Source> instance in the collection with the ID
C<'Blog::Single'> (see above). This will be the single article page for the respective source item.
The page will contain exactly one L<Newcomen::Content> instance, referencing this source item. The
page target, the meta data I<title>, content formatters, page renderer and writer will all be set
according to the configuration, see L<OPTIONS|/OPTIONS>. The creator ID of the page will be
C<'Blog::Single'>. The page's collection attribute will be set to the collection described above.
Another piece of meta data will be set: the key I<page_type>, the value will always be C<'single'>.

An entry will be created in the global URL map (see L<Newcomen::URL>) for every page. The key will
be C<< ['Blog::Single', <source ID>] >>, with I<< <source ID> >> being the ID of the
L<Newcomen::Source> instance belonging to the page.

=head1 OPTIONS

   {
      'blog'             => {
         'single'        => {
            'page_info'  => {},
            'url'        => undef,
            'formatters' => undef,
            'renderer'   => undef,
            'writer'     => undef,
         },
      },
   }

These are the default options set by this plugin. They may be overridden by user configuration.

I<url> specifies the URL for single article pages, this will be used as the target file name. It
must be specified relative to the output directory. This option may contain placeholders in the
format I<< <name> >>. I<name> must be a key in the source item's meta data. All occurrences of
placeholders will be replaced by the meta data value for the specified keys. Nested data structures
(both hashes and arrays) are supported using slashes (C</>) as separators. If a key does not exist
in the page's source item's meta data, it will be replaced by an empty string. Replacement is done
recursively, i.e. the replacement values may again contain placeholders (beware of infinite
recursion, there is no check to avoid this). If the URL is empty after all replacements, the page
will not be created, the same applies if the I<url> configuration setting is C<undef> (the default).

For example, to set the URL's path to the first category (with I<categories> being an arrayref in
the meta data and I<categories/0> being the first element of that array), and the file name to the
source's I<slug> value with a C<'.html'> extension, the following setting could be used:

   {
      'blog'      => {
         'single' => {
            'url' => => '<categories/0>/<slug>.html',
         },
      },
   }

Note that the category list may be empty, in which case the file would be placed in the root (i.e.
the file will be created directly in the output directory).

=over

Note that in addition to the source item's actual meta data, the keys I<year>, I<month> and I<day>
are available for replacement. These will be set to I<time/published/year>, I<time/published/Month>
and I<time/published/Day> (i.e. they are just shortcuts for other meta information), respectively,
unless the key is actually already present in the meta data, in which case the original value will
be available for replacement.

=back

The I<blog/single/page_info> hashref may be used to set arbitrary data that will be included in the
page's meta data. Before processing, this hashref and the hashref of the I<blog/defaults/page_info>
option (see L<Newcomen::Plugin::Blog::Defaults>) will be merged, with I<blog/single/page_info> data
overriding I<blog/defaults/page_info> data if required. The data may contain placeholders (same as
for the I<url> option, see above), these will be replaced (recursively). All data set in this
hashref (and/or the I<defaults> hashref) will be included in the page's meta data, with the original
keys being prefixed with C<'info_'>.

The options I<blog/single/formatters>, I<blog/single/renderer> and I<blog/single/writer> may be used
to specify formatter backends for single page content, and renderer/writer backends for single
pages. The format is the same as described for the options in L<Newcomen::Plugin::Blog::Defaults>,
please see there for details. If these are C<undef> (the default), no backends will be set, and
another plugin may set these (probably L<Newcomen::Plugin::Blog::Defaults>).

=head1 META DATA

=head2 Sources

   {
      'single_url' => $single_url,
   }

Before a page is created, a source's I<single_url> meta data may be used to override the global
setting I<blog/single/url> (for that source's page only). The format is the same as for the option
I<blog/single/url>, see L<OPTIONS|/OPTIONS> above.

After the page is created, I<single_url> will be set to the page target (i.e. the page's URL),
overriding the initial value if it was set.

=head2 Pages

   {
      'page_type' => 'single',
   }

The I<page_type> will always be set to C<'single'> for single article pages. Additionally, all
values set in the I<page_info> configuration hashref (see L<OPTIONS|/OPTIONS>) will be included,
with their keys being prefixed with C<'info_'>.

=head1 HOOKS

This plugin implements the following hooks: I<hook_build_collections()>, I<hook_build_pages()>,
I<hook_formatters()>, I<hook_renderer()> and I<hook_writer()> (all with default priority).

=head1 SEE ALSO

L<Newcomen::Collection>, L<Newcomen::Content>, L<Newcomen::Plugin::Blog::Crawler>,
L<Newcomen::Plugin::Blog::Defaults>, L<Newcomen::Source>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
