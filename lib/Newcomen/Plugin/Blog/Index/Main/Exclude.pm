# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::Blog::Index::Main::Exclude;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;

extends 'Newcomen::Plugin::Base';
with    'Newcomen::Role::Attribute::Config';

override '_build_default_config' => sub {{

   'blog'             => {
      'index'         => {
         'main'       => {
            'exclude' => {},
         },
      },
   }

}};

sub hook_clean_collection {

   my $self       = shift;
   my $source     = shift;
   my $collection = shift;
   my $index      = shift;

   return 1 unless $collection -> creator () eq 'Blog::Index::Main';

   for my $key (keys %{ $self -> _config () -> get (['blog', 'index', 'main', 'exclude']) // {} }) {
      next unless $source -> exists ($key) and defined $source -> get ($key);
      my $regexp = $self -> _config () -> get (['blog', 'index', 'main', 'exclude', $key]);
      return 0 if $source -> get ($key) =~ /$regexp/;
   }

   return 1;

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::Blog::Index::Main::Exclude - Removes articles from the main index page.

=head1 DESCRIPTION

This plugin may be used to remove source items from the main index L<Newcomen::Collection> instance,
i.e. the collection with the creator ID C<'Blog::Index::Main'>. Source instances will be removed
based an meta data. See L<OPTIONS|/OPTIONS> for a description on what meta data will be checked and
how. This plugin works like the L<Newcomen::Plugin::Blog::Index::Exclude> plugin, but only affects
the main index page, not the list index pages.

=head1 OPTIONS

   {
      'blog'             => {
         'index'         => {
            'main'       => {
               'exclude' => {},
            },
         },
      },
   }

These are the default options set by this plugin. They may be overridden by user configuration.

I<blog/index/main/exclude>, if set, must be a hashref. Its keys specify the meta data keys of the
source items to check. Its values will be used as a pattern in a regular expression. If the
configured pattern matches the value of the source's meta data for the specified key, the source
item will be removed from the main index collection. Matching is case sensitive. Nested meta data
structures are supported, the slash (C<'/'>) is used as a separator (e.g. I<published/time/year>
could be used to match against the source items year).

If, for example, all source items with the meta data I<main_index> set to C<'no'> should be removed
from the index, the following may be used:

   {
      'blog'                   => {
         'index'               => {
            'main'             =>
               'exclude'       => {
                  'main_index' => '^no$',
               },
            },
         },
      },
   }

=head1 META DATA

=head2 Sources

The meta data of the L<Newcomen::Source> instances to be checked can be configured freely, see
above.

=head1 HOOKS

This plugin implements the hook I<hook_clean_collection()> (default priority).

=head1 SEE ALSO

L<Newcomen::Collection>, L<Newcomen::Plugin::Blog::Index::Exclude>, L<Newcomen::Source>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
