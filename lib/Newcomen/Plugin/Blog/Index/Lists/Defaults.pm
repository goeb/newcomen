# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::Blog::Index::Lists::Defaults;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;

extends 'Newcomen::Plugin::Base';
with    'Newcomen::Role::Attribute::Config';

override '_build_hook_order' => sub {{

   'hook_formatters' => 400,
   'hook_renderer'   => 600,
   'hook_writer'     => 600,

}};

override '_build_default_config' => sub {{

   'blog'                         => {
      'index'                     => {
         'lists'                  => {
            'defaults'            => {
               'ascending'        => undef,
               'exclude_children' => undef,
               'hierarchical'     => undef,
               'per_page'         => undef,
               'page_info'        => {},
               'formatters'       => undef,
               'renderer'         => undef,
               'writer'           => undef,
            },
         },
      },
   }

}};

sub hook_formatters {

   my $self    = shift;
   my $content = shift;
   my $page    = shift;
   my $index   = shift;

   return unless $page -> creator () =~ /^Blog::Index::Lists(?:::|$)/;

   $content -> formatters () -> add (
      $self -> _config () -> get (['blog', 'index', 'lists', 'defaults', 'formatters']) // []
   );

}

sub hook_renderer {

   my $self = shift;
   my $page = shift;

   $page -> set_renderer (
      $self -> _config () -> get (['blog', 'index', 'lists', 'defaults', 'renderer']),
      qr/^Blog::Index::Lists(?:::|$)/
   );

}

sub hook_writer {

   my $self = shift;
   my $page = shift;

   $page -> set_writer (
      $self -> _config () -> get (['blog', 'index', 'lists', 'defaults', 'writer']),
      qr/^Blog::Index::Lists(?:::|$)/
   );

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::Blog::Index::Lists::Defaults - Sets some defaults for list index pages.

=head1 DESCRIPTION

This plugin will add default formatters for all content on list index pages, and set renderer and
writer for these index pages (unless a renderer/writer is already set).

The default formatter(s) will always be added to all L<Newcomen::Content> instances on a page with a
creator ID starting with C<'Blog::Index::Lists'>, early during the I<hook_formatters()> hook (see
L<HOOKS|/HOOKS>). However, formatters will not be added twice, if a formatter of the same name
already exists in the content's formatter list, it will not be added again.

The default renderer and writer will be set for all L<Newcomen::Page> instances with a creator ID
starting with C<'Blog::Index::Lists'>, unless a renderer or writer is already set for the page. This
will be done late during the I<hook_renderer()>/I<hook_writer()> hook, respectively. See
L<HOOKS|/HOOKS>.

=head1 OPTIONS

   {
      'blog'                         => {
         'index'                     => {
            'lists'                  => {
               'defaults'            => {
                  'ascending'        => undef,
                  'exclude_children' => undef,
                  'hierarchical'     => undef,
                  'per_page'         => undef,
                  'page_info'        => {},
                  'formatters'       => undef,
                  'renderer'         => undef,
                  'writer'           => undef,
               },
            },
         },
      },
   }

These are the default options set by this plugin. They may be overridden by user configuration.

The options I<blog/index/lists/defaults/formatters>, I<blog/index/lists/defaults/renderer> and
I<blog/index/lists/defaults/writer> work like described in L<Newcomen::Plugin::Blog::Defaults>,
please see there for details.

I<blog/index/lists/defaults/ascending> specifies whether to sort the articles in ascending order,
i.e. oldest articles first.

I<blog/index/lists/defaults/per_page> is a default setting for the number of articles per index
page.

I<blog/index/lists/defaults/exclude_children> is the default setting for whether to exclude child
lists on an index page (e.g. articles from subcategories on a category index page).

I<blog/index/lists/defaults/hierarchical> specifies if lists are hierarchical by default.

I<blog/index/lists/defaults/page_info> may contain defaults for additional information to be stored
in the pages' meta data. This has to be a hashref (may be empty).

It is up to the individual index plugins to respect these last five options. They default to
C<undef> (or an empty hashref for the I<page_info> option), meaning that plugins should use the
defaults from I<blog/index/defaults> instead (if applicable), or hard-coded internal defaults.

=head1 HOOKS

This plugin implements the following hooks: I<hook_formatters()> (priority C<400>),
I<hook_renderer()> (priority C<600>) and I<hook_writer()> (priority C<600>).

=head1 SEE ALSO

L<Newcomen::Content>, L<Newcomen::Page>, L<Newcomen::Plugin::Blog::Defaults>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
