# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::Blog::Index::Lists::Date;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;

extends 'Newcomen::Plugin::Base';
with    'Newcomen::Role::Attribute::Catalog';

override '_build_hook_order' => sub {{ 'hook_process_source' => 600 }};

sub hook_process_source {

   my $self = shift;
   my $src  = shift;

   return 1 unless $src -> creator () eq 'Blog::Crawler';

   my $y = $src -> get (['time', 'published', 'year' ]) // '';
   my $m = $src -> get (['time', 'published', 'Month']) // '';
   my $d = $src -> get (['time', 'published', 'Day'  ]) // '';

   next unless $y =~ /^\d{4}$/a and $m =~ /^\d{2}$/a and $d =~ /^\d{2}$/a;

   $src -> set (['time', 'lists', 'day'  ], ["$y/$m/$d"]);
   $src -> set (['time', 'lists', 'month'], ["$y/$m"   ]);
   $src -> set (['time', 'lists', 'year' ], ["$y"      ]);

   return 1;

}

sub hook_post_build_collections {

   my $self = shift;

   for my $coll ($self -> _catalog () -> collections ()) {

      if ($coll -> id () =~ m!^Blog::Index::Lists/!) {

         my $src = $coll -> get (['list_source']);
         my $val = $coll -> get (['list_item'  ]);

         next unless $src and $src =~ m!^/*time/+lists/+(day|month|year)/*$!;
         my $type = $1;

         next unless $val and $val =~ m!^([0-9/]{4,10})$!a;
         my $date = $1;

         my ($y, $m, $d) = split ('/', $date, 3);

         if ($d) {
            $coll -> set (['day'         ], $d        );
            $coll -> set (['month'       ], $m        );
            $coll -> set (['year'        ], $y        );
            $coll -> set (['ymd'         ], "$y/$m/$d");
            $coll -> set (['archive_type'], 'day'     );
         }
         elsif ($m) {
            $coll -> set (['month'       ], $m     );
            $coll -> set (['year'        ], $y     );
            $coll -> set (['ymd'         ], "$y/$m");
            $coll -> set (['archive_type'], 'month');
         }
         else {
            $coll -> set (['year'        ], $y    );
            $coll -> set (['ymd'         ], $y    );
            $coll -> set (['archive_type'], 'year');
         }

         $coll -> set (['archive_source'], $type);

      }

   }

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::Blog::Index::Lists::Date - Helps to creates date archive pages.

=head1 DESCRIPTION

This plugin requires the L<Newcomen::Plugin::Blog::Index::Lists> plugin to be enabled.

This plugin will create the required lists in the meta data of all L<Newcomen::Source> instances
with the crawler ID C<'Blog::Crawler'>, so L<Newcomen::Plugin::Blog::Index::Lists> can be used to
create the yearly/monthly/daily index pages. See the L<OPTIONS|/OPTIONS> for an example.

=head2 Sources

All L<Newcomen::Source> instances with the crawler ID C<'Blog::Crawler'> will be processed. Their
meta data must contain the values for I<time/published/year>, I<time/published/Month> and
I<time/published/Day>, as set by the L<Newcomen::Plugin::Blog::Source::Time> plugin. If any of these
is not set or invalid (just a really basic check), the required list will not be created. For
details on the lists see the L<META DATA|/META DATA> section.

=head2 Collections

There will be meta data added to the collections created by L<Newcomen::Plugin::Blog::Index::Lists>
for the date archive pages. See L<META DATA|/META DATA>.

=head1 OPTIONS

=begin comment

The first { ... } will be extracted automatically for the options summary. Don't remove the
following, else the example will show up on the summary page.

   {
   }

=end comment

B<Note:> It is recommended to read the L<META DATA|/META DATA> section and the documentation of the
L<Newcomen::Plugin::Blog::Index::Lists> plugin first.

There are no default options set for this plugin. See below for a user configuration example.

=head2 Example

   {
      'blog'                         => {
         'index'                     => {
            'lists'                  => {
               'archive'             => {
                  'hierarchical'     => 1,
                  'exclude_children' => 0,
                  'source_list'      => 'time/lists/day',
                  'index'            => {
                     'title'         => 'Archive: <ymd>, Page <page> of <pages>',
                     'url'           => 'archive/<year>/<month>/<day>/index.html',
                  },
                  'pages'            => {
                     'title'         => 'Archive: <ymd>, Page <page> of <pages>',
                     'url'           => 'archive/<year>/<month>/<day>/page-<page>/index.html',
                  },
               },
            },
         },
      },
   }

This example configuration will create daily, monthly and yearly listings. The source list will be
set to C<'time/lists/day'>, and since the I<exclude_children> option is disabled and and the
I<hierarchical> option is set to true, the monthly and yearly index pages will be created from this
list, too.

For URL and title options all the placeholders provided by L<Newcomen::Plugin::Blog::Index::Lists>
are available, and additionally all the collection meta data created by this plugin may be used as
a placeholder, see L<below|/META DATA>.

=head1 META DATA

=head2 Sources

   {
      'time'        => {
         'lists'    => {
            'day'   => [ "$year/$month/$day" ],
            'month' => [ "$year/$month"      ],
            'year'  => [ "$year"             ]
         },
      },
   }

These are the lists required by the L<Newcomen::Plugin::Blog::Index::Lists> plugin to create the
index pages. The month and day values will include a leading zero if the value is less than ten.

The I<time/lists/day> list may be used to generate daily, monthly and yearly archive pages, as
shown in the example above. If different settings for the different page types are required, and
these can not be expressed in the configuration using the provided placeholders, or if only monthly
and/or only yearly archive pages should be created, the other lists may be used as appropriate.

=head2 Collections

   {
      'year'           => $year,
      'month'          => $month,
      'day'            => $day,
      'ymd'            => $ymd,
      'archive_type'   => $archive_type,
      'archive_source' => $archive_source,
   }

I<year>, I<month> and I<day> will be set to the year, month and day of the collection, respectively,
including leading zeroes if required. Note that only daily archive collections will include all
three values. Monthly archive collections won't include the day, and yearly archive collections will
only include the year.

I<ymd> will be set to the values of year, month and day, joined by slashes. Empty parts will not be
included. For example, for the monthly index of August 2013, the value would be C<'2013/08'>.

The value of I<archive_type> and I<archive_source> will be one of C<'day'>, C<'month'> or C<'year'>.

For I<archive_source>, it will be set to the configured source list, i.e. it will be set to C<'day'>
if the source list of the index page is set to I<time/lists/day>, it will be set to C<'month'> for
the source list I<time/lists/month> and C<'year'> for the list I<time/lists/year>, no matter what
the actual type of the index page is.

I<archive_type> on the other hand will always be set to the actual type of the index page, i.e. it
will be set to C<'day'> for daily listings, C<'month'> for monthly listings and C<'year'> for the
yearly index pages.

I<archive_type> and I<archive_source> may contain different values for a single collection depending
on the I<exclude_children> and I<hierarchical> options. See L<Newcomen::Plugin::Blog::Index::Lists>
for details.

=head1 SEE ALSO

L<Newcomen::Plugin::Blog::Index::Lists>, L<Newcomen::Plugin::Blog::Source::Time>,
L<Newcomen::Source>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
