# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::Blog::Index::Main;
our $VERSION = 2014052501;

use namespace::autoclean;
use Data::Rmap ();
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;
use POSIX ();
use Newcomen::Collection;
use Newcomen::Content;
use Newcomen::Util::Hash qw( merge );
use Newcomen::Util::String qw( replace );

extends 'Newcomen::Plugin::Base';
with    'Newcomen::Role::Attribute::Catalog';
with    'Newcomen::Role::Attribute::Config';
with    'Newcomen::Role::Attribute::Crawler';
with    'Newcomen::Role::Attribute::URL';

override '_build_hook_order' => sub {{ 'hook_build_collections' => 400 }};

override '_build_default_config' => sub {{

   'blog'                  => {
      'index'              => {
         'main'            => {
            'ascending'    => undef,
            'per_page'     => undef,
            'page_info'    => {},
            'formatters'   => undef,
            'renderer'     => undef,
            'writer'       => undef,
            'index'        => {
               'page_info' => {},
               'url'       => undef,
            },
            'pages'        => {
               'page_info' => {},
               'url'       => undef,
            }
         },
      },
   }

}};

sub hook_build_collections {

   my $self = shift;

   my $asc  = $self -> _config () -> get (['blog', 'index', 'main',     'ascending'])
           // $self -> _config () -> get (['blog', 'index', 'defaults', 'ascending'])
           // 0;

   my $coll = Newcomen::Collection -> new (
      'creator' => 'Blog::Index::Main', 'id' => 'Blog::Index::Main'
   );

   for my $src ($self -> _crawler () -> sources ()) {
      my $epoch = $src -> get (['time', 'published', 'epoch']);
      if ($src -> creator () eq 'Blog::Crawler' and $epoch and $epoch =~ /^\d+$/a) {
         $coll -> push ($src);
      }
   }

   $coll -> sort (
      sub {
         my $a = $_ [0] -> get (['time', 'published', 'epoch']);
         my $b = $_ [1] -> get (['time', 'published', 'epoch']);
         return $asc ? ($a <=> $b) : ($b <=> $a);
      }
   );

   return ($coll);

}

sub hook_build_pages {

   my $self       = shift;
   my @page_list  = ();
   my $collection = $self -> _catalog () -> collection ('Blog::Index::Main');
   my $per_page   = $self -> _config () -> get (['blog', 'index', 'main',     'per_page'])
                 // $self -> _config () -> get (['blog', 'index', 'defaults', 'per_page'])
                 // 5;

   return () unless $collection;
   confess 'Invalid per_page value' unless $per_page =~ /^\d+$/a;

   my $cur_page = 1;
   my $count    = $collection -> count ();
      $per_page = $count if $per_page == 0;
   my $pages    = ($per_page == 0 ? 1 : POSIX::ceil ($count / $per_page)) || 1;
   my $iterator = $collection -> paginate ($per_page);

   while ((my @sources = $iterator -> ()) or ($cur_page == 1)) {

      my $type = $cur_page == 1 ? 'index' : 'pages';
      my $url  = $self -> _config () -> get (['blog', 'index', 'main', $type, 'url']);

      if ($url) {

         my $meta = $collection -> meta () -> data ();
         my $more = {
            'page'        => $cur_page,
            'pages'       => $pages,
            'posts_total' => $count,
            'posts_from'  => ($cur_page - 1) * $per_page + 1,
            'posts_to'    => $cur_page == $pages ? $count : $cur_page * $per_page,
         };

         my $replacements = merge ($meta, $more);

         $url = replace ($url, $replacements);
         next unless $url;

         my $page = Newcomen::Page -> new (
            'target'     => $url,
            'creator'    => 'Blog::Index::Main',
            'content'    => [ map { Newcomen::Content ->  new ('source' => $_) } @sources ],
            'collection' => $collection,
         );

         my $info     = {};
         my @defaults = (
            ['blog',          'defaults',        'page_info'],
            ['blog', 'index', 'defaults',        'page_info'],
            ['blog', 'index', 'main',            'page_info'],
            ['blog', 'index', 'main',     $type, 'page_info'],
         );
         $info = merge ($info, $self -> _config () -> get ($_) // {}) for (@defaults);
         Data::Rmap::rmap { $_ = replace ($_, $replacements) } $info;

         $page -> set (["info_$_"  ], $info -> {$_}) for keys %$info;
         $page -> set (['page_type'], 'multiple'   );

         $page -> page_meta () -> merge ($more);

         if (scalar @page_list > 0) {
            $page -> set (['prev_page'], $page_list [-1] -> target ());
            $page_list [-1] -> set (['next_page'], $page -> target ());
         }

         $self -> _url () -> set (['Blog::Index::Main', $cur_page], $page -> target ());

         push @page_list, $page;

      }

      ++ $cur_page;

   }

   return @page_list;

}

sub hook_formatters {

   my $self    = shift;
   my $content = shift;
   my $page    = shift;
   my $index   = shift;

   return unless $page -> creator () eq 'Blog::Index::Main';

   $content -> formatters () -> add (
      $self -> _config () -> get (['blog', 'index', 'main', 'formatters']) // []
   );

}

sub hook_renderer {

   my $self = shift;
   my $page = shift;

   $page -> set_renderer ($self -> _config () -> get (['blog', 'index', 'main', 'renderer']));

}

sub hook_writer {

   my $self = shift;
   my $page = shift;

   $page -> set_writer ($self -> _config () -> get (['blog', 'index', 'main', 'writer']));

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::Blog::Index::Main - Creates the blog's main index pages.

=head1 DESCRIPTION

This plugin will create the main index page(s), i.e. the index page(s) containing all blog articles,
usually used as a blog's start page. Posts will be sorted by publication time.

=head2 Collections

One collection will be created with both the collection ID and the creator ID set to
C<'Blog::Index::Main'>. All source items with the creator ID C<'Blog::Crawler'> will be included.
The sources in the collection will be sorted by publication time, in descending order by default
(this can be changed in the configuration). For this to work a source's meta data must contain the
I<time/published/epoch> key (set by the plugin L<Newcomen::Plugin::Blog::Source::Time> by default).
Sources without this key (or an invalid value for the key) will not be included. No meta data will
be set for the collection.

=head2 Content

L<Newcomen::Content> instances will be created for all sources on the final pages. No meta data will
be set for the content items.

=head2 Pages

The source instances in the collection (see L<Collections|/Collections>) will be distributed to the
individual pages, the number of articles per page can be configured by the user (it defaults to
C<5> if not set, see L<OPTIONS|/OPTIONS> below). See L<META DATA|/META DATA> for the page's meta
data set by this plugin. The page target will be set according to the configuration, see below.

An entry will be created in the global URL map (see L<Newcomen::URL>) for every page. The key will
be I<< Blog::Index::Main/<page> >>, with I<< <page> >> being the number of the page, starting with
C<1>.

=head1 OPTIONS

   {
      'blog'                  => {
         'index'              => {
            'main'            => {
               'ascending'    => undef,
               'per_page'     => undef,
               'page_info'    => {},
               'formatters'   => undef,
               'renderer'     => undef,
               'writer'       => undef,
               'index'        => {
                  'page_info' => {},
                  'url'       => undef,
               },
               'pages'        => {
                  'page_info' => {},
                  'url'       => undef,
               }
            },
         },
      },
   }

These are the default options set by this plugin. They may be overridden by user configuration.

I<blog/index/main/ascending> may be set to a true value if the articles should be sorted by
publication time in ascending order (i.e. oldest articles first). If this is C<undef>, the value of
I<blog/index/defaults/ascending> will be used, if this, too, is C<undef>, it defaults to C<0>.

I<blog/index/main/per_page> specifies the maximum number of articles on one index page. It must be
an integer greater or equal to zero. If it is set to zero, all articles will be put on one index
page. If it is C<undef> (the default), the value of I<blog/index/defaults/per_page> will be used. If
this, too, is C<undef>, the value C<5> will be used.

I<blog/index/main/index> and I<blog/index/main/pages> are both hashrefs with the keys I<page_info>
and I<url>. The I<blog/index/main/index> hashref contains the page info and URL settings for the
first index page (page one), the I<blog/index/main/pages> hashref contains the page info and URL
settings for all other pages (page two and all subsequent pages).

The I<page_info> hashrefs may be used to set arbitrary data that will be included in the page's meta
data. The data may contain placeholders (same as for the I<url> option, see below). Everything set
in these hashrefs will be included in the meta data, with the original keys being prefixed with
C<'info_'>. The I<blog/index/main/index/page_info> and I<blog/index/main/pages/page_info> hashrefs
will be merged with the following default options (in that order): I<blog/index/main/page_info>,
I<blog/index/defaults/page_info> and I<blog/defaults/page_info>. More specific data will override
less specific (default) data if necessary. (Also see L<Newcomen::Plugin::Blog::Index::Defaults> and
L<Newcomen::Plugin::Blog::Defaults>.)

The I<page_info> data and the I<url> option may contain placeholders for replacement, as described
in the documentation of L<Newcomen::Plugin::Blog::Single|Newcomen::Plugin::Blog::Single/OPTIONS>'s
options, please see there for details. The meta data available for replacement is the meta data of
the index collection (see L<Collections|/Collections>), which is usually empty. Additionally, the
following data is available:

   {
      'page'        => <current page>,
      'pages'       => <total number of pages>,
      'posts_total' => <total number of posts in the collection>,
      'posts_from'  => <number of first post on the page>,
      'posts_to'    => <number of last post on the page>,
   }

All numbers start at one. If after replacement the URL should be empty, the page will not be
created.

The options I<blog/index/main/formatters>, I<blog/index/main/renderer> and
I<blog/index/main/writer> may be used to specify formatter backends for the index page content, and
renderer/writer backends for index pages. The format is the same as described for the
L<options|Newcomen::Plugin::Blog::Defaults/OPTIONS> in L<Newcomen::Plugin::Blog::Defaults>, please
see there for details. If these are C<undef> (the default), no backends will be set, and another
plugin may set these.

=head1 META DATA

=head2 Pages

   {
      'page'        => <current page>,
      'pages'       => <total number of pages>,
      'posts_total' => <total number of posts in the collection>,
      'posts_from'  => <number of first post on the page>,
      'posts_to'    => <number of last post on the page>,
      'prev_page'   => <URL of previous page>,
      'next_page'   => <URL of next page>,
      'page_type'   => 'multiple',
   }

I<title> will be the title of the page, as configured. I<page_type> will always be set to
C<'multiple'>. I<prev_page> and I<next_page> will be set to the URL of the previous and/or next
page, if there is a previous and/or next page. The other keys are explained above, see
L<OPTIONS|/OPTIONS>.

Additionally, all meta data specified in the I<page_info> hashrefs (see L<OPTIONS|/OPTIONS>) will be
set for the pages, keys will be prefixed by C<'info_'>.

=head1 HOOKS

This plugin implements the following hooks: I<hook_build_collections()> (priority C<400>),
I<hook_build_pages()>, I<hook_formatters()>, I<hook_renderer()> and I<hook_writer()> (default
priority).

=head1 SEE ALSO

L<Newcomen::Collection>, L<Newcomen::Content>, L<Newcomen::Page>,
L<Newcomen::Plugin::Blog::Defaults>, L<Newcomen::Plugin::Blog::Index::Defaults>,
L<Newcomen::Plugin::Blog::Single>, L<Newcomen::Plugin::Blog::Source::Time>, L<Newcomen::URL>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
