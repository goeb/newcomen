# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::Blog::Index::Lists;
our $VERSION = 2014052501;

use namespace::autoclean;
use Data::Rmap ();
use Digest::SHA qw( sha1_hex );
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;
use Newcomen::Collection;
use Newcomen::Util::Hash qw( merge );
use Newcomen::Util::String qw( replace split_ssv );

extends 'Newcomen::Plugin::Base';
with    'Newcomen::Role::Attribute::Catalog';
with    'Newcomen::Role::Attribute::Config';
with    'Newcomen::Role::Attribute::URL';

has '_collections'  => (
   'is'             => 'ro',
   'isa'            => 'ArrayRef[Str]',
   'default'        => sub {[]},
   'init_arg'       => undef,
   'traits'         => ['Array'],
   'handles'        => {
      '_add'        => 'push',
      '_all'        => 'elements',
   },
);

has '_digests'      => (
   'is'             => 'ro',
   'isa'            => 'HashRef[Str]',
   'default'        => sub {{}},
   'init_arg'       => undef,
   'traits'         => ['Hash'],
   'handles'        => {
      '_key_get'    => 'get',
      '_key_set'    => 'set',
      '_key_exists' => 'exists',
   },
);

override '_build_default_config' => sub {{

   'blog'        => {
      'index'    => {
         'lists' => {},
      },
   }

}};

sub hook_process_source {

   my $self = shift;
   my $src  = shift;

   return 1 unless $src -> creator () eq 'Blog::Crawler';

   for my $key (keys %{ $self -> _config -> get (['blog', 'index', 'lists']) // {} }) {

      next if $key eq 'defaults';

      my $hier = $self -> _config -> get (['blog', 'index', 'lists', $key,       'hierarchical'])
              // $self -> _config -> get (['blog', 'index', 'lists', 'defaults', 'hierarchical'])
              // 1;

      next unless $hier;

      my $list  = $self -> _config -> get (['blog', 'index', 'lists', $key, 'source_list']) // $key;
      my $items = $src -> get ($list);

      next unless $items and ref $items eq 'ARRAY' and @$items;

      my @cleaned = ();
      for my $item (@$items) {
         my @parts = split_ssv ($item);
         push @cleaned, join ('/', @parts);
      }
      $src -> set ($list, \@cleaned);

   }

   return 1;

}

sub hook_build_collections {

   my $self  = shift;
   my $def   = 'defaults';
   my $index = $self -> _catalog () -> collection ('Blog::Index::Main');

   return () unless $index;

   for my $src ($index -> sources ()) {
      for my $key (keys %{ $self -> _config -> get (['blog', 'index', 'lists']) // {} }) {

         next if $key eq $def;

         my $list = $self -> _config -> get (['blog', 'index', 'lists', $key, 'source_list'])
                 // $key;

         my $hier = $self -> _config -> get (['blog', 'index', 'lists', $key, 'hierarchical'])
                 // $self -> _config -> get (['blog', 'index', 'lists', $def, 'hierarchical'])
                 // 1;

         my $excl = $self -> _config -> get (['blog', 'index', 'lists', $key, 'exclude_children'])
                 // $self -> _config -> get (['blog', 'index', 'lists', $def, 'exclude_children'])
                 // 0;

         my $items = $src -> get ($list);
         next unless $items and ref $items eq 'ARRAY' and @$items;

         for my $item (@$items) {

            my @all_items = ();

            if (not $hier or $excl) {
               push @all_items, $item;
            }
            else {
               my @parts = split_ssv ($item);
               push @all_items, @all_items ? $all_items [-1] . '/' . $_ : $_ for (@parts);
            }

            for (@all_items) {

               my $dig = $self -> _key_get ($key);
               unless ($dig) {
                  $dig = sha1_hex ($key);
                  $self -> _key_set ($key, $dig);
               }

               my $coll_id = "Blog::Index::Lists/$dig/$_";

               my $collection = $self -> _catalog () -> collection ($coll_id);

               unless ($collection) {

                  $collection = Newcomen::Collection -> new (
                     'creator' => 'Blog::Index::Lists',
                     'id'      => $coll_id,
                  );

                  my $path = $_;
                  $path =~ s!/!-!g unless $hier;

                  $collection -> set (['list_id'          ], $key );
                  $collection -> set (['list_id_sha1'     ], $dig );
                  $collection -> set (['list_source'      ], $list);
                  $collection -> set (['list_item'        ], $_   );
                  $collection -> set (['list_path'        ], $path);
                  $collection -> set (['list_hierarchical'], $hier);
                  $collection -> set (['list_no_children' ], $excl);

                  $self -> _add ($coll_id);
                  $self -> _catalog () -> add ($collection);

               }

               $collection -> push ($src);

            }

         }

      }
   }

   return ();

}

sub hook_post_build_collections {

   my $self = shift;

   for my $coll_id ($self -> _all ()) {

      next unless $self -> _catalog () -> exists ($coll_id);
      my $coll = $self -> _catalog () -> collection ($coll_id);

      my $id = $coll -> get (['list_id']);
      next unless $id;

      my $asc  = $self -> _config () -> get (['blog', 'index', 'lists', $id,        'ascending'])
              // $self -> _config () -> get (['blog', 'index', 'lists', 'defaults', 'ascending'])
              // $self -> _config () -> get (['blog', 'index',          'defaults', 'ascending'])
              // 0;

      $coll -> sort (
         sub {
            my $a = $_ [0] -> get (['time', 'published', 'epoch']) // confess 'No epoch set';
            my $b = $_ [1] -> get (['time', 'published', 'epoch']) // confess 'No epoch set';
            return $asc ? ($a <=> $b) : ($b <=> $a);
         }
      );

   }

}

sub hook_build_pages {

   my $self      = shift;
   my @page_list = ();
   my $def       = 'defaults';

   for my $coll_id ($self -> _all ()) {

      next unless $self -> _catalog () -> exists ($coll_id);

      my $coll = $self -> _catalog () -> collection ($coll_id);
      next unless $coll -> count ();

      my $id = $coll -> get (['list_id']);
      next unless $id;

      my $per_page = $self -> _config () -> get (['blog', 'index', 'lists', $id,  'per_page'])
                  // $self -> _config () -> get (['blog', 'index', 'lists', $def, 'per_page'])
                  // $self -> _config () -> get (['blog', 'index',          $def, 'per_page'])
                  // 5;

      confess 'Invalid per_page value' unless $per_page =~ /^\d+$/a;

      my $item         = $coll -> get (['list_item'        ]);
      my $hierarchical = $coll -> get (['list_hierarchical']);
      my $count        = $coll -> count ();
         $per_page     = $count if $per_page == 0;
      my $cur_page     = 1;
      my $pages        = ($per_page == 0 ? 1 : POSIX::ceil ($count / $per_page)) || 1;
      my $iterator     = $coll -> paginate ($per_page);
      my @coll_pages   = ();

      while (my @sources = $iterator -> ()) {

         my $type = $cur_page == 1 ? 'index' : 'pages';
         my $url  = $self -> _config () -> get (['blog', 'index', 'lists', $id, $type, 'url']);

         if ($url) {

            my $meta = $coll -> meta () -> data ();
            my $more = {
               'page'        => $cur_page,
               'pages'       => $pages,
               'posts_total' => $count,
               'posts_from'  => ($cur_page - 1) * $per_page + 1,
               'posts_to'    => $cur_page == $pages ? $count : $cur_page * $per_page,
            };

            my $replacements = merge ($more, $meta);

            $url = replace ($url, $replacements);
            next unless $url;

            my $page = Newcomen::Page -> new (
               'target'     => $url,
               'creator'    => 'Blog::Index::Lists',
               'content'    => [ map { Newcomen::Content ->  new ('source' => $_) } @sources ],
               'collection' => $coll,
            );

            my $info     = {};
            my @defaults = (
               ['blog',          'defaults',                    'page_info'],
               ['blog', 'index', 'defaults',                    'page_info'],
               ['blog', 'index', 'lists',    'defaults',        'page_info'],
               ['blog', 'index', 'lists',    $id,               'page_info'],
               ['blog', 'index', 'lists',    $id,        $type, 'page_info'],
            );
            $info = merge ($info, $self -> _config () -> get ($_) // {}) for (@defaults);
            Data::Rmap::rmap { $_ = replace ($_, $replacements) } $info;

            $page -> set (["info_$_"  ], $info -> {$_}) for keys %$info;
            $page -> set (['page_type'], 'multiple'   );

            $page -> page_meta () -> merge ($more);

            $self -> _url () -> set (
               ['Blog::Index::Lists', $id, $item, $cur_page], $page -> target ()
            );

            if (scalar @coll_pages > 0) {
               $page -> set (['prev_page'], $coll_pages [-1] -> target ());
               $coll_pages [-1] -> set (['next_page'], $page -> target ());
            }

            push @page_list,  $page;
            push @coll_pages, $page;

         }

         ++ $cur_page;

      }

   }

   return @page_list;

}

sub hook_formatters {

   my $self    = shift;
   my $content = shift;
   my $page    = shift;
   my $index   = shift;

   return unless $page -> creator () eq 'Blog::Index::Lists';

   my $id = $page -> get (['list_id']);

   $content -> formatters () -> add (
      $self -> _config () -> get (['blog', 'index', 'lists', $id, 'formatters'])
   );

}

sub hook_renderer {

   my $self = shift;
   my $page = shift;

   return unless $page -> creator () eq 'Blog::Index::Lists';

   my $id = $page -> get (['list_id']);

   $page -> set_renderer (
      $self -> _config () -> get (['blog', 'index', 'lists', $id, 'renderer'])
   );

}

sub hook_writer {

   my $self = shift;
   my $page = shift;

   return unless $page -> creator () eq 'Blog::Index::Lists';

   my $id = $page -> get (['list_id']);

   $page -> set_writer (
      $self -> _config () -> get (['blog', 'index', 'lists', $id, 'writer'])
   );

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::Blog::Index::List - Creates custom index pages.

=head1 DESCRIPTION

This plugin may be used to create index pages for arbitrary meta data lists of the source items, for
example category index pages or tag index pages. The meta data to create an index page from has to
be an arrayref, for every item in the arrayref one or more index pages will be created. Both
hierarchical (e.g. categories and subcategories) and non-hierarchical (e.g. tags) items are
supported. For hierarchical data there is a configuration option whether sub-items should be
included on the pages or not (e.g. whether to include I<category/subcategory> items on the
I<category> index pages). For hierarchical items the slash (C<'/'>) is used as a separator.

Please see the L<OPTIONS|/OPTIONS> for details on how to specify the data used to build the index
pages.

=head2 Sources

For hierarchical data, the source lists in the L<Newcomen::Source> instances' meta data will be
cleaned up: for every source list element, multiple consecutive slashes will be replaced by one, and
slashes at the beginning and end of an element will be removed. Non-hierarchical data will not be
altered. If the specified source list is not an arrayref, it will be ignored. All source items with
the creator ID C<'Blog::Crawler'> will be processed.

=head2 Collections

For every index page to be created one collection will be created. For non-hierarchical data, this
means that every item in one of the source lists will result in one collection. For hierarchical
data the same applies if the I<exclude_children> option is set. If not, one element in the source
list will create a collection for every part in its hierarchy (e.g. given the element
I<category/subcategory>, one collection will be created for I<category/subcategory> and one for
I<category>). The collection ID will be set to C<< 'Blog::Index::Lists/<ID digest>/<element>' >>,
with C<< <ID digest> >> being the hexadecimal representation of the SHA-1 digest of the index ID
(see L<OPTIONS|/OPTIONS>, usually the index ID is the same as the meta data key of the source list),
and C<< <element> >> the element in the list for which the collection is created. The creator ID
will be C<'Blog::Index::Lists'>.

=head2 Pages

For every collection created (as described above) one or more pages will be created (depending on
the I<per_page> option, see below). Note that this plugin keeps an internal list of all the
collections it created, and only those will be processed. Collections may still be deleted to
prevent pages from being created, but no collection added by another plugin to the catalog will be
used by this plugin. Page targets will be set according to the configuration. The creator ID will be
set to C<'Blog::Index::Lists'>. The page's collection attribute will reference the source
collection.

The page targets will be stored in the global URL map (see L<Newcomen::URL>) with the key
C<< ['Blog::Index::Lists', <ID>, <element>, <page>] >>. C<< <ID> >> is the index ID (please see the
L<OPTIONS|/OPTIONS> for details) and C<< <element> >> the element in the list for which the page was
created. C<< <page> >> specifies the page number (starting at one). It is recommended to always use
the arrayref access for this key, since both C<< <ID> >> and C<< <element> >> may contain slashes.

=head1 OPTIONS

   {
      'blog'        => {
         'index'    => {
            'lists' => {},
         },
      },
   }

This data structure is defined by this plugin by default. It doesn't really contain any options. The
user configuration may add options as required. If so, the values in the I<blog/index/lists> hashref
must themselves be hashrefs.

The following example will add index pages for categories and tags:

   {
      'blog'                         => {
         'index'                     => {
            'lists'                  => {
               'cats'                => {
                  'ascending'        => 0,
                  'exclude_children' => 0,
                  'hierarchical'     => 1,
                  'per_page'         => 5,
                  'source_list'      => 'categories'
                  'page_info'        => {
                     'title'         => 'A Category Listing',
                  },
                  'index'            => {
                     'url'           => '<list_path>/index.html',
                  },
                  'pages'            => {
                     'url'           => '<list_path>/page-<page>/index.html',
                  },
               },
               'tags'                => {
                  'hierarchical'     => 0,
                  'index'            => {
                     'page_info'     => {
                        'title'      => 'Tag: <list_name>',
                     },
                     'url'           => 'tag/<list_path>/index.html',
                  },
                  'pages'            => {
                     'page_info'     => {
                        'title'      => 'Tag: <list_name>, Page <page> of <pages>',
                     },
                     'url'           => 'tag/<list_path>/page-<page>/index.html',
                  },
               },
            },
         },
      },
   }

The keys in the I<blog/index/lists> hashref are the index ID (C<'cats'> and C<'tags'> in the example
above). These index IDs will be used to look up the source lists in the L<Newcomen::Source>
instances' meta data, i.e. the lists key has to match the index ID. Nested meta data is supported
using slashes as separators. To override the meta data key, the I<source_list> option in an item's
hashref may be used (in the example: I<blog/index/lists/cats/source_list> is set, so the list in the
source meta data is called C<'categories'>, instead of C<'cats'>, which would be used by default).

In the source items meta data the specified list has to be an arrayref, if not, or if it doesn't
exist, the source item will not be included on the index page.

B<Note:> The index ID I<defaults> is reserved and must not be used for user configuration!

The I<< blog/index/lists/<index ID> >> hashrefs may contain the following options (the following
names are all relative to these hashrefs, unless they start with I<blog/>):

=over

=item *

I<source_list>, see above for a description.

=item *

I<ascending> specifies whether to sort the articles in ascending order (oldest first) or not. If
this is not set, the value of I<blog/lists/defaults/ascending> will be used. If this, too, is not
set, the value of I<blog/index/defaults/ascending> will be used. If this is not set, it defaults to
false.

=item *

I<exclude_children> specifies whether to not include the children (e.g. subcategories) on the index
page of a parent. This option will be ignored for non-hierarchical data. If this is not set, the
value of I<blog/index/lists/defaults/exclude_children> will be used, this is defined in
L<Newcomen::Plugin::Blog::Index::Lists::Defaults>. If this, too, is C<undef>, it defaults to C<0>
(i.e. children will be included).

=item *

I<hierarchical> specifies if the data is hierarchical or not. If enabled, the slash (C<'/'>) may be
used in the meta data as separator to specify the hierarchy, e.g. C<'category/subcategory'>. If this
is not set, the value of I<blog/index/lists/defaults/hierarchical> will be used. If this is not set,
too, it defaults to C<1>.

=item *

I<per_page> specifies the number of articles per page. If this is not set, the value of the option
I<blog/index/lists/defaults/per_page> will be used. If this, too, is not set, the value of
I<blog/index/defaults/per_page> will be used. If this, too, is not set, it defaults to C<5>. A value
of C<0> means to put all posts on one page.

=item *

I<index> and I<pages> are basically the same as the I<blog/index/main/index> and
I<blog/index/main/pages> hashrefs, specifying the URLs and additional meta data for the different
pages, please see L<Newcomen::Plugin::Blog::Index::Main> for details. The values may contain the
same placeholders available for I<blog/index/main/index> and I<blog/index/main/pages>, and
additionally the following: I<< <list_id> >>, I<< <list_id_sha1> >>, I<< <list_source> >>,
I<< <list_item> >>, I<< <list_path> >>, I<< <list_hierarchical> >> and I<< <list_no_children> >>.
See the collection meta data below for a description.

=item *

The hashrefs I<page_info>, I<index/page_info> and I<pages/page_info> may be used to set arbitrary
data that will be included in the page's meta data. The data may contain placeholders (same as for
the I<url> option). Everything set in the hashref(s) will be included in the meta data, with the
original keys being prefixed with C<'info_'> (so for the example above the pages will contain the
I<info_title> key). Before being copied to the page's meta data, hashrefs will be merged in the
following order (with data from hashrefs mentioned later overriding the data from earlier ones):
I<blog/defaults/page_info>, I<blog/index/defaults/page_info>,
I<blog/index/lists/defaults/page_info>, I<< blog/index/lists/<list ID>/page_info >>,
I<< blog/index/lists/<list ID>/index/page_info >> or
I<< blog/index/lists/<list ID>/pages/page_info >> (as appropriate).


=item *

I<formatters>, I<renderer> and I<writer> may be used to specify formatter backends for the index
page content, and renderer/writer backends for index pages. The format is the same as described for
the options in L<Newcomen::Plugin::Blog::Defaults>, please see there for details. If these are
C<undef> (the default), no backends will be set, and another plugin may set these.

=back

=head1 META DATA

=head2 Collections

   {
      'list_id'           => $list_id,
      'list_id_sha1'      => $list_id_sha1,
      'list_source'       => $list_source,
      'list_item'         => $list_item,
      'list_path'         => $list_path,
      'list_hierarchical' => $list_hierarchical,
      'list_no_children'  => $list_no_children,
   }

=over

=item *

I<list_id> is the list ID as described above, see L<OPTIONS|/OPTIONS>.

=item *

I<list_id_sha1> is the SHA-1 digest of the I<list_id> value in its hexadecimal representation.

=item *

I<list_source> is the name of the list in the source item's meta data.

=item *

I<list_item> is the name of the item in the list for which the collection is created.

=item *

I<list_path> is same as list I<list_item>, but for non-hierarchical data all slashes are replaced by
dashes.

=item *

I<list_hierarchical> and I<list_no_children> are set to the values of the respective configuration
options.

=back

=head2 Pages

   {
      'page'        => $cur_page,
      'pages'       => $pages,
      'posts_total' => $count,
      'posts_from'  => $posts_from,
      'posts_to'    => $posts_to,
      'prev_page'   => $prev_page,
      'next_page'   => $next_page,
      'type'        => 'multiple'
   }

The meta data of the collection the page is created from may be accessed through the page's meta
data, see L<Newcomen::Page> for details.

I<type> will always be set to C<'multiple'>. The other keys are the same as described in
L<Newcomen::Plugin::Blog::Index::Main>, please see there for details.

Additionally, all meta data specified in the I<page_info> hashref(s) (see L<OPTIONS|/OPTIONS>) will
be set for the pages, keys will be prefixed by C<'info_'>.

=head1 HOOKS

This plugin implements the following hooks: I<hook_process_source()>, I<hook_build_collections()>,
I<hook_post_build_collections()>, I<hook_build_pages()>, I<hook_formatters()>, I<hook_renderer()>
and I<hook_writer()> (all with default priority).

=head1 SEE ALSO

L<Newcomen::Collection>, L<Newcomen::Page>, L<Newcomen::Plugin::Blog::Defaults>,
L<Newcomen::Plugin::Blog::Index::Lists::Defaults>, L<Newcomen::Plugin::Blog::Index::Main>,
L<Newcomen::Source>, L<Newcomen::URL>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
