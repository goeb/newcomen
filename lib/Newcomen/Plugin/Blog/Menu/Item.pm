# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::Blog::Menu::Item;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::Aliases;
use MooseX::StrictConstructor;
use Scalar::Util qw( refaddr );
use Newcomen::Util::Traits;

{

   my $original = Newcomen::Util::Traits::std_hash_handles ();
   my $methods  = {};
   while (my ($k, $v) = each %$original) {
      $methods -> {"_$k"} = $v;
   }

   has '_children'  => (
      'is'          => 'ro',
      'isa'         => 'HashRef[Newcomen::Plugin::Blog::Menu::Item]',
      'init_arg'    => undef,
      'default'     => sub {{}},
      'traits'      => ['Hash'],
      'handles'     => $methods,
   );

   alias 'child'    => '_get';
   alias 'ids'      => '_keys';
   alias 'children' => '_values';
   alias 'count'    => '_total';

}

{

   my $original = Newcomen::Util::Traits::std_hash_handles ();
   my $methods  = {};
   while (my ($k, $v) = each %$original) {
      $methods -> {"_urls_$k"} = $v;
   }

   has '_urls'   => (
      'is'       => 'ro',
      'isa'      => 'HashRef[ArrayRef[Newcomen::Plugin::Blog::Menu::Item]]',
      'default'  => sub {{}},
      'init_arg' => undef,
      'traits'   => ['Hash'],
      'handles'  => $methods,
   );

}

has 'id'       => (
   'is'        => 'ro',
   'isa'       => 'Str',
   'required'  => 1,
);

has 'parent'   => (
   'is'        => 'ro',
   'isa'       => 'Maybe[Newcomen::Plugin::Blog::Menu::Item]',
   'default'   => undef,
);

has 'active'   => (
   'is'        => 'rw',
   'isa'       => 'Int',
   'init_arg'  => undef,
   'default'   => 0,
);

has 'url'      => (
   'is'        => 'rw',
   'isa'       => 'Maybe[Str]',
   'default'   => undef,
   'trigger'   => \&_trigger_url,
);

has 'order'    => (
   'is'        => 'rw',
   'isa'       => 'Maybe[Str]',
   'default'   => undef,
);

has 'meta'     => (
   'is'        => 'ro',
   'isa'       => 'Newcomen::Data',
   'init_arg'  => undef,
   'default'   => sub { Newcomen::Data -> new () },
   'handles'   => [qw( delete exists get set )],
);

has '_current' => (
   'is'        => 'rw',
   'isa'       => 'Maybe[Str]',
   'default'   => undef,
   'init_arg'  => undef,
);

sub BUILD {

   my $self = shift;

   $self -> parent () -> add_child ($self) if $self -> parent ();

}

sub add_child {

   my $self  = shift;
   my $child = shift;

   confess 'Parent/child mismatch' unless refaddr ($self) == refaddr ($child -> parent ());
   confess 'Menu ID not unique'    if     $self -> child ($child -> id ());

   $self -> _set ($child -> id (), $child);

}

sub sorted {

   my $self = shift;

   my @ids = sort {
      ($self -> child ($a) -> order () // $self -> child ($a) -> id ())
      cmp
      ($self -> child ($b) -> order () // $self -> child ($b) -> id ())
   } $self -> ids ();

   return map { $self -> child ($_) } @ids;

}

sub _trigger_url {

   my $self = shift;
   my $new  = shift;
   my $old  = shift;

   if (defined $old and defined $new) {
      return if $old eq $new;
   }
   else {
      return unless defined $old or defined $new;
   }

   $self -> parent () -> update_url ($self, $old, $new) if $self -> parent ();

}

sub update_url {

   my $self = shift;
   my $item = shift;
   my $old  = shift;
   my $new  = shift;

   if ($old) {
      my @list  = @{ $self -> _urls_get ($old) };
      my $index = 0;
      for (@list) {
         last if refaddr ($_) == refaddr ($item);
         ++ $index;
      }
      splice @list, $index, 1;
      $self -> _urls_set ($old, \@list);
   }

   if ($new) {
      my @list = @{ $self -> _urls_get ($new) // [] };
      push @list, $item;
      $self -> _urls_set ($new, \@list);
   }

   $self -> parent () -> update_url ($item, $old, $new) if $self -> parent ();

}

sub activate {

   my $self = shift;
   my $url  = shift;

   if (my $current = $self -> _current ()) {
      return if $url and $url eq $current;
      for my $root (@{ $self -> _urls_get ($current) // [] }) {
         my $menu = $root;
         while ($menu) {
            $menu -> active (0);
            $menu = $menu -> parent ();
         }
      }
   }

   if ($url) {
      for my $root (@{ $self -> _urls_get ($url) // [] }) {
         my $menu = $root;
         $menu -> active (2);
         while ($menu = $menu -> parent ()) {
            $menu -> active (1);
         }
      }
   }

   $self -> _current ($url);

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::Blog::Menu::Item - An item in a menu.

=head1 SYNOPSIS

   use Newcomen::Plugin::Blog::Menu::Item;

   # Create a top-level item:
   my $root = Newcomen::Plugin::Blog::Menu::Item -> new (
      'id' => 'root'
   );

   # Create a child item:
   my $child = Newcomen::Plugin::Blog::Menu::Item -> new (
      'id'     => 'child',
      'parent' => $root,
   );

   # Set the URL of the child item:
   $child -> url ('some URL');

   # Set arbitrary meta data:
   $child -> set (['name'], 'some name');

   # Set the 'active' flags for an URL:
   $root -> activate ('some  URL');

   # Get an ordered list of the root's children:
   my @children = $root -> sorted ();

=head1 DESCRIPTION

I<Newcomen::Plugin::Blog::Menu::Item> represents an item in a menu.

An item itself may have an arbitrary number of child menu items. Each menu item is identified by an
ID, and all direct children of a menu item must have unique IDs. Other properties of a menu item are
its parent (another I<Newcomen::Plugin::Blog::Menu::Item> instance, or C<undef> if it is a top-level
item), an URL (basically an arbitrary string, or C<undef>), an order value used for sorting, an
active indicator (see below), and arbitrary meta data.

=head1 CLASS METHODS

=head2 new

   my $menu = Newcomen::Plugin::Blog::Menu::Item -> new (
      'id'     => $id,
      'parent' => $parent,
      'url'    => $url,
      'order'  => $order,
   );

Constructor.

The I<id> parameter is required. It must be unique among all the IDs of a parent's direct child menu
items. If it is not unique, the constructor will I<die()>.

If a menu item should be added as a sub-menu to a parent, this parent has to be specified by the
I<parent> parameter. It must be another I<Newcomen::Plugin::Blog::Menu::Item> instance (it may be
set to C<undef> for top-level menu items). The parent can only be set during construction, and it
can not be changed or deleted later on. This also means that there is currently no way to remove a
child menu item from its parent!

The parameters I<url> and I<order> may be used to set initial values for the I<url> and I<order>
properties. These may be changed later on, see below.

=head1 INSTANCE METHODS

=head2 Properties

=head3 id

   my $id = $menu -> id ();

Returns the ID of the menu item. The ID can only be set during construction, see L<new()|/new>.

=head3 order

   # Get the order value:
   my $order = $menu -> order ();

   # Set the value:
   $menu -> order ($order);

Getter/setter for the order value. This value is used when requesting a sorted list of child menu
items using the L<sorted()|/sorted> method. Standard string comparison is used to determine the
menu item order, based on the I<order> attribute. If the I<order> attribute is not set, the menu
item's ID will be used instead.

=head3 parent

   my $parent = $menu -> parent ();

Returns the parent of the menu item. This is another I<Newcomen::Plugin::Blog::Menu::Item> instance,
or C<undef> if the menu item has no parent. The parent can only be set during construction, see
see L<new()|/new>.

=head3 url

   # Get the current URL:
   my $url = $menu -> url ();

   # Set a new URL for the item:
   $menu -> url ($url);

Getter/setter for the URL attribute of a menu item. The URL may be set to any arbitrary string, or
to C<undef> to clear it.

=head3 activate

   $menu -> activate ($url);

The I<activate()> method expects one parameter, which must be a string specifying an URL. The URL
will be looked up in the menu structure (to an arbitrary depth), and if the URL attribute of one or
more menu items in the structure is set to the specified value, all intermediate menu items (from
the menu's root item to the item with the specified URL) will be marked active. The I<active>
attribute of intermediate items will be set to C<1>, the I<active> attribute of the actual menu item
with the specified URL will be set to C<2>. If the specified URL is set for more than one item in
the menu structure, multiple paths in the hierarchy may be marked active. The order in which paths
are marked active is not defined, so if multiple items on one path refer to the specified URL, then
items referring to the URL that are also intermediate items may be set either to C<2> or C<1>, which
one it is is not defined. The user should try to avoid this situation. All items not in any path as
described above will be marked as inactive by setting the I<active> attribute to C<0>.

Note that if the parameter is omitted or set to C<undef> all I<active> flags will be set to C<0>.

=head3 active

   my $active = $menu -> active ();

Get the current setting of the I<active> flag. The flag is usually set using the I<activate()>
method, see above. However, if required, the I<active()> method may also be used as a setter. In
that case the I<active> attribute can be set to any integer value.

=head2 Children

Any menu item may have an arbitrary number of (direct) child items. The children are identified by
their ID, which must be unique for all direct child items (menu items in one menu structure may have
the same ID as long as they do not have the same parent).

=head3 child

   my $child = $menu -> child ($id);

Returns the child item (another I<Newcomen::Plugin::Blog::Menu::Item> instance) by the specified ID,
or C<undef> if no item with the specified ID exists.

=head3 ids

   my @ids = $menu -> ids ();

Returns the IDs of all direct children of the menu item. The list may be empty if there are no
children.

=head3 children

   my @children = $menu -> children ();

Returns all direct children of the menu item (I<Newcomen::Plugin::Blog::Menu::Item> instances). The
list may be empty if there are no children. The order of the items returned is not defined.

=head3 count

   my $count = $menu -> count ();

Returns the number of direct children of the menu item.

=head3 sorted

   my @children = $menu -> sorted ();

Like L<children()|/children>, but the list returned is sorted. Standard string comparison is used to
determine the menu item order, based on the I<order> attribute. If this is not set, the menu item's
ID will be used instead.

=head2 Meta Data

Meta data is stored in an L<Newcomen::Data> instance.

=head3 delete, exists, get, set

The methods I<delete()>, I<exists()>, I<get()> and I<set()> are mapped to the L<Newcomen::Data>
methods of the same name, see there for details.

=head3 meta

   my $meta = $menu -> meta ();

Returns the L<Newcomen::Data> instance storing the menu item's meta data.

=head2 Miscellaneous

=head3 add_child, update_url

These methods are called automatically if required and should not be called directly!

=head1 SEE ALSO

L<Newcomen::Data>, L<Newcomen::Plugin::Blog::Menu>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
