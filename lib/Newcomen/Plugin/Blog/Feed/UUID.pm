# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::Blog::Feed::UUID;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;
use Newcomen::Util::UUID;

extends 'Newcomen::Plugin::Base';
with    'Newcomen::Role::Attribute::Config';
with    'Newcomen::Role::Attribute::Site';

sub hook_post_build_pages {

   my $self = shift;
   my $ns   = $self -> _config () -> get (['blog', 'feed', 'defaults', 'uuid_ns']);

   for my $page ($self -> _site () -> pages ()) {

      next unless $page -> creator () =~ /^Blog::Feed::/;

      for my $cnt ($page -> contents ()) {

         next if $cnt -> exists (['atom_uuid']);
         confess 'UUID namespace required' unless $ns;

         my $file = $cnt -> source () -> get (['path', 'source', 'root']);
         confess 'File name required' unless $file;

         $cnt -> source () -> set (['atom_uuid'], Newcomen::Util::UUID::uuid_v5 ($ns, $file));

      }

   }

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::Blog::Feed::UUID - Sets UUIDs for feed entries.

=head1 DESCRIPTION

This plugin will set UUIDs for all sources on feed pages.

All sources on pages with a creator ID starting with C<'Blog::Feed::'> will be processed. Sources
not included on any such page will not be processed. The meta data set is described below.

=head1 META DATA

=head2 Sources

   {
      'atom_uuid' => $uuid,
   }

This is the meta data set for the source items. It will not be overridden if it already exists. If
it does not exist, the configuration option I<blog/feed/defaults/uuid_ns> has to be set to a valid
UUID. This will be used as UUID namespace to generate per-source UUIDs. A source's UUID will be
derived from its path (the value of the meta data I<path/source/root>, as set by the plugin
L<Newcomen::Plugin::Blog::Crawler>). If this value does not exist, this plugin will I<die()>.

=head1 HOOKS

This plugin implements the I<hook_post_build_pages()> hook (default priority).

=head1 SEE ALSO

L<Newcomen::Page>, L<Newcomen::Plugin::Blog::Crawler>, L<Newcomen::Plugin::Blog::Feed::Defaults>,
L<Newcomen::Source>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
