# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::Blog::Feed::Defaults;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;

extends 'Newcomen::Plugin::Base';
with    'Newcomen::Role::Attribute::Config';

override '_build_hook_order' => sub {{

   'hook_formatters' => 300,
   'hook_renderer'   => 700,
   'hook_writer'     => 700,

}};

override '_build_default_config' => sub {{

   'blog'                => {
      'feed'             => {
         'defaults'      => {
            'ascending'  => undef,
            'entries'    => undef,
            'uuid_ns'    => undef,
            'feed_info'  => {},
            'formatters' => undef,
            'renderer'   => undef,
            'writer'     => undef,
         },
      },
   }

}};

sub hook_formatters {

   my $self    = shift;
   my $content = shift;
   my $page    = shift;
   my $index   = shift;

   return unless $page -> creator () =~ /^Blog::Feed::/;

   $content -> formatters () -> add (
      $self -> _config () -> get (['blog', 'feed', 'defaults', 'formatters']) // []
   );

}

sub hook_renderer {

   my $self = shift;
   my $page = shift;

   $page -> set_renderer (
      $self -> _config () -> get (['blog', 'feed', 'defaults', 'renderer']),
      qr/^Blog::Feed::/
   );

}

sub hook_writer {

   my $self = shift;
   my $page = shift;

   $page -> set_writer (
      $self -> _config () -> get (['blog', 'feed', 'defaults', 'writer']),
      qr/^Blog::Feed::/
   );

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::Blog::Feed::Defaults - Sets some defaults for atom feeds.

=head1 DESCRIPTION

This plugin will add default formatters for all content on feed pages, and set renderer and writer
for these feeds (unless a renderer/writer is already set).

The default formatter(s) will always be added to all L<Newcomen::Content> instances on a page with a
creator ID starting with C<'Blog::Feed::'>, early during the I<hook_formatters()> hook (see
L<HOOKS|/HOOKS>). However, formatters will not be added twice, if a formatter of the same name
already exists in the content's formatter list, it will not be added again.

The default renderer and writer will be set for all L<Newcomen::Page> instances with a creator ID
starting with C<'Blog::Feed::'>, unless a renderer or writer is already set for the page. This will
be done late during the I<hook_renderer()>/I<hook_writer()> hook, respectively. See L<HOOKS|/HOOKS>.

=head1 OPTIONS

   {
      'blog'                => {
         'feed'             => {
            'defaults'      => {
               'ascending'  => undef,
               'entries'    => undef,
               'uuid_ns'    => undef,
               'feed_info'  => {},
               'formatters' => undef,
               'renderer'   => undef,
               'writer'     => undef,
            },
         },
      },
   }

These are the default options set by this plugin. They may be overridden by user configuration.

The options I<blog/feed/defaults/formatters>, I<blog/feed/defaults/renderer> and
I<blog/feed/defaults/writer> work like described in L<Newcomen::Plugin::Blog::Defaults>, please see
there for details.

I<blog/feed/defaults/ascending> specifies whether to sort the articles in ascending order, i.e.
oldest articles first.

I<blog/feed/defaults/entries> is a default setting for the number of articles per list feed.

I<blog/feed/defaults/feed_info> may contain defaults for additional information to be stored in the
pages' meta data. This has to be a hashref (may be empty).

It is up to the individual feed plugins to respect these last three options. See the individual
plugins for details (e.g. L<Newcomen::Plugin::Blog::Feed::Main>).

The I<uuid_ns> options must be set unless a UUID is specified explicitly for every feed in the
configuration! It must be a valid UUID string, and it will be used as the UUID namespace from which
the individual feed UUIDs will be derived. Please see the individual feed plugins for details.

=head1 HOOKS

This plugin implements the following hooks: I<hook_formatters()> (priority C<300>),
I<hook_renderer()> (priority C<700>) and I<hook_writer()> (priority C<700>).

=head1 SEE ALSO

L<Newcomen::Content>, L<Newcomen::Page>, L<Newcomen::Plugin::Blog::Defaults>,
L<Newcomen::Plugin::Blog::Feed::Main>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
