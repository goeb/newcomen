# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::Blog::Formatters;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;

extends 'Newcomen::Plugin::Base';
with    'Newcomen::Role::Attribute::Config';

override '_build_default_config' => sub {{

   'blog'          => {
      'formatters' => {},
   }

}};

override '_build_hook_order' => sub {{ 'hook_formatters' => 200 }};

sub hook_formatters {

   my $self  = shift;
   my $cnt   = shift;
   my $page  = shift;
   my $index = shift;

   return unless $page -> creator () =~ /^Blog::/;

   my $file = $cnt -> get (['path', 'source', 'root']);
   confess "No file name found for content" unless $file;

   for my $re (sort keys %{ $self -> _config () -> get (['blog', 'formatters']) // {} }) {
      if ($file =~ /$re/) {
         $cnt -> formatters () -> add ($self -> _config () -> get (['blog', 'formatters', $re]));
      }
   }

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::Blog::Formatters - Sets formatters for blog entries based on file names.

=head1 DESCRIPTION

This plugin will add one or more formatters to an L<Newcomen::Content> instance, based on the file
name of the source item. All content instances belonging to a page with a creator ID starting with
C<'Blog::'> will be checked, if the source's file name relative to the root directory matches one of
the regular expressions set in the configuration the associated formatters will be added. (If one of
the formatters is already set for the content item, it will not be added again.)

The relative file name of the source has to be set for the meta data key I<path/source/root>, as is
done by the blog's default crawler. If this key is not present (or set to a false value) for source
and/or content items, this plugin will I<die()>.

The formatters will be added early during the I<hook_formatters()> hook, but after the default
formatters have been added (see L<Newcomen::Plugin::Blog::Defaults>).

=head1 OPTIONS

   {
      'blog'          => {
         'formatters' => {},
      },
   }

These are the default options set by this plugin. They may be overridden by user configuration.

In the following description the term specification refers to either a simple string, in which case
it must be the basename of a formatter backend to be used, i.e. the backend's module name without
the C<'Newcomen::Formatter::'> prefix. In this case no additional backend options will be used. A
specification may also be a hashref, in which case it must contain the key I<name>, to specify the
backend's basename (as described before), and it may contain another key I<options>, which must be
set to a hashref containing the backend options to be used (the I<options> hashref may be empty).
See the individual backends for details on their options. Other keys in the specification hashref
will be ignored. Backend names are case sensitive.

I<blog/formatters> must be a hashref (may be empty, the default). The keys of this hashref must be
strings, they will be used as the regular expressions against which the file names will be checked.
The values must be either single formatter specifications, or an arrayref with an arbitrary number
of formatter specifications. These are the formatters to be added to matching content items.

For example, to use the L<Newcomen::Formatter::MultiMarkdown> formatter backend for all
L<Newcomen::Content> instances derived from a file with a file name ending in C<'.mmd'>, the
following configuration may be used:

   {
      'blog'          => {
         'formatters' => {
            '\.mmd$'  => 'MultiMarkdown',
         },
      },
   }

=head1 HOOKS

This plugin implements the I<hook_formatters()> hook (priority C<200>).

=head1 SEE ALSO

L<Newcomen::Content>, L<Newcomen::Formatter::MultiMarkdown>, L<Newcomen::Page>,
L<Newcomen::Plugin::Blog::Defaults>, L<Newcomen::Source>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
