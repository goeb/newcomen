# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::Blog::Source;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;
use Newcomen::Util::File;

extends 'Newcomen::Plugin::Base';
with    'Newcomen::Role::Attribute::Config';

override '_build_default_config' => sub {{

   'blog'                => {
      'source'           => {
         'filename_meta' => undef,
      },
   }

}};

sub hook_prepare_source {

   my $self = shift;
   my $src  = shift;

   return 1 unless $src -> creator () eq 'Blog::Crawler';

   my $id   = $src -> id  ();
   my $path = $src -> get (['path', 'source', 'absolute']);

   confess "Source file not found (or invalid): $id" unless $path and -f -T $path;

   $src -> set (['stat'], Newcomen::Util::File::stat_hash ($path));

   my $meta_re  = $self -> _config () -> get (['blog', 'source', 'filename_meta']);
   my $path_rel = $src  ->               get (['path', 'source', 'root'         ]);

   if ($meta_re and $path_rel) {
      $src -> meta () -> merge (\%+) if $path_rel =~ /$meta_re/;
   }

   my ($file_meta, $content) = Newcomen::Util::File::parse_file ($path);

   $src -> content ($content);
   $src -> meta () -> merge ($file_meta);

   return 1;

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::Blog::Source - Sets some initial data for blog sources.

=head1 DESCRIPTION

This plugin will set the content and some meta data for all L<Newcomen::Source> instances with the
crawler ID C<'Blog::Crawler'>.

Some of the meta data set will be extracted from both the file content and the file name, with meta
data from the content overriding the data from the file name if required. L<Newcomen::Util::File>'s
L<parse_file()|Newcomen::Util::File/parse_file> function is used to parse the files, see there for
details on the file format. See the L<OPTIONS|/OPTIONS> below for details on the file name parsing.

Additionally to these user defined information, the file's I<stat()> information will be included in
the source item's meta data. See the L<Sources|/Sources> section below for details.

B<Note:> This plugin expects two keys to be present in the meta data, both are set by the blog's
default crawler (L<Newcomen::Plugin::Blog::Crawler>). The keys are I<path/source/absolute> and
I<path/source/root>. These keys should not be modified or removed by other plugins, if any of these
doesn't exist or is invalid for some reason, this plugin will I<die()> or not work as expected.


=head1 OPTIONS

   {
      'blog'                => {
         'source'           => {
            'filename_meta' => undef,
         },
      },
   }

These are the default options set by this plugin. They may be overridden by user configuration.

I<blog/source/filename_meta> must be set to a string which is used in a regular expression to be
matched against the file name of the source file relative to the project's root directory. This
regular expression may contain named capture groups (see L<perlre>), and if one of these groups
matches anything, it will be added to the L<Newcomen::Source> instance's meta data (the capture
group's name will be the key, the matching part of the file name will be the value). If this is not
set (the default), or the regular expression doesn't match, no data will be added.

=head1 META DATA

=head2 Sources

   {
      'stat'       => {
         'dev'     => $dev,
         'ino'     => $ino,
         'mode'    => $mode,
         'nlink'   => $nlink,
         'uid'     => $uid,
         'gid'     => $gid,
         'rdev'    => $rdev,
         'size'    => $size,
         'atime'   => $atime,
         'mtime'   => $mtime,
         'ctime'   => $ctime,
         'blksize' => $blksize,
         'blocks'  => $blocks,
      },
   }

I<stat> contains the I<stat()> information of the source file, see L<Newcomen::Util::File>'s
L<stat_hash()|Newcomen::Util::File/stat_hash> function for details.

Additionally, all meta data extracted from the file name (as described above) and all meta data from
the file content will be set by this plugin.

=head1 HOOKS

This plugin implements the I<hook_prepare_source()> hook (default priority).

=head1 SEE ALSO

L<Newcomen::Plugin::Blog::Crawler>, L<Newcomen::Source>, L<Newcomen::Util::File>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
