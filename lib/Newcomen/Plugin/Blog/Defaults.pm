# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::Blog::Defaults;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;

extends 'Newcomen::Plugin::Base';
with    'Newcomen::Role::Attribute::Config';

override '_build_hook_order' => sub {{

   'hook_formatters' => 100,
   'hook_renderer'   => 900,
   'hook_writer'     => 900,

}};

override '_build_default_config' => sub {{

   'blog'             => {
      'defaults'      => {
         'page_info'  => {},
         'formatters' => undef,
         'renderer'   => undef,
         'writer'     => undef,
      },
   }

}};

sub hook_formatters {

   my $self    = shift;
   my $content = shift;
   my $page    = shift;
   my $index   = shift;

   return unless $page -> creator () =~ /^Blog::/;

   $content -> formatters () -> add (
      $self -> _config () -> get (['blog', 'defaults', 'formatters']) // []
   );

}

sub hook_renderer {

   my $self = shift;
   my $page = shift;

   $page -> set_renderer (
      $self -> _config () -> get (['blog', 'defaults', 'renderer']),
      qr/^Blog::/
   );

}

sub hook_writer {

   my $self = shift;
   my $page = shift;

   $page -> set_writer (
      $self -> _config () -> get (['blog', 'defaults', 'writer']),
      qr/^Blog::/
   );

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::Blog::Defaults - Sets some defaults for blog pages.

=head1 DESCRIPTION

This plugin will add default formatters for all blog contents, and set renderer and writer for all
blog pages (unless a renderer/writer is already set).

The default formatter(s) will always be added to all L<Newcomen::Content> instances on a page with a
creator ID starting with C<'Blog::'>, early during the I<hook_formatters()> hook (see
L<HOOKS|/HOOKS>). However, formatters will not be added twice, if a formatter of the same name
already exists in the content's formatter list, it will not be added again.

The default renderer and writer will be set for all L<Newcomen::Page> instances with a creator ID
starting with C<'Blog::'>, unless a renderer or writer is already set for the page. This will be
done late during the I<hook_renderer()>/I<hook_writer()> hook, respectively. See L<HOOKS|/HOOKS>.

=head1 OPTIONS

   {
      'blog'             => {
         'defaults'      => {
            'page_info'  => {},
            'formatters' => undef,
            'renderer'   => undef,
            'writer'     => undef,
         },
      },
   }

These are the default options set by this plugin. They may be overridden by user configuration.

In the following description the term specification refers to either a simple string, in which case
it must be the basename of a formatter/renderer/writer backend to be used, i.e. the backend's module
name without the C<'Newcomen::*::'> prefix. In this case no additional backend options will be used.
A specification may also be a hashref, in which case it must contain the key I<name>, to specify the
backend's basename (as described before), and it may contain another key I<options>, which must be
set to a hashref containing the backend options to be used (the I<options> hashref may be empty).
See the individual backends for details on their options. Other keys in the specification hashref
will be ignored. Backend names are case sensitive.

I<blog/defaults/formatters>, if set, must either be a single formatter specification or an arrayref
containing an arbitrary number of formatter specifications, as described above.

I<blog/defaults/renderer> and I<blog/defaults/writer> must be set to a renderer/writer specification
(as described above) that should be used for blog pages.

Any of these three options may also be set to C<undef> (the default), in which case no defaults will
be applied to any content item's and/or pages. Setting I<blog/defaults/formatters> to an empty
arrayref has the same effect as setting it to C<undef>.

The I<blog/defaults/page_info> option must be set to a hashref. It may be empty (the default).
Several other plugins use this hashref to set additional meta data for the pages. The documentation
of these plugins will contain a reference to this option if it is used, as well as a description of
the processing of its contents.

=head1 HOOKS

This plugin implements the following hooks: I<hook_formatters()> (priority C<100>),
I<hook_renderer()> (priority C<900>) and I<hook_writer()> (priority C<900>).

=head1 SEE ALSO

L<Newcomen::Content>, L<Newcomen::Formatter>, L<Newcomen::Page>, L<Newcomen::Renderer>,
L<Newcomen::Writer>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
