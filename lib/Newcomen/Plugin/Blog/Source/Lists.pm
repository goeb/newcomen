# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::Blog::Source::Lists;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;
use Newcomen::Util::String;

extends 'Newcomen::Plugin::Base';
with    'Newcomen::Role::Attribute::Config';

override '_build_hook_order' => sub {{ 'hook_process_source' => 200 }};

override '_build_default_config' => sub {{

   'blog'        => {
      'source'   => {
         'lists' => {},
      },
   }

}};

sub hook_process_source {

   my $self = shift;
   my $src  = shift;

   return 1 unless $src -> creator () eq 'Blog::Crawler';

   for my $field (keys %{ $self -> _config () -> get (['blog', 'source', 'lists']) // {} }) {

      next unless $field and $field =~ m![^/]!;

      my @list = Newcomen::Util::String::split_csv ($src -> get ($field) // '');
      my $tgt  = $self -> _config () -> get (['blog', 'source', 'lists', $field]);

      $src -> set ($tgt,   \@list            ) unless $src -> exists ($tgt);
      $src -> set ($field, join (', ', @list));

   }

   return 1;

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::Blog::Source::Lists - Creates meta data lists.

=head1 DESCRIPTION

This plugin will will create meta data lists (i.e. arrayrefs) from existing meta data entries for
all L<Newcomen::Source> instances with the creator ID C<'Blog::Crawler'>. It will be done early
during the I<hook_process_source()> hook (priority C<200>).

The existing (scalar) entries will be split at commas (spaces before and after a comma will be
removed), empty elements will be ignored. The meta data to be processed can be freely configured,
see below. By default, no lists will be created.

B<Note:> For every configured key to be processed a list will be created. This list may be empty, if
the original data was empty or did not exist. Additionally, the original data will be cleaned up: it
will be set to the list joined by C<', '> (comma, followed by a space). Again, this may be an empty
string if the original data was empty or did not exist. However, if the target key already exists in
the meta data, it will not be overridden (the original data will still be cleaned up, though).

=head1 OPTIONS

   {
      'blog'        => {
         'source'   => {
            'lists' => {},
         },
      },
   }

These are the default options set by this plugin. They may be overridden by user configuration.

The I<blog/source/lists> option has to be a hashref (or C<undef>, if you know what you are doing).
The keys of the hashref specify the meta data to be processed, the values specify the name of the
list that will be created for the key. Nested data is supported, using the slash (C<'/'>) as
separator.

For example, to split the meta data I<category> and I<tag>, and store the resulting lists in the
meta data as I<categories> and I<tags>, respectively, use:

   {
      'blog'              => {
         'source'         => {
            'lists'       => {
               'category' => 'categories',
               'tag'      => 'tags',
            },
         },
      },
   }

=head1 META DATA

=head2 Sources

Meta data will be processed and created according to the user configuration. For more details see
the L<DESCRIPTION|/DESCRIPTION> and L<OPTIONS|/OPTIONS>.

=head1 HOOKS

This plugin implements the hook I<hook_process_source()> (priority C<200>).

=head1 SEE ALSO

L<Newcomen::Source>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
