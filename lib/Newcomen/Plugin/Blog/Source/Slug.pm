# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::Blog::Source::Slug;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;

extends 'Newcomen::Plugin::Base';

sub hook_process_source {

   my $self = shift;
   my $src  = shift;

   return 1 unless $src -> creator () eq 'Blog::Crawler';

   my $slug = $src -> get (['slug']);

   unless ($slug) {

      $slug = $src -> get (['path', 'source', 'basename']);
      confess 'No basename set for ' . $src -> id () unless $slug;

      $slug =~ s/\..+?$//;

   }

   $slug =~ s!/!-!g;
   $slug =~ s/[^a-zA-Z0-9._-]/-/ag;
   $slug =~ s/-+\.|\.-+/./g;
   $slug =~ s/\.+/./g;
   $slug =~ s/-+/-/g;
   $slug =~ s/^[.-]+|[.-]+$//g;

   confess 'Could not set slug for ' . $src -> id () unless $slug;

   $src -> set (['slug'], $slug);

   return 1;

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::Blog::Source::Slug - Sets the source item's slug.

=head1 DESCRIPTION

This plugin will set the "slug" for all blog sources (all L<Newcomen::Source> instances with the
creator ID C<'Blog::Crawler'>), which may later be used in the output file names of single article
pages.

The initial value will be set to the source's meta data I<slug> value, if it exists. If this doesn't
exist, the plugin looks for the meta data value of I<path/source/basename> (this will be set by the
L<Newcomen::Role::Crawler::Filesystem> role used by L<Newcomen::Plugin::Blog::Crawler>). If this is
set, the basename of the source file will be used without the file name extension. If the initial
value could not be set (i.e. both I<slug> and I<path/source/basename> are not set or empty), this
plugin will I<die()>.

The initial value will then be cleaned up, basically the same procedure as is used in the plugin
L<Newcomen::Plugin::Rewrite::Clean>, with the exception that all slashes will be replaced by dashes
initially (so you can not use the slug to create directories).

If, after the clean up, the slug is empty, this plugin will I<die()>, else the slug will be stored
in the source item's meta data as I<slug> (overriding the initial value if it was set).

=head1 META DATA

=head2 Sources

   {
      'slug' => $slug,
   }

The final value of the I<slug> (as describes in the L<DESCRIPTION|/DESCRIPTION>) will be stored in
the meta data.

=head1 HOOKS

This plugin implements the I<hook_process_source()> hook (default priority).

=head1 SEE ALSO

L<Newcomen::Plugin::Blog::Crawler>, L<Newcomen::Plugin::Rewrite::Clean>,
L<Newcomen::Role::Crawler::Filesystem>, L<Newcomen::Source>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
