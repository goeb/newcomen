# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::Blog::Source::Time;
our $VERSION = 2014052501;

use namespace::autoclean;
use Clone;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;
use Newcomen::Util::Time qw( parse_time );

extends 'Newcomen::Plugin::Base';
with    'Newcomen::Role::Attribute::Config';

override '_build_default_config' => sub {{

   'blog'                   => {
      'source'              => {
         'time'             => {
            'input_format'  => undef,
            'output_format' => undef,
         },
      },
   }

}};

sub hook_process_source {

   my $self = shift;
   my $src  = shift;

   return 1 unless $src -> creator () eq 'Blog::Crawler';

   my $id       = $src -> id ();
   my $i_format = $self -> _config () -> get (['blog', 'source', 'time', 'input_format' ]);
   my $o_format = $self -> _config () -> get (['blog', 'source', 'time', 'output_format']);

   $i_format ||= '%Y-%m-%d %H:%M';
   $o_format ||= '%Y-%m-%d %H:%M';

   my $modified  = $src -> get (['modified' ]);
   my $published = $src -> get (['published']);

   if ($modified or $published) {

      $src -> set (['modified' ], $published) unless $modified;
      $src -> set (['published'], $modified ) unless $published;

      $modified  = parse_time ($src -> get (['modified' ]), $i_format, $o_format);
      $published = parse_time ($src -> get (['published']), $i_format, $o_format);

   }
   else {

      my $mtime = $src -> get (['stat', 'mtime']);
      confess "No valid mtime for $id" unless $mtime and $mtime =~ /^\d+$/a and $mtime >= 0;

      $modified  = parse_time ($mtime, '%s', $o_format);
      $published = Clone::clone ($modified);

      $src -> set (['modified' ], $modified  -> {'string'});
      $src -> set (['published'], $published -> {'string'});

   }

   $src -> set (['time'], { 'published' => $published, 'modified' => $modified });

   my $old = $self -> _core () -> get (['blog', 'oldest_item']);
   my $new = $self -> _core () -> get (['blog', 'latest_item']);

   if ((not $old) or ($old -> get (['time', 'published', 'epoch']) > $published -> {'epoch'})) {
      $self -> _core () -> set (['blog', 'oldest_item'], $src);
   }

   if ((not $new) or ($new -> get (['time', 'published', 'epoch']) < $published -> {'epoch'})) {
      $self -> _core () -> set (['blog', 'latest_item'], $src);
   }

   return 1;

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::Blog::Source::Time - Sets time information for blog sources.

=head1 DESCRIPTION

This plugin will add time information to the L<Newcomen::Source> instances with the creator ID
C<'Blog::Crawler'>.

Information about the publication and modification time will be included. Please see
L<OPTIONS|/OPTIONS> and L<META DATA|/META DATA> below for details.

=head1 OPTIONS

   {
      'blog'                   => {
         'source'              => {
            'time'             => {
               'input_format'  => undef,
               'output_format' => undef,
            },
         },
      },
   }

These are the default options set by this plugin. They may be overridden by user configuration.

The I<blog/source/time/input_format> must be set to the time format specification that should be
used to parse the published/modified time set in the source's meta data.

I<blog/source/time/output_format> must be set to another (or the same) time format specification.
The parsed time will be reformatted using this format and the result will be included in the meta
data.

If one of these options is set to C<undef> (the default) or any other false value, the default value
C<'%Y-%m-%d %H:%M'> will be used.

The function L<parse_time()|Newcomen::Util::Time/parse_time> (package L<Newcomen::Util::Time>) is
used to do the parsing, see there for more details.

=head1 META DATA

=head2 Sources

   {

      'published'         => $published,
      'modified'          => $modified,

      'time'              => {
         'published'      => {
            'year'        => $pub_year,
            'month'       => $pub_month,
            'day'         => $pub_day,
            'hour'        => $pub_hour,
            'minute'      => $pub_minute,
            'second'      => $pub_second,
            'day_of_week' => $pub_dow,
            'day_of_year' => $pub_doy,
            'week'        => $pub_week,
            'month_short' => $pub_m_short,
            'month_name'  => $pub_m_name,
            'day_short'   => $pub_d_short,
            'day_name',   => $pub_d_name,
            'is_dst'      => $pub_is_dst,
            'epoch'       => $pub_epoch,
            'iso'         => $pub_iso,
            'offset'      => $pub_offset,
            'Month'       => $pub_Month,
            'Day'         => $pub_Day,
            'Hour'        => $pub_Hour,
            'Minute'      => $pub_Minute,
            'Second'      => $pub_Second,
            'string'      => $pub_string,
         },
         'modified'       => {
            'year'        => $mod_year,
            'month'       => $mod_month,
            'day'         => $mod_day,
            'hour'        => $mod_hour,
            'minute'      => $mod_minute,
            'second'      => $mod_second,
            'day_of_week' => $mod_dow,
            'day_of_year' => $mod_doy,
            'week'        => $mod_week,
            'month_short' => $mod_m_short,
            'month_name'  => $mod_m_name,
            'day_short'   => $mod_d_short,
            'day_name',   => $mod_d_name,
            'is_dst'      => $mod_is_dst,
            'epoch'       => $mod_epoch,
            'iso'         => $mod_iso,
            'offset'      => $mod_offset,
            'Month'       => $mod_Month,
            'Day'         => $mod_Day,
            'Hour'        => $mod_Hour,
            'Minute'      => $mod_Minute,
            'Second'      => $mod_Second,
            'string'      => $mod_string,
         },
      },

   }

This is the source item meta data used and/or set by this plugin.

The keys I<published> and I<modified> will be used as initial data for the publication and
modification time of the source item. If set, these keys must be in the format specified by the
I<blog/source/time/input_format> configuration option. If only one of these is set, the other one
will automatically be set to the same value. If none of these keys are set, the source item's
I<stat/mtime> value (provided by L<Newcomen::Plugin::Blog::Source>) will be used to determine both
publication and modification times. In that case both keys will be set to the reformatted value
according to the I<blog/source/time/output_format> configuration option. Note that if neither
I<published>, I<modified> or I<stat/mtime> exists, this plugin will I<die()>.

The two hashrefs I<time/published> and I<time/modified> will contain the parsed information from the
two aforementioned values. These hashrefs are provided by L<Newcomen::Util::Time>'s
L<parse_time()|Newcomen::Util::Time/parse_time> function, see there for details. Note that any
existing I<time> key in the meta data will be overridden.

=head1 GLOBAL DATA

   {
      'blog'           => {
         'oldest_item' => $oldest_item,
         'latest_item' => $latest_item,
      },
   }

This is the global data (see L<Newcomen::Core>) set by this plugin. I<blog/oldest_item> and
I<blog/latest_item> will be set to the oldest and newest blog source items (according to their
publication time), respectively.

=head1 HOOKS

This plugin implements the hook I<hook_process_source()> (default priority).

=head1 SEE ALSO

L<Newcomen::Core>, L<Newcomen::Plugin::Blog::Source>, L<Newcomen::Source>, L<Newcomen::Util::Time>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
