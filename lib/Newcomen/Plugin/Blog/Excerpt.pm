# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::Blog::Excerpt;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;

extends 'Newcomen::Plugin::Base';
with    'Newcomen::Role::Attribute::Config';
with    'Newcomen::Role::Attribute::Site';

override '_build_default_config' => sub {{

   'blog'       => {
      'excerpt' => {},
   }

}};

sub hook_post_format {

   my $self = shift;

   for my $page (grep { $_ -> creator () =~ /^Blog::/ } $self -> _site () -> pages ()) {
      for my $content ($page -> contents ()) {

         my $text = $content -> content ();
         next unless $text;

         for my $key (keys %{ $self -> _config () -> get (['blog', 'excerpt']) // {} }) {

            next if not $key or $key !~ m![^/]! or $content -> exists ($key);

            my ($start, $stop) = @{ $self -> _config () -> get (['blog', 'excerpt', $key]) };

            if    ($text =~ /\Q$start\E/ and $text !~ /\Q$stop\E/) { $text = $text  . $stop; }
            elsif ($text !~ /\Q$start\E/ and $text =~ /\Q$stop\E/) { $text = $start . $text; }

            if ($text =~ /\Q$start\E(.*)\Q$stop\E/s) {
               $content -> set ($key, $1);
            }

         }

      }
   }

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::Blog::Excerpt - Creates excerpts for blog content.

=head1 DESCRIPTION

This plugin extracts parts of the content of all L<Newcomen::Content> instances that belong to pages
with a creator ID starting with C<'Blog::'> and stores the extracted parts in the meta data of the
L<Newcomen::Content> instance under a user defined key.

The part(s) to extract are delimited by a start and a stop string, which can be freely configured.
Multiple parts of the content may be extracted using multiple keys and different start and stop
strings for each key. If neither a start nor a stop string is found in some content, the key will
not be created in the meta data. If at least one of the strings is found, the key will be created.
If only a start string is found, everything from the start string to the end of the content will be
extracted. If only a stop string is found, everything from the start of the content to the stop
string will be extracted. If both are found, everything between the two strings will be extracted.
If one start and/or stop string occurs multiple times, the longest matching part of the content will
be extracted.

Start and stop strings are not interpreted as regular expressions, but as literal strings.

If the L<Newcomen::Content> instance or the L<Newcomen::Source> instance referenced by it already
contain meta data for a configured key, it will not be overridden, even if it is empty or C<undef>.

Extraction of parts of the content will be done after all formatters have been applied, so the
extracted parts of the content are formatted.

=head1 OPTIONS

   {
      'blog'       => {
         'excerpt' => {},
      },
   }

These are the default options set by this plugin. They may be overridden by user configuration.

I<blog/excerpt> has to be a hashref (or C<undef>, if you know what you are doing). Its keys will be
used as meta data keys. Its values have to be arrayrefs, with two elements: the first one being the
start string, the second one being the stop string. Everything between the start and stop strings
will be extracted from the content and stored in the L<Newcomen::Content> instance's meta data under
the specified key. Note that nested meta data structures are supported, using slashes (C</>) as
separators in the key.

By default, nothing will be extracted (with I<blog/excerpt> being an empty hashref).

=head2 Example

To extract everything between C<< '<!--excerpt-->' >> and  C<< '<!--/excerpt-->' >> and store it in
the meta data under the key I<excerpt>, the following configuration may be used:

   {
      'blog'          => {
         'excerpt'    => {
            'excerpt' => ['<!--excerpt-->', '<!--/excerpt-->'],
         },
      },
   }

=head1 META DATA

=head1 Content

Meta data set for the blog's L<Newcomen::Content> instances depends on the configuration. By
default, nothing will be set by this plugin.

=head1 HOOKS

This plugin implements the I<hook_post_format()> hook (default priority).

=head1 SEE ALSO

L<Newcomen::Content>, L<Newcomen::Source>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
