# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::Blog;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;

extends 'Newcomen::Plugin::Base';

# Note: Enabling plugins from within another plugin will only work if the plugins to be enabled are
# sorted after the plugin that enables them. Standard string comparison is used to sort the plugins,
# everything "greater than" "Newcomen::Plugin::Blog" may be enabled in here.

override '_build_default_config' => sub {{

   'plugins' => {
      'Blog::Crawler'                => 1,
      'Blog::Defaults'               => 1,
      'Blog::Excerpt'                => 1,
      'Blog::Feed::Defaults'         => 1,
      'Blog::Feed::Exclude'          => 1,
      'Blog::Feed::Lists'            => 1,
      'Blog::Feed::Lists::Defaults'  => 1,
      'Blog::Feed::Main'             => 1,
      'Blog::Feed::Updated'          => 1,
      'Blog::Feed::UUID'             => 1,
      'Blog::Index::Defaults'        => 1,
      'Blog::Index::Exclude'         => 1,
      'Blog::Index::Lists'           => 1,
      'Blog::Index::Lists::Date'     => 1,
      'Blog::Index::Lists::Defaults' => 1,
      'Blog::Index::Main'            => 1,
      'Blog::Index::Main::Exclude'   => 1,
      'Blog::Formatters'             => 1,
      'Blog::Menu'                   => 1,
      'Blog::Single'                 => 1,
      'Blog::Source'                 => 1,
      'Blog::Source::Empty'          => 1,
      'Blog::Source::Lists'          => 1,
      'Blog::Source::Slug'           => 1,
      'Blog::Source::Time'           => 1,
   },

}};

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::Blog - Enable all blog plugins.

=head1 DESCRIPTION

At the moment this plugin does nothing more than automatically enabling all the other blog plugins,
thus avoiding a long list of C<'Blog::*'> lines in the configuration file. Individual plugins may of
course be disabled by the user's configuration if required.

=head1 OPTIONS

   {
      'plugins' => {
         'Blog::Crawler'                => 1,
         'Blog::Defaults'               => 1,
         'Blog::Excerpt'                => 1,
         'Blog::Feed::Defaults'         => 1,
         'Blog::Feed::Exclude'          => 1,
         'Blog::Feed::Lists'            => 1,
         'Blog::Feed::Lists::Defaults'  => 1,
         'Blog::Feed::Main'             => 1,
         'Blog::Feed::Updated'          => 1,
         'Blog::Feed::UUID'             => 1,
         'Blog::Index::Defaults'        => 1,
         'Blog::Index::Exclude'         => 1,
         'Blog::Index::Lists'           => 1,
         'Blog::Index::Lists::Date'     => 1,
         'Blog::Index::Lists::Defaults' => 1,
         'Blog::Index::Main'            => 1,
         'Blog::Index::Main::Exclude'   => 1,
         'Blog::Formatters'             => 1,
         'Blog::Menu'                   => 1,
         'Blog::Single'                 => 1,
         'Blog::Source'                 => 1,
         'Blog::Source::Empty'          => 1,
         'Blog::Source::Lists'          => 1,
         'Blog::Source::Slug'           => 1,
         'Blog::Source::Time'           => 1,
      },
   }

These are the default options set by this plugin. They may be overridden by user configuration.

All the blog plugins will be enabled if this plugin is enabled. To disable a plugin, the appropriate
option has to be included in the user configuration and set to a false value.

=head1 SEE ALSO

L<Newcomen::Plugin::Blog::Crawler>,
L<Newcomen::Plugin::Blog::Defaults>,
L<Newcomen::Plugin::Blog::Excerpt>,
L<Newcomen::Plugin::Blog::Feed::Defaults>,
L<Newcomen::Plugin::Blog::Feed::Exclude>,
L<Newcomen::Plugin::Blog::Feed::Lists>,
L<Newcomen::Plugin::Blog::Feed::Lists::Defaults>,
L<Newcomen::Plugin::Blog::Feed::Main>,
L<Newcomen::Plugin::Blog::Feed::Updated>,
L<Newcomen::Plugin::Blog::Feed::UUID>,
L<Newcomen::Plugin::Blog::Index::Defaults>,
L<Newcomen::Plugin::Blog::Index::Exclude>,
L<Newcomen::Plugin::Blog::Index::Lists>,
L<Newcomen::Plugin::Blog::Index::Lists::Date>,
L<Newcomen::Plugin::Blog::Index::Lists::Defaults>,
L<Newcomen::Plugin::Blog::Index::Main>,
L<Newcomen::Plugin::Blog::Index::Main::Exclude>,
L<Newcomen::Plugin::Blog::Formatters>,
L<Newcomen::Plugin::Blog::Menu>,
L<Newcomen::Plugin::Blog::Single>,
L<Newcomen::Plugin::Blog::Source>,
L<Newcomen::Plugin::Blog::Source::Empty>,
L<Newcomen::Plugin::Blog::Source::Lists>,
L<Newcomen::Plugin::Blog::Source::Slug>,
L<Newcomen::Plugin::Blog::Source::Time>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
