# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::Newcomen;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;

extends 'Newcomen::Plugin::Base';

override '_build_default_config' => sub {{

   'VERSION'  => '2014052501',
   'ROOT_DIR' => '.',

}};

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::Newcomen - Plugin defining some general options.

=head1 DESCRIPTION

This plugin does nothing. It just adds I<VERSION> and I<ROOT_DIR> options to the configuration. Both
should not be changed, unless you know what you are doing.

=head1 OPTIONS

   {
      'VERSION'  => '2014052501',
      'ROOT_DIR' => '.',
   }

The following default settings are set by I<Newcomen::Plugin::Newcomen>:

   'VERSION' => '2014052501'

Not really a configuration setting. This contains the version information of this release of
B<Newcomen>. There is no reason to change this, it is included for informational purposes only.

   'ROOT_DIR' => '.'

This sets the root directory of an B<Newcomen> project. The default setting C<'.'> specifies the
current directory, i.e. the working directory you run the F<newcomen> script from.
B<< Do not change this setting! >> Seriously, changing this has never been tested, and if you change
it, a kitten may die a horrible death. You don't want to kill a kitten, do you?

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
