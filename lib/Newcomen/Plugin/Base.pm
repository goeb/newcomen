# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::Base;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use Moose::Util::TypeConstraints;
use MooseX::StrictConstructor;

with 'Newcomen::Role::Attribute::Core';

subtype 'HookOrder'
   => as    'Int'
   => where { $_ >= 0 and $_ <= 1000 };

has '_default_config'      => (
   'is'                    => 'ro',
   'isa'                   => 'HashRef',
   'builder'               => '_build_default_config',
   'lazy'                  => 1,
   'init_arg'              => undef,
);

has '_hook_order'          => (
   'is'                    => 'ro',
   'isa'                   => 'HashRef[HookOrder]',
   'builder'               => '_build_hook_order',
   'lazy'                  => 1,
   'init_arg'              => undef,
   'traits'                => ['Hash'],
   'handles'               => {
      '_hook_order_exists' => 'exists',
      '_hook_order_get'    => 'get',
   },
);

sub _build_default_config {{}}
sub _build_hook_order     {{}}

sub hook_order {

   my $self = shift;
   my $hook = shift;

   return $self -> _hook_order_exists ($hook) ? $self -> _hook_order_get ($hook) : 500;

}

sub BUILD {

   my $self = shift;
   my $conf = $self -> _default_config ();

   $self -> _core () -> config () -> add_default ($conf) if %$conf;

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::Base - Base class for Newcomen plugins.

=head1 SYNOPSIS

   package Newcomen::Plugin::MyPlugin;

   use namespace::autoclean;
   use Moose;

   extends 'Newcomen::Plugin::Base';

   # Add defaults for the plugins config options:

   override '_build_default_config' => sub {{
      'foo'    => {
         'bar' => 'some stuff',
      }
   }};

   # Set a custom hook order value for a hook:

   override '_build_hook_order' => sub {{
      'some_hook_name' => 100,
   }};

   __PACKAGE__ -> meta () -> make_immutable ();

   1;

=head1 DESCRIPTION

Base class for B<Newcomen> plugins. Every plugin must extend this base class.

It defines the attributes L<_default_config|/_default_config> and L<_hook_order|/_hook_order>,
including two handles I<_hook_order_exists()> and I<_hook_order_get()>, the private methods
L<_build_default_config()|/build_default_config> and L<_build_hook_order()|/_build_hook_order>, and
the public method L<hook_order()|/hook_order> (and of course L<new()|/new> provided by L<Moose>). It
also consumes the core role L<Newcomen::Role::Attribute::Core>, providing the L<_core()|/core>
method (and the builder for the I<_core> attribute: I<_build_core()>).

A subclass must not override any of these, except for the methods
L<_build_default_config()|/build_default_config> and L<_build_hook_order()|/_build_hook_order> if
required. See below.

The default configuration of the plugin will be added to the global L<Newcomen::Config> instance
after construction. This is done automatically, the plugin just needs to override the builder of the
default configuration, see below.

=head1 CLASS METHODS

=head2 new

   my $plugin = Newcomen::Plugin::Foo -> new ();

Constructor. Expects no parameters.

=head1 INSTANCE METHODS

=head2 hook_order

   my $order = $plugin -> hook_order ($hook_name);

Returns the order number for that plugin and hook. See L<_build_hook_order()|/_build_hook_order>
below for more details. It should not be required to call this method directly, instead use
L<Newcomen::Plugins>' L<sort()|Newcomen::Plugins/sort> method to get a sorted list of plugins
implementing a hook.

=head1 MISCELLANEOUS

=head2 Methods To Be Overridden

The following two methods must be overridden if a plugin wants to add configuration defaults or
define a custom hook order for one of the hooks it implements:

=head3 _build_default_config

   override '_build_default_config' => sub { return $config_hashref };

If overridden, this method must return a hashref containing the default values for the plugin's
configuration options. The hashref may contain other hashrefs or arrayrefs as required. It may also
be empty, which is basically the same as not overriding this method. See L<Newcomen::Config> for
details on configuration handling. Also see the L<SYNOPSIS|/SYNOPSIS> above for an example on how to
use this method.

=head3 _build_hook_order

   override '_build_hook_order' => sub {
      return {
         $hook_name => $hook_priority,
         # ...
      };
   };

If overridden, this method must return a hashref. The keys must be names of hooks, the values must
be integers specifying the plugin priority for this hook, between C<0> and C<1000>. When hooks are
run, the priority of the plugin is checked for all plugins implementing the hook. Lower numbers will
cause the plugin's hook to be run before plugins defining a higher number for this hook. The default
for any hook is C<500>. Also see the L<SYNOPSIS|/SYNOPSIS> for an example.

=head2 Attributes And Handles

The following attributes are defined in the base class. There is usually no need to use any of these
directly in the plugins, except for the L<_core|/_core> attribute.

=head3 _default_config

A hashref storing the plugin's default configuration settings. The builder of this attribute is the
L<_build_default_config()|/_build_default_config> method mentioned above. The attribute itself is
read only and may only be set by the builder.

=head3 _hook_order

A hashref of I<HookOrder> values (integers between C<0> and C<1000>), the keys are the hook names.
The builder of this attribute is the L<_build_hook_order()|/_build_hook_order> method mentioned
above. Again, this attribute is read only. It defines two handles, I<_hook_order_exists()> and
I<_hook_order_get()>, see below.

=head3 _hook_order_exists, _hook_order_get

Mappings for L<Moose>'s L<native traits|Moose::Meta::Attribute::Native::Trait::Hash> I<exists()> and
I<get()>, respectively. See L<Moose::Meta::Attribute::Native::Trait::Hash> for more details.

=head3 _core

Returns the global L<Newcomen::Core> instance. This attribute is provided by the role
L<Newcomen::Role::Attribute::Core>. The builder of this attribute is called I<_build_core()>.

=head1 SEE ALSO

L<Moose>, L<Moose::Meta::Attribute::Native::Trait::Hash>, L<Newcomen::Config>, L<Newcomen::Core>,
L<Newcomen::Plugins>, L<Newcomen::Role::Attribute::Core>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
