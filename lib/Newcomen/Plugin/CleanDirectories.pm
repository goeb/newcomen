# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Plugin::CleanDirectories;
our $VERSION = 2014052501;

use namespace::autoclean;
use Capture::Tiny;
use File::Path qw( remove_tree );
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;
use Newcomen::Util::Path qw( resolute );

extends 'Newcomen::Plugin::Base';
with    'Newcomen::Role::Attribute::Config';

override '_build_default_config' => sub {{

   'cleandirectories' => {
      'dirs'          => undef,
      'verbose'       => undef,
   },

}};

sub hook_pre_init {

   my $self = shift;
   my $root = $self -> _config () -> get (['ROOT_DIR']);
   my $dirs = $self -> _config () -> get (['cleandirectories', 'dirs'   ]) // [];
   my $verb = $self -> _config () -> get (['cleandirectories', 'verbose']) // 1;

   my $abs_root = resolute (Path::Class::dir ($root));
   confess 'No valid root directory' unless $root and -d $abs_root -> stringify ();

   $dirs = [$dirs] unless ref $dirs eq 'ARRAY';

   for my $dir (@$dirs) {

      my $abs_dir = resolute ($abs_root -> subdir ($dir));

      if ($abs_root -> contains ($abs_dir) and -d $abs_dir -> stringify ()) {
         my $stdout = Capture::Tiny::capture_stdout {
            remove_tree (
               $abs_dir -> stringify (), { 'keep_root' => 1, 'safe' => 1, 'verbose' => 1 }
            );
         };
         if ($verb and $stdout) {
            $stdout =~ s/^/CLN /gm;
            $stdout =~ s/rmdir/rmdir /g;
            print $stdout;
         }
      }

   }

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Plugin::CleanDirectories - Empty directories before building the project.

=head1 DESCRIPTION

This plugin may be used to empty directories before the actual build process begins. The plugin
should die if some error occurs, subject to L<File::Path>'s error handling (L<File::Path>'s
I<remove_tree()> function is used internally). No directories will be emptied by default.

=head1 OPTIONS

   {
      'cleandirectories' => {
         'dirs'          => undef,
         'verbose'       => undef,
      },
   }

These are the defaults set by this plugin, they may be overridden by user configuration.

If directories should be be emptied before the build process begins, I<cleandirectories/dirs> must
be set to either an arrayref containing zero or more strings specifying these directories, or a
single string if only one directory should be emptied. Directories must be specified as paths
relative to the project's root directory. Setting I<cleandirectories/dirs> to C<undef> (the default)
has the same effect as setting it to an empty arrayref (i.e. nothing will be deleted in that case).

If the I<cleandirectories/verbose> option is set to a true value, a list of all files/directories
deleted will be printed once a directory has been emptied. The output will be one file/directory per
line (the output of L<File::Path>'s I<remove_tree()> function, prefixed by the string C<'CLN '>).

B<Note:> For security reasons all paths will be transformed to resolved absolute paths. If a
configured directory is not a (direct or indirect) child of the project's root directory (as set by
the I<ROOT_DIR> option), its contents will not be deleted.

=head1 HOOKS

This plugin implements the I<hook_pre_init()> hook (default priority).

=head1 SEE ALSO

L<File::Path>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
