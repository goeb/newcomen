# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Role::Frontend;
our $VERSION = 2014052501;

use namespace::autoclean;
use Module::Find ();
use MooseX::Role::Parameterized;

parameter 'namespace' => ( 'isa' => 'Str', 'required' => 1 );
parameter 'type'      => ( 'isa' => 'Str', 'required' => 1 );

role {

   my $parms = shift;
   my $ns    = $parms -> namespace ();
   my $type  = $parms -> type ();

   has '_backends'        => (
      'is'                => 'ro',
      'isa'               => "HashRef[$type]",
      'builder'           => '_build_backends',
      'lazy'              => 1,
      'init_arg'          => undef,
      'traits'            => ['Hash'],
      'handles'           => {
         'backend'        => 'get',
         'backend_exists' => 'exists',
         'backends'       => 'keys',
      }
   );

   method '_build_backends' => sub {

      my $self     = shift;
      my $backends = {};

      confess 'Backend namespace and/or type invalid' unless $ns and $type;

      for my $backend (Module::Find::useall ($ns)) {
         next if ($backend eq $type) or (not $backend -> isa ($type)) or ($backends -> {$backend});
         $backends -> {$backend} = $backend -> new ();
      }

      return $backends;

   }

};

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Role::Frontent - Moose role for frontend components.

=head1 SYNOPSIS

   # In the consuming class:

   with 'Newcomen::Role::Frontend' => {
      'namespace' => 'Foo::Bar',
      'type'      => 'Foo::Bar::Backend',
   };

   # Get a backend instance:
   my $backend = $self -> backend ($backend_name);

=head1 DESCRIPTION

This is a L<Moose> role to enable a class to act as a frontend for several backends. Backends will
be searched and instantiated automatically as soon as required (and as late as possible, but as soon
as one backend is accessed, all are instantiated). All backends must have a constructor called
I<new()> (created automatically for L<Moose> classes).

=head1 OPTIONS

There are two parameters for this role: I<namespace> specifies the namespace to look for backends,
e.g. in the L<SYNOPSIS|/SYNOPSIS> example above backends must be put in the I<Foo::Bar> namespace,
I<Foo::Bar::Baz> would be a valid example for a backend name. The second parameter I<type> must
match the backend class (or one of its superclasses). In the example, I<Foo::Bar::Baz> must extend
I<Foo::Bar::Backend>. The module I<type> itself will always be skipped when loading the backends.

=head1 INSTANCE METHODS

The following methods allow access to the backends. They will be available in the consuming class.

=head2 backend

   my $backend = $self -> backend ($full_name);

Returns a backend instance. The parameter must be the full module name. This name is case sensitive.
Returns C<undef> if the backend does not exist.

=head2 backend_exists

   if ($self -> backend_exists ($full_name)) {
      # do something...
   }

Returns a true value if the backend (specified by its full case sensitive module name) exists.

=head2 backends

   my @backends = $self -> backends ();

Returns a list containing the full module names of all available backends.

=head1 MISCELLANEOUS

This role uses a I<_build_backends()> method as a builder. The consuming class must not use this
name itself.

=head1 SEE ALSO

L<Moose>, L<Newcomen::Config>, L<Newcomen::Formatter>, L<Newcomen::Renderer>, L<Newcomen::Writer>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
