# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Role::Backend::Specification;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose::Role;
use Newcomen::Data;

has 'name'    => (
   'is'       => 'ro',
   'isa'      => 'Str',
   'required' => 1,
);

has 'options' => (
   'is'       => 'ro',
   'isa'      => 'Newcomen::Data',
   'init_arg' => undef,
   'default'  => sub { Newcomen::Data -> new () },
   'handles'  => [qw( delete exists get set )],
);

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Role::Backend::Specification - Moose role for backend specifications.

=head1 SYNOPSIS

   package Newcomen::Foo::Specification;

   use namespace::autoclean;
   use Moose;

   with 'Newcomen::Role::Backend::Specification';

   __PACKAGE__ -> meta () -> make_immutable ();

   1;

=head1 DESCRIPTION

This role will add the attributes I<name> and I<options> and the methods mentioned below to the
consuming class. The I<name> attribute is of the string type, I<options> is an L<Newcomen::Data>
instance. Both will be read only, and I<name> will be required (i.e. it has to be supplied to the
consuming class' constructor). It will not be possible to set the I<options> from the constructor,
an empty L<Newcomen::Data> instance will be used initially.

Consuming this role is sufficient for a complete backend specification class, see L</SYNOPSIS> for
an example.

=head1 INSTANCE METHODS

=head2 General

=head3 name

   my $name = $spec -> name ();

Returns the name set for the backend specification. The name can only be set using the constructor.

=head2 Options

The backend options are stored in an L<Newcomen::Data> instance. Supported options may differ for
different backends, see the individual backends for details.

=head3 delete, exists, get, set

The methods I<delete()>, I<exists()>, I<get()> and I<set()> are mapped to the L<Newcomen::Data>
methods of the same name, see there for details.

=head3 options

   my $options = $spec -> options ();

Returns the L<Newcomen::Data> instance storing the options. The options will be empty initially.

=head1 SEE ALSO

L<Moose>, L<Newcomen::Data>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
