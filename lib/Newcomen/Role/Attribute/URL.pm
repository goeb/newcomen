# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Role::Attribute::URL;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose::Role;

with 'Newcomen::Role::Attribute::Component' => { 'component' => 'URL' };

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Role::Attribute::URL - Moose role for easy access to the URL map.

=head1 SYNOPSIS

   # In the consuming class:

   with 'Newcomen::Role::Attribute::Core';
   with 'Newcomen::Role::Attribute::URL';

   # Then _url() is available:
   my $url = $self -> _url ();

=head1 DESCRIPTION

I<Newcomen::Role::Attribute::URL> adds the I<_url> attribute to the consuming class. This may be
used to access the L<Newcomen::URL> instance (as a getter: L<_url()|/_url>). It may be used by any
class that requires access to it.

Note that this role requires a I<_core()> method to be available, which must return the global core
(L<Newcomen::Core>) instance. Consuming the role L<Newcomen::Role::Attribute::Core> is the easiest
way to satisfy this requirement. This has to be done before consuming the URL role, see the
L<SYNOPSIS|/SYNOPSIS> above.

=head1 INSTANCE METHODS

Note that these methods are available in the consuming class, I<Newcomen::Role::Attribute::URL> is
just a L<Moose> role.

=head2 _url

   my $url = $self -> _url ();

Returns the L<Newcomen::URL> instance (as provided by L<Newcomen::Core>'s I<url()> method).

=head1 MISCELLANEOUS

This role also defines the method I<_build_url()>, the builder for the I<_url> attribute. Do not use
this name for anything in the consuming class. The builder is lazy, i.e. the attribute will be
initialized on first access. The attribute will be read only, and it may not be set using the
constructor.

=head1 SEE ALSO

L<Moose>, L<Newcomen::Core>, L<Newcomen::Role::Attribute::Core>, L<Newcomen::URL>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
