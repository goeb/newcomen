# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Role::Attribute::Component;
our $VERSION = 2014052501;

use namespace::autoclean;
use MooseX::Role::Parameterized;

parameter 'component' => ( 'isa' => 'Str', 'required' => 1 );

role {

   my $parms = shift;
   my $comp  = $parms -> component ();
   my $lcomp = lc $comp;

   requires '_core';

   has "_$lcomp" => (
      'is'       => 'ro',
      'isa'      => "Newcomen::$comp",
      'init_arg' => undef,
      'builder'  => "_build_$lcomp",
      'lazy'     => 1,
   );

   method "_build_$lcomp" => sub { shift -> _core () -> $lcomp () }

};

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Role::Attribute::Component - Moose role (parameterized) for components.

=head1 SYNOPSIS

The consuming role for a specific component (I<Foo>):

   package Newcomen::Role::Attribute::Foo;

   use namespace::autoclean;
   use Moose::Role;

   with 'Newcomen::Role::Attribute::Component' => { 'component' => 'Foo' };

   1;

In the consuming class:

   with 'Newcomen::Role::Attribute::Core';
   with 'Newcomen::Role::Attribute::Foo';

   # Then _foo() is available:
   my $foo = $self -> _foo ();

=head1 DESCRIPTION

This parameterized L<Moose> role may be used to create other roles that provide an attribute to the
consuming classes to easily access a specific B<Newcomen> component. All components provided by the
core (see L<Newcomen::Core>) are supported.

=head1 OPTIONS

In the consuming role, the I<component> has to be specified:

   with 'Newcomen::Role::Attribute::Component' => { 'component' => 'Foo' };

This example would create the attribute I<_foo>, that can be used as a getter by consuming classes
to access the I<Foo> component. The core has to provide a I<foo()> method in that case. Both the
attribute name and the core's method name will be derived from the supplied I<component> parameter
by converting it to lowercase. The type of the attribute must match the component name prefixed with
C<'Newcomen::'>, in the example this would be I<Newcomen::Foo>.

It is required that the consuming class provides a I<_core()> method that returns the
L<Newcomen::Core> instance. Consuming the role L<Newcomen::Role::Attribute::Core> is the easiest
way to satisfy this requirement. The I<_foo()> method is only a shortcut to
C<< $instance->_core()->foo() >>:

   # In the consuming class:

   with 'Newcomen::Role::Attribute::Core';
   with 'Newcomen::Role::Attribute::Foo';

   # Using Newcomen::Role::Attribute::Foo's attribute:
   my $foo = $self -> _foo ();

   # Same value, using only the core attribute:
   my $foo = $self -> _core () -> foo ();

A builder for the attribute will also be created. In the example the builder would be a method named
I<_build_foo()> (the all lowercase I<component> parameter, prefixed by C<'_build_'>). The consuming
class must not use this name for one of its own methods. The builder is called on first access to
the attribute (i.e. the attribute is set to be I<lazy>).

=head1 SEE ALSO

L<Moose>, L<Newcomen::Core>, L<Newcomen::Role::Attribute::Core>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
