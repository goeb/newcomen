# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Crawler;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::Aliases;
use MooseX::StrictConstructor;
use Newcomen::Util::Traits;

{

   my $methods = Newcomen::Util::Traits::std_hash_handles ();

   delete $methods -> {'set'};
   $methods -> {'_set'} = 'set';

   has '_sources' => (
      'is'        => 'ro',
      'isa'       => 'HashRef[Newcomen::Source]',
      'init_arg'  => undef,
      'default'   => sub {{}},
      'traits'    => ['Hash'],
      'handles'   => $methods,
   );

}

alias 'ids'     => 'keys';
alias 'source'  => 'get';
alias 'sources' => 'values';

sub add {

   my $self = shift;
   my $src  = shift;

   confess 'Source ID not unique: ' . $src -> id () if $self -> exists ($src -> id ());

   $self -> _set ($src -> id (), $src);

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Crawler - Manages the sources of an Newcomen project.

=head1 SYNOPSIS

   use Newcomen::Crawler;

   my $crawler = Newcomen::Crawler -> new ();

   # Add a source item:
   $crawler -> add ($source);

   # Get a source item:
   my $source = $crawler -> source ($source_id);

   # Iterate over all sources:
   for my $source ($crawler -> sources ()) {
      # do something...
   }

=head1 DESCRIPTION

I<Newcomen::Crawler> is a container for the source items (L<Newcomen::Source> instances) of the
B<Newcomen> project. See the developer section in L<Newcomen::Manual> for a description of how
B<Newcomen> works and how the crawler fits in.

=over

B<Note:> Despite its name this class can not be used to gather the source items (e.g. from the file
system or a database). It is just a container class. Plugins may - for example - use the role
L<Newcomen::Role::Crawler::Filesystem> to get sources from the file system.

=back

Plugins may use L<Newcomen::Role::Attribute::Crawler> to access the main crawler instance.

=head1 CLASS METHODS

=head2 new

   my $crawler = Newcomen::Crawler -> new ();

Constructor. Expects no parameters, the sources list will be empty initially.

=head1 INSTANCE METHODS

I<Newcomen::Crawler> uses a hashref to store the source items, with the source IDs as keys, and the
source instances as values. It uses the L<hash methods|Newcomen::Util::Traits/Hash_Methods> of
L<Newcomen::Util::Traits>, with the following exceptions and additions:

=over

=item *

There is no L<set()|Newcomen::Util::Traits/set> method. L<add()|/add> must be used to add a source
instance (see below).

=item *

The I<Newcomen::Crawler> method I<ids()> is an alias for the L<keys()|Newcomen::Util::Traits/keys>
method.

=item *

The I<Newcomen::Crawler> method I<source()> is an alias for the L<get()|Newcomen::Util::Traits/get>
method.

=item *

The I<Newcomen::Crawler> method I<sources()> is an alias for the
L<values()|Newcomen::Util::Traits/values> method.

=back

Please see L<Newcomen::Util::Traits> for a complete list of methods and more details.

=head2 add

   $crawler -> add ($source);

This method adds a source (an L<Newcomen::Source> instance) to the crawler. The source instance may
then be accessed by its source ID. The ID must be unique, trying to add an item with an ID that
already exists in the crawler's list will cause an error (the method will I<die()>).

=head1 SEE ALSO

L<Newcomen::Manual>, L<Newcomen::Role::Attribute::Crawler>, L<Newcomen::Role::Crawler::Filesystem>,
L<Newcomen::Source>, L<Newcomen::Util::Traits>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
