# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Site;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::Aliases;
use MooseX::StrictConstructor;
use Newcomen::Util::Traits;

{

   my $methods = Newcomen::Util::Traits::std_hash_handles ();

   delete $methods -> {'set'};
   $methods -> {'_set'} = 'set';

   has '_pages'  => (
      'is'       => 'ro',
      'isa'      => 'HashRef[Newcomen::Page]',
      'init_arg' => undef,
      'default'  => sub {{}},
      'traits'   => ['Hash'],
      'handles'  => $methods,
   );

}

alias 'page'    => 'get';
alias 'pages'   => 'values';
alias 'targets' => 'keys';

sub add {

   my $self = shift;
   my $page = shift;

   confess 'Page target not unique: ' . $page -> target () if $self -> exists ($page -> target ());

   $self -> _set ($page -> target (), $page);

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Site - Manages all pages of the project.

=head1 SYNOPSIS

   use Newcomen::Site;

   my $site = Newcomen::Site -> new ();

   # Add a page:
   $site -> add ($page);

   # Get a page:
   my $page = $site -> page ($target);

   # Iterate over all pages:
   for my $page ($site -> pages ()) {
      # ...
   }

=head1 DESCRIPTION

I<Newcomen::Site> is a container for the pages of the B<Newcomen> project (see L<Newcomen::Page>).
See the developer section in L<Newcomen::Manual> for a description of how B<Newcomen> works and how
I<Newcomen::Site> fits in.

Plugins may use L<Newcomen::Role::Attribute::Site> to access the main I<Newcomen::Site> instance.

=head1 CLASS METHODS

=head2 new

   my $site = Newcomen::Site -> new ();

Constructor. No parameters, the page list will be empty initially.

=head1 INSTANCE METHODS

I<Newcomen::Site> uses a hashref to store the pages, with the page targets as keys, and the page
instances as values. It uses the L<hash methods|Newcomen::Util::Traits/Hash_Methods> of
L<Newcomen::Util::Traits>, with the following exceptions and additions:

=over

=item *

There is no L<set()|Newcomen::Util::Traits/set> method. L<add()|/add> must be used to add a page
(see below).

=item *

The I<Newcomen::Site> method I<targets()> is an alias for the L<keys()|Newcomen::Util::Traits/keys>
method.

=item *

The I<Newcomen::Site> method I<page()> is an alias for the L<get()|Newcomen::Util::Traits/get>
method.

=item *

The I<Newcomen::Site> method I<pages()> is an alias for the
L<values()|Newcomen::Util::Traits/values> method.

=back

Please see L<Newcomen::Util::Traits> for a complete list of methods and more details.

=head2 add

   $site -> add ($page);

This method adds a page (an L<Newcomen::Page> instance, the only parameter) to the site. The page
may then be accessed by its target. The target must be unique, trying to add a page with a target
that already exists in the list will cause a fatal error.

=head1 SEE ALSO

L<Newcomen::Manual>, L<Newcomen::Page>, L<Newcomen::Role::Attribute::Site>,
L<Newcomen::Util::Traits>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
