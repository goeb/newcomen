# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Core;
our $VERSION = 2014052501;

use feature 'state';
use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;
use Newcomen::Catalog;
use Newcomen::Crawler;
use Newcomen::Data;
use Newcomen::Formatter;
use Newcomen::Plugins;
use Newcomen::Renderer;
use Newcomen::Site;
use Newcomen::URL;
use Newcomen::Writer;

has 'config'     => (
   'is'          => 'ro',
   'isa'         => 'Newcomen::Config',
   'required'    => 1,
);

has 'global'     => (
   'is'          => 'ro',
   'isa'         => 'Newcomen::Data',
   'default'     => sub { Newcomen::Data -> new () },
   'init_arg'    => undef,
   'handles'     => [qw( delete exists get set )],
);

for (qw( Catalog Crawler Formatter Plugins Renderer Site URL Writer )) {
   has lc $_     => (
      'is'       => 'ro',
      'isa'      => "Newcomen::$_",
      'builder'  => lc "_build_$_",
      'lazy'     => 1,
      'init_arg' => undef,
   );
}

sub _instances {
   state  $instances = {};
   return $instances;
}

sub instance {

   my $package  = shift;
   my %options  = @_;
   my $instance = $options {'INSTANCE'} // 'DEFAULT';

   # Creation of different instances disabled for now (remove this next line to enable it again):
   $instance = 'DEFAULT';

   delete $options {'INSTANCE'} if exists $options {'INSTANCE'};

   unless (exists $package -> _instances () -> {$instance}) {
      $package -> _instances () -> {$instance} = $package -> new (%options);
   }

   return $package -> _instances () -> {$instance};

}

sub clean_all {
   my $self = shift;
   delete $self -> _instances () -> {$_} for keys %{ $self -> _instances () };
}

sub _build_catalog   { Newcomen::Catalog   -> new () }
sub _build_crawler   { Newcomen::Crawler   -> new () }
sub _build_formatter { Newcomen::Formatter -> new () }
sub _build_plugins   { Newcomen::Plugins   -> new () }
sub _build_renderer  { Newcomen::Renderer  -> new () }
sub _build_site      { Newcomen::Site      -> new () }
sub _build_url       { Newcomen::URL       -> new () }
sub _build_writer    { Newcomen::Writer    -> new () }

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Core - Provides access to the different components of Newcomen.

=head1 SYNOPSIS

   use Newcomen::Core;

   # Singleton access - create new or return existing instance:
   my $core = Newcomen::Core -> instance ('config' => $config);

   # Access the configuration:
   $core -> config () -> get (['some', 'option']);

   # Set global meta data:
   $core -> set (['some', 'data'], 'value');

   # Get the catalog instance:
   my $catalog = $core -> catalog ();

=head1 DESCRIPTION

The I<Newcomen::Core> class glues together the different components of B<Newcomen>. It is used as a
singleton class, every plugin may retrieve the global core instance and thus get access to all the
components. Additionally, I<Newcomen::Core> provides a global meta data storage.

Plugins may use L<Newcomen::Role::Attribute::Core> to access the core instance.

=head1 CLASS METHODS

=head2 instance

   # First call, creating the instance:
   my $core = Newcomen::Core -> instance ('config' => $config);

   # All subsequent calls (e.g. from plugins):
   my $core = Newcomen::Core -> instance ();

Returns the global I<Newcomen::Core> instance. The I<config> parameter is only required the first
time it is called (this happens internally in L<Newcomen>), it has to be an L<Newcomen::Config>
instance. The core instance will be created when I<instance()> is first called. Parameters will be
ignored once the instance has been created.

=head2 clean_all

   Newcomen::Core -> clean_all ();

Deletes the instance if it exists. The next call to L<instance()|/instance> will return a new
I<Newcomen::Core> instance.

=head2 new

Constructor. Not to be used directly, use L<instance()|/instance> instead.

=head1 INSTANCE METHODS

=head2 Newcomen Components

=head3 catalog, config, crawler, formatter, plugins, renderer, site, url, writer

   my $config  = $core -> config ();
   my $crawler = $core -> crawler ();
   # ...

These methods return the B<Newcomen> component of the same name, e.g. I<config()> returns the
L<Newcomen::Config> instance, I<crawler()> returns the L<Newcomen::Crawler> instance etc. All of
these will be instantiated by the core on first access to the component.

=head2 Global Data Storage

Arbitrary globally available data may be stored in the I<Newcomen::Core> instance. The data will be
stored in an L<Newcomen::Data> instance. There is no initial meta data. The following methods are
mapped to the L<Newcomen::Data> methods of the same name and may be used to access the global data:

=head3 delete, exists, get, set

Please see L<Newcomen::Data>'s documentation for details.

=head3 global

   my $data = $core -> global ();

This method will return the L<Newcomen::Data> instance used as the global data storage.

=head1 SEE ALSO

L<Newcomen::Catalog>, L<Newcomen::Config>, L<Newcomen::Crawler>, L<Newcomen::Data>,
L<Newcomen::Formatter>, L<Newcomen::Plugins>, L<Newcomen::Renderer>,
L<Newcomen::Role::Attribute::Core>, L<Newcomen::Site>, L<Newcomen::URL>, L<Newcomen::Writer>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
