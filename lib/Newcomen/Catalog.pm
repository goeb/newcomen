# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Catalog;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::Aliases;
use MooseX::StrictConstructor;
use Newcomen::Util::Traits;

{

   my $methods = Newcomen::Util::Traits::std_hash_handles ();

   delete $methods -> {'set'};
   $methods -> {'_set'} = 'set';

   has '_collections' => (
      'is'            => 'ro',
      'isa'           => 'HashRef[Newcomen::Collection]',
      'init_arg'      => undef,
      'default'       => sub {{}},
      'traits'        => ['Hash'],
      'handles'       => $methods,
   );

}

alias 'ids'         => 'keys';
alias 'collection'  => 'get';
alias 'collections' => 'values';

sub add {

   my $self = shift;
   my $coll = shift;

   confess 'Collection ID not unique: ' . $coll -> id () if $self -> exists ($coll -> id ());

   $self -> _set ($coll -> id (), $coll);

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Catalog - Manages all source collections of the project.

=head1 SYNOPSIS

   use Newcomen::Catalog;

   my $catalog = Newcomen::Catalog -> new ();

   # Add a collection:
   $catalog -> add ($collection);

   # Get a collection:
   my $collection = $catalog -> collection ($collection_id);

   # Iterate over all collections:
   for my $collection ($catalog -> collections ()) {
      # ...
   }

=head1 DESCRIPTION

I<Newcomen::Catalog> is a container for the source collections of the B<Newcomen> project (see
L<Newcomen::Collection>). See the developer section in L<Newcomen::Manual> for a description of how
B<Newcomen> works and how the catalog fits in.

Plugins may use L<Newcomen::Role::Attribute::Catalog> to access the main catalog instance.

=head1 CLASS METHODS

=head2 new

   my $catalog = Newcomen::Catalog -> new ();

Constructor. No parameters, the collection list will be empty initially.

=head1 INSTANCE METHODS

I<Newcomen::Catalog> uses a hashref to store the collections, with the collection IDs as keys, and
the collection instances as values. It uses the L<hash methods|Newcomen::Util::Traits/Hash_Methods>
of L<Newcomen::Util::Traits>, with the following exceptions and additions:

=over

=item *

There is no L<set()|Newcomen::Util::Traits/set> method. L<add()|/add> must be used to add a
collection (see L<below|/add>).

=item *

The I<Newcomen::Catalog> method I<ids()> is an alias for the L<keys()|Newcomen::Util::Traits/keys>
method.

=item *

The I<Newcomen::Catalog> method I<collection()> is an alias for the
L<get()|Newcomen::Util::Traits/get> method.

=item *

The I<Newcomen::Catalog> method I<collections()> is an alias for the
L<values()|Newcomen::Util::Traits/values> method.

=back

Please see L<Newcomen::Util::Traits> for a complete list of methods and more details.

=head2 add

   $catalog -> add ($collection);

This method adds a collection (an L<Newcomen::Collection> instance) to the catalog. The collection
may then be accessed by its collection ID. The ID must be unique, trying to add a collection with an
ID that already exists in the catalog will cause a fatal error.

=head1 SEE ALSO

L<Newcomen::Collection>, L<Newcomen::Manual>, L<Newcomen::Role::Attribute::Catalog>,
L<Newcomen::Util::Traits>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
