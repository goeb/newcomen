# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Writer::Copy;
our $VERSION = 2014052501;

use namespace::autoclean;
use File::Copy;
use File::Path qw( make_path );
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;
use Path::Class ();
use Newcomen::Util::Hash qw( merge );
use Newcomen::Util::Path qw( resolute contains_dir contains_file );

extends 'Newcomen::Writer::Backend';
with    'Newcomen::Role::Attribute::Core';
with    'Newcomen::Role::Attribute::Config';

sub BUILD {
   shift -> _config () -> add_default ({
      'writer'            => {
         'copy'           => {
            'output_dir'  => undef,
            'output_file' => undef,
            'override'    => undef,
            'source_file' => undef,
         },
      }
   });
}

override '_do_write' => sub {

   my $self = shift;
   my $page = shift;
   my $opts = shift;

   my $config = $self -> _config () -> get (['writer', 'copy']);
   $config = merge ($config, $opts -> data ());

   my $root = $self -> _config () -> get (['ROOT_DIR']);
   my $dir  = $config -> {'output_dir' } // $self -> _config -> get (['writer', 'output_dir']);
   my $ovrd = $config -> {'override'   } // $self -> _config -> get (['writer', 'override'  ]);
   my $src  = $config -> {'source_file'} // $page -> get (['source_file']);
   my $tgt  = $config -> {'output_file'} // $page -> target ();

   my $root_dir = resolute (Path::Class::dir ($root));
   my $out_dir  = resolute ($root_dir -> subdir ($dir));
   my $out_file = $out_dir -> file ($tgt);
   my $src_file = Path::Class::file ($src);

   confess 'Invalid root directory'   unless $root and -d $root_dir -> stringify ();
   confess 'Invalid output directory' unless $dir  and contains_dir  ($root_dir, $out_dir );
   confess 'Invalid source file'      unless $src  and contains_file ($root_dir, $src_file);
   confess 'Invalid target file'      unless $tgt  and $out_dir -> subsumes ($out_file);

   if (-e $out_file -> stringify () and not $ovrd) {
      confess 'File ' . $out_file -> stringify () . ' already exists';
   }

   make_path ($out_file -> parent () -> stringify ());
   copy ($src, $out_file -> stringify ()) or confess "Error copying $src: $!";

};

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Writer::Copy - Copies a file.

=head1 DESCRIPTION

This module provides a writer backend that copies a file. It must not be used directly, see
L<Newcomen::Writer> for the writer frontend.

=head1 OPTIONS

   {
      'writer'            => {
         'copy'           => {
            'source_file' => undef,
            'output_file' => undef,
            'output_dir'  => undef,
            'override'    => undef,
         },
      },
   }

These are the default options for this writer backend. The user may override any default setting in
the configuration file as required. However, plugins may decide to supersede the user defined
options. Usually this shouldn't be the case for this writer backend, though.

I<source_file> specifies the absolute path to the file that should be copied. Usually it makes no
sense to set this option in the configuration. If this option is C<undef>, the backend looks for the
key I<source_file> in the page's meta data (see L<Newcomen::Page>) and uses that instead. If both
are undefined (or empty), the backend will I<die()>.

I<output_file> should usually not be set in the configuration, i.e. it should be left with the
default C<undef> value. It may be used by plugins to set the file name of the output file, relative
to the I<output_dir>. If it is C<undef> (the default), the page target will be used as name of the
output file, see L<Newcomen::Page>.

The I<output_dir> option specifies the directory to which the files should be written. It has to be
specified relative to the project's root directory (the I<ROOT_DIR> option, usually set to the
current working directory C<'.'>). The output directory has to exist, it will not be created
automatically. If it does not exist, this backend will I<die()>. Subdirectories of the output
directory will be created automatically if required. If this option is C<undef>, the backend will
try to use the global option I<writer/output_dir>, see L<Newcomen::Writer>.

If the I<override> option is set to a true value, existing files will be overridden. If this option
is set to a false value, and an output file already exists, the backend will I<die()>. If this
option is C<undef>, the backend will try to use the global option I<writer/override>, see
L<Newcomen::Writer>.

=head1 CLASS AND INSTANCE METHODS

See L<Newcomen::Writer::Backend>.

=head1 SEE ALSO

L<Newcomen::Page>, L<Newcomen::Writer>, L<Newcomen::Writer::Backend>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
