# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Util;
our $VERSION = 2014052501;

use strict;
use warnings;
use Data::Dumper ();
use Digest::SHA;
use Exporter ();
use Scalar::Util;

our @ISA       = qw( Exporter );
our @EXPORT_OK = qw( is_a data_sha1 );

sub is_a {

   my $var = shift;
   my $cls = shift;

   defined $var && Scalar::Util::blessed $var && $var -> isa ($cls);

}

sub data_sha1 {

   local $Data::Dumper::Indent   = 0;
   local $Data::Dumper::Pair     = '=>',
   local $Data::Dumper::Purity   = 1;
   local $Data::Dumper::Sortkeys = 1;
   local $Data::Dumper::Terse    = 1;

   return Digest::SHA::sha1_hex (Data::Dumper::Dumper (shift));

}

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Util - Utility functions.

=head1 SYNOPSIS

   use Newcomen::Util;

   if (Newcomen::Util::is_a ($page, 'Newcomen::Page')) {
      # do something...
   }

   my $checksum = Newcomen::Util::data_sha1 ($data);

=head1 DESCRIPTION

This package provides miscellaneous utility functions. Note that all functions may be imported into
the package using I<Newcomen::Util>:

   use Newcomen::Util qw( is_a data_sha1 );

No function is exported by default.

=head1 FUNCTIONS

=head2 is_a

   if (Newcomen::Util::is_a ($page, 'Newcomen::Page')) {
      # do something...
   }

This function will return a true value if the variable supplied as first parameter is an instance of
the class supplied by its full name as the second parameter, a false value otherwise.

=head2 data_sha1

   my $checksum = Newcomen::Util::data_sha1 ($data);

This function expects one parameter, usually a hashref or arrayref. It will serialize the data
structure using L<Data::Dumper>, and calculate the SHA-1 hash of the serialized data. The function
will return the hexadecimal representation of the hash.

B<Note:> Results will only be reliable if the data structure does not contain anything else than
simple scalar values, especially any blessed objects should be avoided!

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
