# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Formatter;
our $VERSION = 2014052501;

use namespace::autoclean;
use Digest::SHA qw( sha1_hex );
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;

with 'Newcomen::Role::Attribute::Core';
with 'Newcomen::Role::Attribute::Config';
with 'Newcomen::Role::Frontend' => {
   'namespace' => 'Newcomen::Formatter',
   'type'      => 'Newcomen::Formatter::Backend',
};

has '_cache'        => (
   'is'             => 'ro',
   'isa'            => 'HashRef[Str]',
   'default'        => sub {{}},
   'init_arg'       => undef,
   'traits'         => ['Hash'],
   'handles'        => {
       '_set_cache' => 'set',
       '_get_cache' => 'get',
       '_cached'    => 'exists',
   },
);

sub BUILD {
   shift -> _config () -> add_default ({
      'formatter'    => {
         'use_cache' => 1,
      },
   });
}

sub format_content {

   my $self     = shift;
   my $content  = shift;
   my $page     = shift;
   my $index    = shift;
   my $cache_id = undef;
   my $text     = $content -> content ();

   if ($self -> _config () -> get (['formatter', 'use_cache']) and $content -> source ()) {
      $cache_id = sha1_hex ($content -> source () -> id ());
   }

   for my $formatter ($content -> formatters () -> formatters ()) {

      my $name = $formatter -> name ();
      my $full = "Newcomen::Formatter::$name";
      my $opts = $formatter -> options ();

      confess "No such formatter: $name" unless $self -> backend ($full);

      my $backend_id = undef;
      if ($cache_id) {
         $backend_id = $self -> backend ($full) -> unique_id (
            $text, $opts, $content, $page, $index
         );
         $cache_id = undef unless defined $backend_id;
      }

      if ($cache_id) {
         $cache_id .= "/$name/$backend_id";
         if ($self -> _cached ($cache_id)) {
            $text = $self -> _get_cache ($cache_id);
         }
         else {
            $text = $self -> backend ($full) -> format ($text, $opts, $content, $page, $index);
            $self -> _set_cache ($cache_id, $text);
         }
      }
      else {
         $text = $self -> backend ($full) -> format ($text, $opts, $content, $page, $index);
      }

   }

   $content -> content ($text);

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Formatter - Formatter frontend.

=head1 SYNOPSIS

   use Newcomen::Formatter;

   my $formatter = Newcomen::Formatter -> new ();

   $formatter -> format_content ($content, $page, $index);

=head1 DESCRIPTION

I<Newcomen::Formatter> is the frontend for the content formatter classes. The actual formatting work
will be done by backends. The formatter backends to use and their options are set on a per-content
basis (see L<Newcomen::Content>).

Plugins may use L<Newcomen::Role::Attribute::Formatter> to access the main formatter instance, but
usually this should not be necessary.

=head1 OPTIONS

   {
      'formatter'    => {
         'use_cache' => 1,
      },
   }

These default options are set and used by the formatter frontend.

The I<use_cache> option may be used to enable or disable the caching of formatter results used by
the frontend. It is enabled by default, set I<use_cache> to a false value to disable it. Please see
L<below|/CACHING> for details.

=head1 CLASS METHODS

=head2 new

   my $formatter = Newcomen::Formatter -> new ();

Constructor. Expects no parameters.

=head1 INSTANCE METHODS

=head2 Formatting

=head3 format_content

   $formatter -> format_content ($content, $page, $index);

Expects an L<Newcomen::Content> instance as first parameter, its "parent" L<Newcomen::Page> instance
as second parameter and the index of the content on the page as third parameter.

If there are formatter backends set for the content, the backends will be called in the specified
order to format it (i.e. the first formatter will format the original content, the second formatter
will format the return value of the first formatter, and so on). Details and backend options may
differ for different backends, see the appropriate documentation for details. Backend names must be
module basenames only (i.e. without the C<'Newcomen::Formatter::'> prefix), and names are case
sensitive. The backend must exist, else this method will I<die()>.

Formatter results may be cached (if enabled), see L<below|/CACHING> for details.

=head2 Backends

The methods L<backend()|Newcomen::Role::Frontend/backend>,
L<backend_exists()|Newcomen::Role::Frontend/backend_exists> and
L<backends()|Newcomen::Role::Frontend/backends> are available to access the formatter backends,
though usually this should not be necessary. For a description of these methods please see
L<Newcomen::Role::Frontend>.

=head1 CACHING

To avoid applying a formatter to the same content multiple times (if the content is included on
multiple pages), this frontend caches the formatter results, if enabled (see L<OPTIONS|/OPTIONS>).

The cache uses the content's source ID to identify the content itself, and backend names and a
backend specific ID to identify backends. This means that the L<Newcomen::Content> instances must
have an associated L<Newcomen::Source> instance, and this source must have a (globally) unique ID.
Caching will not be applied to content instances without sources!

A formatter backend may choose to disable caching. Since multiple formatter backends can be applied
to the content, the cache will store the results of every step in the formatter chain per content
(or rather source) item. As soon as a backend disables caching, any formatting applied after that
will not be cached, too. Additionally, the unique ID used to identify the backend usually depends on
the backend options. Results from the same backend called with different options will usually cause
a new cache entry (this also depends on the backend itself). A backend may also choose to return
different results based on meta data, page target, etc. This, too, will result in a new ID and thus
a new entry in the cache.

This means that the order in which the formatters are applied is important for an effective caching!
See the formatter backends for details.

It also means that caching only works if the content is changed only by formatter backends! It must
not be changed by plugins! If a plugin is used that changes the content, caching must be disabled,
else the generated output may be invalid.

=head1 MISCELLANEOUS

Backends must use L<Newcomen::Formatter::Backend> as their base class. They must be placed in the
I<Newcomen::Formatter> namespace, and implement a
L<_do_format()|Newcomen::Formatter::Backend/do_format> method. For more details, see
L<Newcomen::Formatter::Backend>.

=head1 SEE ALSO

L<Newcomen::Content>, L<Newcomen::Formatter::Backend>, L<Newcomen::Page>,
L<Newcomen::Role::Attribute::Formatter>, L<Newcomen::Role::Frontend>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
