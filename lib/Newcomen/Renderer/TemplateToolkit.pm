# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Renderer::TemplateToolkit;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;
use Template;
use Newcomen::Util::Hash;
use Newcomen::Util;

extends 'Newcomen::Renderer::Backend';
with    'Newcomen::Role::Attribute::Core';
with    'Newcomen::Role::Attribute::Config';

sub BUILD {
   shift -> _config () -> add_default ({
      'renderer'           => {
         'templatetoolkit' => {
            'template'     => undef,
            'use_cache'    => 1,
         },
      }
   });
}

has '_cache'        => (
   'is'             => 'ro',
   'isa'            => 'HashRef[Template]',
   'default'        => sub {{}},
   'init_arg'       => undef,
   'traits'         => ['Hash'],
   'handles'        => {
       '_set_cache' => 'set',
       '_get_cache' => 'get',
       '_cached'    => 'exists',
   },
);

override '_do_render' => sub {

   my $self = shift;
   my $page = shift;
   my $opts = shift;
   my $out  = undef;
   my $conf = $self -> _config () -> get (['renderer', 'templatetoolkit']);

   $conf = Newcomen::Util::Hash::merge ($conf, $opts -> data ());
   my $tmpl  = (delete $conf -> {'template' }) // 'default.tt';
   my $cache = (delete $conf -> {'use_cache'});

   my $t = undef;
   if ($cache) {
      my $key = Newcomen::Util::data_sha1 ($conf);
      if ($self -> _cached ($key)) {
         $t = $self -> _get_cache ($key);
      }
      else {
         $t = Template -> new ($conf) or confess 'Template Toolkit error: ' . Template -> error ();
         $self -> _set_cache ($key, $t);
      }
   }
   else {
      $t = Template -> new ($conf) or confess 'Template Toolkit error: ' . Template -> error ();
   }

   my $vars = {

      'core'    => $self -> _core (),
      'catalog' => $self -> _core () -> catalog (),
      'config'  => $self -> _core () -> config  (),
      'crawler' => $self -> _core () -> crawler (),
      'plugins' => $self -> _core () -> plugins (),
      'site'    => $self -> _core () -> site    (),
      'url'     => $self -> _core () -> url     (),
      'page'    => $page,

      'c'       => $self -> _core () -> config  (),
      'u'       => $self -> _core () -> url     (),
      'p'       => $page,

   };

   $t -> process ($tmpl, $vars, \$out) or confess 'Template Toolkit error: ' . $t -> error ();

   return $out;

};

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Renderer::TemplateToolkit - Template Toolkit renderer backend.

=head1 DESCRIPTION

Renderer backends are usually not called directly. See L<Newcomen::Renderer> for the frontend
documentation.

This module provides a renderer backend using L<Template Toolkit|Template>.

=head1 OPTIONS

   {
      'renderer'           => {
         'templatetoolkit' => {
            'template'     => undef,
            'use_cache'    => 1,
         },
      },
   }

These are the default options for this renderer backend. The user may override any default setting
in the configuration file as required. However, plugins may decide to supersede the user defined
options.

The I<renderer/templatetoolkit> hashref may contain any L<Template Toolkit|Template> options as
required, with the following addition: The options I<renderer/templatetoolkit/template> and
I<renderer/templatetoolkit/use_cache> are not L<Template Toolkit|Template> options, but processed by
this backend directly.

The I<template> specifies the template file to be used for rendering. It will be passed to
L<Template Toolkit|Template>'s I<process()> method. If it is not defined in the configuration it
defaults to C<'default.tt'>.

The I<use_cache> option indicates whether to cache the template objects or not. It defaults to C<1>,
meaning caching is enabled, and may be set to any false value to disable it. Note that template
objects will be cached based on the options set (these may differ for different pages, see the
individual plugins that deal with these). Different options will cause different entries in the
cache. Note that this is based on the actual contents of the options hashref, so - for example -
including a default setting in one hashref and omitting it from another will cause two template
objects to be created, even though their behaviour is exactly the same. Also note that this caching
is different from L<Template Toolkit|Template>'s own caching, which will save compiled templates on
disk.

All other options will be used as configuration for L<Template Toolkit|Template> itself.
Technically, the complete I<renderer/templatetoolkit> hashref will be passed to
L<Template Toolkit|Template>'s constructor, after the I<template> and I<use_cache> keys have been
deleted from it.

B<Note>: At least the L<Template Toolkit|Template> option I<INCLUDE_PATH> should be set, so the
templates can be found. See L<Template Toolkit|Template>'s documentation for details.

=head1 TEMPLATE VARIABLES

The following variables will be available in the template:

=over

=item I<catalog>

The main L<Newcomen::Catalog> instance.

=item I<config> (also available as I<c>)

The main L<Newcomen::Config> instance.

=item I<core>

The main L<Newcomen::Core> instance.

=item I<crawler>

The main L<Newcomen::Crawler> instance.

=item I<page> (also available as I<p>)

The L<Newcomen::Page> instance that should be rendered.

=item I<plugins>

The main L<Newcomen::Plugins> instance.

=item I<site>

The main L<Newcomen::Site> instance.

=item I<url> (also available as I<u>)

The main L<Newcomen::URL> instance.

=back

=head1 CLASS AND INSTANCE METHODS

See L<Newcomen::Renderer::Backend>.

=head1 MISCELLANEOUS

The I<render()> method of this backend will I<die()> if there is an error processing the template.

=head1 SEE ALSO

L<Template>, L<Newcomen::Catalog>, L<Newcomen::Config>, L<Newcomen::Core>, L<Newcomen::Crawler>,
L<Newcomen::Page>, L<Newcomen::Plugins>, L<Newcomen::Renderer>, L<Newcomen::Renderer::Backend>,
L<Newcomen::Site>, L<Newcomen::URL>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
