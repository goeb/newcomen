# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Renderer::Backend;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;
use Newcomen::Data;
use Newcomen::Util qw( is_a );

sub render {

   my $self = shift;
   my $page = shift;
   my $opts = shift // Newcomen::Data -> new ();

   confess 'Usage: render($page,$options)' unless
      is_a ($page, 'Newcomen::Page') and is_a ($opts, 'Newcomen::Data') and not @_;

   my $text = $self -> _do_render ($page, $opts);
   confess 'No string returned by renderer backend' unless defined $text and ref $text eq '';

   return $text;

}

sub _do_render { confess '_do_render() not implemented by backend' }

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Renderer::Backend - Base class for renderer backends.

=head1 SYNOPSIS

   package Newcomen::Renderer::MyBackend;

   use namespace::autoclean;
   use Moose;

   extends 'Newcomen::Renderer::Backend';

   override '_do_render' => sub {

      my $self    = shift;
      my $page    = shift;
      my $options = shift;

      # Render page...

      return $rendered;

   };

   __PACKAGE__ -> meta () -> make_immutable ();

   1;

=head1 DESCRIPTION

This is the base class to be extended by renderer backends. It does not define any attributes. It
does define the methods L<render()|/render> and L<_do_render()|/_do_render>. L<render()|/render>
will call the L<_do_render()|/_do_render> method, which has to be overridden by the actual backend
implementation (see L<SYNOPSIS|/SYNOPSIS>). The L<_do_render()|/_do_render> method of the base class
will simply I<die()>!

=head1 CLASS METHODS

=head2 new

   my $backend = Newcomen::Renderer::MyBackend -> new ();

Constructor, expects no parameters. I<new()> will be called once for every (!) backend found. A
backend does not have to be actually used to be instantiated! If it is actually used,
L<render()|/render> will be called. The one backend instance will be used to render all pages.

=head1 INSTANCE METHODS

=head2 render

   $rendered = $backend -> render ($page, $backend_options);

The I<render()> method of a backend will be called when a page should be rendered. It will return
the rendered content. The page to be rendered (an L<Newcomen::Page> instance) must be passed as
first parameter. The backend options must be passed as second parameter. This must be an
L<Newcomen::Data> instance or C<undef>. The I<render()> method must not be overridden by a backend
implementation!

=head1 MISCELLANEOUS

=head2 _do_render

The actual backends must implement a I<_do_render()> method, I<_do_render()> of this base class will
I<die()>. The page to be rendered (an L<Newcomen::Page> instance) will be the first parameter of
this method, the L<Newcomen::Data> instance containing the options (or a newly created empty one, if
no options were supplied) will be the second parameter. I<_do_render()> has to return a string
containing the rendered content.

=head1 SEE ALSO

L<Newcomen::Data>, L<Newcomen::Page>, L<Newcomen::Renderer>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
