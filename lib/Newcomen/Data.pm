# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Data;
our $VERSION = 2014101201;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::Aliases;
use MooseX::NonMoose;
use MooseX::StrictConstructor;
use Newcomen::Util qw( is_a );
use Newcomen::Util::Hash;

extends 'Data::SimplePath';

alias 'delete' => 'remove';
alias 'exists' => 'does_exist';

around [qw( get data )] => sub {

   my $orig = shift;
   my $self = shift;

   return scalar $self -> $orig (@_);

};

around 'key' => sub {

   my $orig = shift;
   my $self = shift;

   return $self -> normalize_key ($self -> $orig (@_));

};

around 'set' => sub {

   my $orig = shift;
   my $self = shift;

   confess 'Could not set the data.' unless $self -> $orig (@_);

   return 1;

};

around 'separator' => sub {

   my $orig = shift;
   my $self = shift;

   confess ('Changing the separator is not allowed') if @_;

   return $self -> $orig ();

};

around 'auto_array' => sub {

   my $orig = shift;
   my $self = shift;

   confess ('Enabling the auto_array setting is not allowed') if @_ and $_ [0];

   return $self -> $orig (@_);

};

sub merge {

   my $self = shift;
   my $data = $self -> data ();

   while (@_) {
      my $hash = is_a ($_ [0], 'Newcomen::Data') ? shift -> data () : shift;
      $data = Newcomen::Util::Hash::merge ($data, $hash);
   }
   $self -> set ('', $data);

   return $self;

}

override 'clone' => sub { Newcomen::Data -> new () -> merge (shift -> data ()) };

sub FOREIGNBUILDARGS { {} }

sub BUILD {

   my $self = shift;

   $self -> auto_array (0);

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Data - General data storage class, Data::SimplePath wrapper.

=head1 SYNOPSIS

   use Newcomen::Data;

   my $data = Newcomen::Data -> new ();

   # Set a value:
   $data -> set (['some', 'data'], 'value');

   # Get the value:
   my $value = $data -> get (['some', 'data']);

=head1 DESCRIPTION

Please read (and understand) the documentation of L<Data::SimplePath> first. I<Newcomen::Data>
extends L<Data::SimplePath>. I<Newcomen::Data> is a L<Moose> class. All methods provided by
L<Data::SimplePath> are available for an I<Newcomen::Data> instance, and most of these behave
exactly like or at least similar their original counterparts.

=head2 Differences To Data::SimplePath

The constructor of I<Newcomen::Data> does not allow the setting of initial content. Instead, the
initial data will always be an empty hashref. Setting the root element to an arrayref does work, but
it will break the L<merge()|/merge> method, and any code using I<Newcomen::Data> that expects a
hashref, so only do this if you really know what you are doing (usually this means: don't do it).

The I<get()> and L<data()|/data> methods will always return a scalar value, i.e. calling them in
list context will not work as it does for L<Data::SimplePath>. This allows for easier usage of
I<Newcomen::Data> instances within L<Template Toolkit|Template> templates.

The I<set()> method will I<die()> on failure.

The I<separator()> and I<auto_array()> methods do not allow their respective settings to be changed.
They can be used to get the values, though. The value of the I<separator> is C<'/'> (as is the
default for L<Data::SimplePath>). The value of I<auto_array> is C<0> (disabled), this is different
from the initial L<Data::SimplePath> value. This means that for an I<Newcomen::Data> instance you
will have to create arrays explicitly. B<Note:> Trying to change any of these settings will cause
the methods to I<die()>.

The I<key()> method works like L<Data::SimplePath>'s L<method|Data::SimplePath/key>, but it will
always return the normalized key.

I<Newcomen::Data>'s L<clone()|/clone> will return a new I<Newcomen::Data> instance. It will use
L<merge()|/merge> internally, which itself uses L<Clone> to clone the data, instead of L<Storable>
used by L<Data::SimplePath>'s L<clone()|Data::SimplePath/clone> method. This means that coderefs or
regexps inside the data should work.

The methods I<delete()> and I<exists()> are aliases for the L<remove()|Data::SimplePath/remove> and
L<does_exist()|Data::SimplePath/does_exist> methods of L<Data::SimplePath> (both names may be used
for any of these two methods).

=head1 CLASS METHODS

=head2 new

   my $data = Newcomen::Data -> new ();

Constructor. The initial data will be an empty hashref.

=head1 INSTANCE METHODS

=head2 auto_array, replace_leaf, separator

   # Set new value (works only for replace_leaf):
   $data -> replace_leaf (1);

   # Get the current separator setting:
   $separator = $data -> separator ();

These get or set the L<Data::SimplePath> options, see there for details. Also check the differences
between I<Newcomen::Data> and L<Data::SimplePath> mentioned above.

=head2 delete, remove

   $data -> delete (['some', 'data']);

Remove an element. I<delete()> is an alias for L<Data::SimplePath>'s
L<remove()|Data::SimplePath/remove> method. Both may be used with I<Newcomen::Data>.

=head2 exists, does_exist

   if ($data -> exists (['some', 'data'])) {
      # do something...
   }

Check if an element exists. I<exists()> is an alias for L<Data::SimplePath>'s
L<does_exist()|Data::SimplePath/does_exist> method. Both may be used with I<Newcomen::Data>.

=head2 get, set

   my $value = $data -> get (['some', 'data']);

   $data -> set (['some', 'data'], $value);

Get or set an element. See L<Data::SimplePath> for details.

=head2 data

   my $hashref = $data -> data ();

Returns the underlying data structure, same as L<Data::SimplePath>'s L<data()|Data::SimplePath/data>
method.

=head2 clone

   my $copy = $data -> clone ();

Returns a new I<Newcomen::Data> instance with the same contents, but independent from the source
instance.

=head2 merge

   $data -> merge ($other_data);

Expects one or more hashrefs or I<Newcomen::Data> instances as parameters. The hashrefs (or
instances) will be merged with the current data of the instance, one after another in the order
specified. See L<Newcomen::Util::Hash>'s L<merge()|Newcomen::Util::Hash/merge> function for details.
For convenience, this method will return the current instance, so the following may be used to
create an instance with initial data:

   my $data = Newcomen::Data -> new () -> merge ($initial_data);

Also, it allows calls to I<merge()> to be chained, but the same can be achieved by using multiple
parameters.

=head2 key, path, normalize_key

   my $key  = $data -> key ($path);
   my $path = $data -> path ($key);
   my $norm = $data -> normalize_key ($key);

See L<Data::SimplePath> for details. Note that I<Newcomen::Data>'s I<key()> function will always
return the normalized key.

=head1 SEE ALSO

L<Data::SimplePath>, L<Newcomen::Util::Hash>

=head1 VERSION

This is version C<2014101201>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
