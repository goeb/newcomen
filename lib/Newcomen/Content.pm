# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Content;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::Aliases;
use MooseX::StrictConstructor;
use Newcomen::Data;
use Newcomen::Formatter::Specification::List;

has 'source'     => (
   'is'          => 'ro',
   'isa'         => 'Maybe[Newcomen::Source]',
   'default'     => undef,
);

has 'content'    => (
   'is'          => 'rw',
   'isa'         => 'Maybe[Str]',
   'init_arg'    => undef,
   'builder'     => '_build_content',
   'lazy'        => 1,
);

has '_cnt_meta'  => (
   'is'          => 'ro',
   'isa'         => 'Newcomen::Data',
   'init_arg'    => undef,
   'default'     => sub { Newcomen::Data -> new () },
   'handles'     => [qw( delete set )],
);

has 'meta'       => (
   'is'          => 'ro',
   'isa'         => 'Newcomen::Data',
   'init_arg'    => undef,
   'writer'      => '_set_meta',
   'default'     => sub { Newcomen::Data -> new () },
   'handles'     => [qw( exists get )],
);

has 'formatters' => (
   'is'          => 'ro',
   'isa'         => 'Newcomen::Formatter::Specification::List',
   'init_arg'    => undef,
   'default'     => sub { Newcomen::Formatter::Specification::List -> new () },
);

alias 'content_meta' => '_cnt_meta';

sub _merge_meta {
   my $self = shift;
   $self -> _set_meta (
      Newcomen::Data -> new () -> merge (
         $self -> source () ? $self -> source () -> meta () : {},
         $self -> _cnt_meta ()
      )
   );
}

before [qw( exists get meta         )] => sub { shift -> _merge_meta (); };
after  [qw( delete set content_meta )] => sub { shift -> _merge_meta (); };

sub BUILD { shift -> _merge_meta (); }

sub _build_content {

   my $self = shift;

   $self -> source ?  $self -> source () -> content () : undef;

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Content - Represents a piece of content on a page.

=head1 SYNOPSIS

   use Newcomen::Content;

   my $content = Newcomen::Content -> new ('source' => $source);

   # Set the actual content (text):
   $content -> content ($text);

   # Set some meta data:
   $content -> set (['some', 'data'], 'value');

   # Access the associated source item:
   my $source = $content -> source ();

=head1 DESCRIPTION

An I<Newcomen::Content> instance is basically a container for an L<Newcomen::Source> instance, and
related meta data. It is used to keep the source item's content separated from its formatted
content. Any source item may be referenced by an arbitrary number of content items (and thus
different formatters may be applied to the content), and every content item references exactly zero
or one source item. The I<Newcomen::Content> instances also keep a list of formatter specifications
(see L<Newcomen::Formatter::Specification::List>), and the content of the instance will be formatted
according to the formatter specifications in this list (if the content item is added to a page, see
L<Newcomen::Page>).

Content items are used by the pages (see L<Newcomen::Page>), and every item usually represents a
piece of content on a page (created from the associated source item). See the developer section in
L<Newcomen::Manual> for a description of how B<Newcomen> works and how these content items fit in.

B<Important:> The handling of meta data of an I<Newcomen::Content> instance differs from most other
classes. Please see the L<Meta Data|/Meta Data> section below for details.

=head1 CLASS METHODS

=head2 new

   my $content = Newcomen::Content -> new ('source' => $source);

Constructor. The I<source> parameter is optional. If specified, it must either be the
L<Newcomen::Source> instance that should be associated with the new content item, or C<undef> (which
is basically the same as not specifying a I<source> parameter). The source item can not be changed
later on.

=head1 INSTANCE METHODS

=head2 General

=head3 content

   # Get the current content:
   my $text = $content -> content ();

   # Set the content:
   $content -> content ($text);

Get and set the content. As a setter, the first (and only) parameter must be a string, or C<undef>.
The initial value is the value of the source item's content (if one is set, else C<undef>), it will
be set on first access (not during construction). Note that the setter's parameter may be any
string, i.e. it may also be a binary format if this is required for some reason.

=head3 source

   my $source = $content -> source ();

Returns the L<Newcomen::Source> instance associated with the I<Newcomen::Content> instance (if any).
The source can only be set by the constructor (see L<new()|/new>). If no source item is set, this
method will return C<undef>.

=head3 formatters

   my $formatters = $content -> formatters ();

   # To add formatter specifications:
   $content -> formatters () -> add ([$spec_1, $spec_2]);

Returns the L<Newcomen::Formatter::Specification::List> instance associated with the content item.
The list will be empty initially.

=head2 Meta Data

=head3 get, set, exists, delete

   # Set meta data:
   $content -> set (['some', 'data'], 'value');

   # Retrieve and delete it, if it exists:
   my $value = 'some default value';
   if ($content -> exists (['some', 'data'])) {
      $value = $content -> get (['some', 'data']);
      $content -> delete (['some', 'data']);
   }

Meta data of an I<Newcomen::Content> instance consists of two parts: The meta data of the source
item referenced by the content (if any), and the content item's own meta data.

The methods I<exists()> and I<get()> will operate on the content's meta data if it exists for a
given key. If it does not exist, they will operate on the source item's meta data for that key.
Note that the current implementation uses the merged data (see below), which will always be cloned.
So any references will not point to the original data!

The methods I<delete()> and I<set()> on the other hand will only operate on the content's meta data.

=over

The methods I<get()>, I<set()>, I<delete()> and I<exists()> are mapped to the L<Newcomen::Data>
methods of the same name. Please see there for details on the parameters.

=back

=head3 meta

   my $merged = $content -> meta ();

The L<Newcomen::Data> instance returned by the I<meta()> method contains both items' meta data
merged (with content meta data overriding source meta data if necessary) at the time of the call.
Merging will be done before or after (as appropriate) every call to I<get()>, I<set()>, I<exists()>,
I<delete()>, I<meta()> or I<content_meta()>. Once retrieved, the instance it will not be updated (a
new instance is created on every merge)! Any changes to the L<Newcomen::Data> instance returned by
I<meta()> will be lost on the next call to one of the aforementioned methods. It is recommended to
only use I<meta()> to read the meta data (mostly used in the templates). The I<meta()> method exists
only to provide an interface consistent with the other classes.

=head3 content_meta

   my $content_meta = $content -> content_meta ();

The L<Newcomen::Data> instance returned by I<content_meta()> contains the content item's own meta
data, without any source item meta data. If access to content meta data only is required, without
falling back to the source meta data, this method may be used to access the L<Newcomen::Data>
instance. After every call to I<content_meta()> meta data merging occurs, so changes in the
content's meta data will be visible when using L<meta()|/meta>.

=head1 SEE ALSO

L<Newcomen::Data>, L<Newcomen::Formatter::Specification::List>, L<Newcomen::Manual>,
L<Newcomen::Page>, L<Newcomen::Source>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
