# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

package Newcomen::Config;
our $VERSION = 2014052501;

use namespace::autoclean;
use Moose '-meta_name' => '_moose_meta';
use MooseX::StrictConstructor;
use Newcomen::Data;

with 'Newcomen::Role::Frontend' => {
   'namespace' => 'Newcomen::Config',
   'type'      => 'Newcomen::Config::Backend',
};

for (qw( default user )) {
   has "_$_"     => (
      'is'       => 'ro',
      'isa'      => 'Newcomen::Data',
      'init_arg' => undef,
      'default'  => sub { Newcomen::Data -> new () },
      'handles'  => { "_merge_$_" => 'merge' },
   );
}

has '_config'    => (
   'is'          => 'rw',
   'isa'         => 'Newcomen::Data',
   'init_arg'    => undef,
   'default'     => sub { Newcomen::Data -> new () },
   'handles'     => [qw( exists get key path )],
);

after [qw( _merge_default _merge_user )] => sub {

   my $self = shift;

   $self -> _config (Newcomen::Data -> new () -> merge ($self -> _default (), $self -> _user ()));

};

sub add_default {

   my $self = shift;
   my $conf = shift;

   $self -> _merge_default ($conf);

}

sub add_config {

   my $self    = shift;
   my $name    = shift;
   my $opts    = shift // Newcomen::Data -> new ();
   my $backend = undef;

   $opts = Newcomen::Data -> new () -> merge ($opts) if ref $opts eq 'HASH';

   for ($self -> backends ()) {
      if (lc "Newcomen::Config::$name" eq lc $_) {
         $backend = $self -> backend ($_);
         last;
      }
   }

   confess "Configuration backend $name not found" unless $backend;

   $self -> _merge_user ($backend -> config ($opts));

}

__PACKAGE__ -> _moose_meta () -> make_immutable ();

1;

__END__

####################################################################################################

=head1 NAME

Newcomen::Config - Configuration frontend.

=head1 SYNOPSIS

   use Newcomen::Config;

   my $config = Newcomen::Config -> new ();

   # Load user configuration:
   $config -> add_config ($backend_name, $backend_options);

   # A plugin may add default values:
   $config -> add_default ($default_options);

   # Access options:
   my $value = 'some default';
   if ($config -> exists (['some', 'option'])) {
      $value = $config -> get (['some', 'option']);
   }

=head1 DESCRIPTION

I<Newcomen::Config> manages B<Newcomen>'s configuration.

User configuration settings are loaded by configuration backends, currently the only one available
is L<Newcomen::Config::Perl>. Plugins may add default values for their configuration options. User
and default values are kept separate, adding default values after the user configuration has been
loaded will not override the user defined values. That means that calls to L<get()|/get> or
L<exists()|/exists> will always return the expected value.

Access to the configuration options works like access to data stored in an L<Newcomen::Data>
instance. The methods L<get()|/get> and L<exists()|/exists> are equivalent to the L<Newcomen::Data>
methods of the same name. So are the methods L<path()|/path> and L<key()|/key>, included for
convenience should they be required. Please see L<Newcomen::Data> for details on how to access
individual elements/options.

Plugins may use L<Newcomen::Role::Attribute::Config> to access the main configuration instance.

=head1 CLASS METHODS

=head2 new

   my $config = Newcomen::Config -> new ();

Constructor. Expects no parameters. The initial configuration will be empty.

=head1 INSTANCE METHODS

=head2 Setting Configuration Values

Note that there is no method to set a single option to a specific value, i.e. there is no equivalent
to L<Newcomen::Data>'s I<set()> method. The only way to add stuff to the configuration is to use one
of the two methods, L<add_config()|/add_config> or L<add_default()|/add_default>, described below.

Using one of these two methods will merge the new values with the already existing ones. No option
already set will ever be deleted, but existing options may be overridden if they are defined again
in a configuration loaded later on.

=head3 add_config

   $config -> add_config ($backend_name, $backend_options);

Uses the specified backend to load user configuration. The first parameter must be the basename of
the backend module, i.e. the module name without the C<'Newcomen::Config::'> prefix. The name is
B<not> case sensitive. If the specified backend does not exist, this method will I<die()>. The
second parameter is optional. If it is specified, it must be a hashref or an L<Newcomen::Data>
instance containing the options to be passed to the backend (the hashref, or L<Newcomen::Data>
instance, may be empty). Different backends may use different options, see the individual backends
for details.

=over

B<Note:> The backend's name matching is not case sensitive, as mentioned above. Matching is
performed using Perl's built-in I<lc()> function, which may not work with UTF-8 module names. Using
ASCII only is recommended.

=back

=head3 add_default

   $config -> add_default ($default_options);

This method may be used by plugins to add default values for their configuration options. The method
must be called with one parameter, which must be a hashref containing the default options/values to
be set.

=head2 Accessing Configuration Settings

=head3 exists

   my $exists = $config -> exists ($path);

Checks if the specified configuration option exists. The one (required) parameter has to be an
arrayref or a string specifying the path to the configuration option. See L<Newcomen::Data> for more
details on this parameter's format.

=head3 get

   my $value = $config -> get ($path);

Returns the value of the specified configuration option. This will be C<undef> if the option does
not exist, use L<exists()|/exists> if you need to know. If the user configuration includes a setting
for the option, this value will be returned. If the default values include this option, and no user
setting overrides it, the default will be returned. The required parameter specifies the requested
configuration option. See L<Newcomen::Data> for a description of the possible values.

=head2 Backends

The following methods allow access to the configuration backends, though usually this should not be
necessary.

=head3 backend

   my $backend = $config -> backend ($full_name);

Returns a configuration backend instance. The parameter must be the full module name, including the
C<'Newcomen::Config::'> prefix. This name is case sensitive.

=head3 backend_exists

   my $exists = $config -> backend_exists ($full_name).

Returns a true value if the backend (specified by its full module name as described above) exists.

=head3 backends

   my @backends = $config -> backends ();

Returns a list containing the full module names of all available configuration backends.

=head2 Miscellaneous Methods

=head3 key

   my $key = $config -> key ($path);

See L<Newcomen::Data>'s I<key()> method.

=head3 path

   my $path = $config -> path ($key);

See L<Newcomen::Data>'s I<path()> method.

=head1 MISCELLANEOUS

Backends must use L<Newcomen::Config::Backend> as their base class. They must be placed in the
I<Newcomen::Config> namespace, and implement a L<_do_config()|Newcomen::Config::Backend/do_config>
method. See L<Newcomen::Config::Backend> for more details.

=head1 SEE ALSO

L<Newcomen::Config::Backend>, L<Newcomen::Config::Perl>, L<Newcomen::Data>,
L<Newcomen::Role::Attribute::Config>, L<Newcomen::Role::Frontend>

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
