# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

=pod

=head1 NAME

Newcomen::Manual::Configuration - Configuration information.

=head1 CONFIGURATION INFORMATION

=over

B<Note:> User configuration itself is processed by a configuration backend, this backend also
defines the format of the configuration file (if any). In the documentation, the configuration
options will always be specified as Perl data structures. For different configuration formats this
has to be adapted as appropriate.

=back

Most of the configuration options are defined and processed by plugins. However, B<Newcomen> must
know which plugins to load, and thus the following configuration options are defined by the core
modules:

   {
      'plugins' => {},
   }

The I<plugins> option must be a hashref. Its keys must be plugin basenames, i.e. the plugins' module
names without the C<'Newcomen::Plugin::'> prefix. The values in the hashref specify if the plugin
should be loaded (any true value) or not (any false value). By default, no plugin will be loaded
(and an error message will be printed in this case), so any plugin that should be loaded requires an
entry in this I<plugins> configuration hashref.

Note that - unlike plugins - formatter, renderer and writer backends will always be loaded and these
can not be disabled!

For an overview of all configuration options defined by plugins (and backends), please see
L<Newcomen::Manual::Summary::Config>, and of course the individual plugin and backend modules.

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
