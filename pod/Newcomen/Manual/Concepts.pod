# Copyright 2013-2014 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

=pod

=head1 NAME

Newcomen::Manual::Concepts - General concepts.

=head1 HOW NEWCOMEN PROCESSES A PROJECT

B<Newcomen> is a static content generator. Its input as well as its output may basically be
anything. However, examples in this document assume that both input and output are regular files.

The process of generating the output from the input is described below. Plugins should implement the
appropriate hooks (see L<Newcomen::Manual::Hooks> for details) for the different processing stages
as required. For details on the individual modules and classes involved please follow the links
provided in this documentation.

=head2 0. Configuration And Initialization

Usually the first thing to do before the actual processing starts is to read the configuration of an
B<Newcomen> project. Please see L<Newcomen>, L<Newcomen::Config>, and any configuration backends.
After that, the processing of the project starts with any initialization tasks plugins may require.

=head2 1. Gathering The Input Data

Any piece of input data, for example individual source files, should be represented by an
L<Newcomen::Source> instance. It is up to plugins to create these source items as required. There
are some modules that may be used to help with this task, most notably the L<Moose> role
L<Newcomen::Role::Crawler::Filesystem>, which may be used to create a list of source items from
files and/or directories.

Source items should usually be added to the global L<Newcomen::Crawler> instance provided by the
core (see L<Newcomen::Core>). This is basically a container for any number of source items, and any
item added to this crawler can easily be accessed by other plugins and/or backends.

After all source items have been created a number of filter hooks will be run, allowing the source
items to be modified as well as the removal of any unwanted source items from the crawler's list.

=head2 2. Sorting The Input Data

The L<Newcomen::Source> instances created in step one may then be sorted by plugins in different
collections (L<Newcomen::Collection> instances). Collections allow source instances to be grouped
together according to arbitrary criteria as required. Any source item may be added to any number of
collections. Collections should be added to the catalog (an L<Newcomen::Catalog> instance) provided
by the core (see L<Newcomen::Core>), to allow them to be easily accessed by any plugin.

After the initial creation of the collections, filter hooks will be run for the sources of any
collection, allowing source items to be removed again from the collections if required.

Then filter hooks are run for the collections themselves, to modify a collection or remove it from
the catalog if required.

=head2 3. Creating The Output Pages

The next step is to create pages (L<Newcomen::Page> instances).

A page represents an individual (but complete) piece of output, for example a single output file.
One or more pages are usually created from one collection. The pages themselves may reference the
L<Newcomen::Source> items, but not directly.

Another layer is added: the L<Newcomen::Source> instances are wrapped in L<Newcomen::Content>
instances. Any source item may be referenced by any number of content instances on any number of
pages. The content instances decouple the formatted (see below) source data from the raw input data,
and thus allow the data of one input item to be formatted differently on different pages, as
required. Note that it is also possible for a content item to not reference a source item at all.

Pages should usually be added to the global L<Newcomen::Site> instance as provided by the core (see
L<Newcomen::Core>), to allow easy access from all plugins.

Again, a number of filter hooks will be run after page creation to allow modification and/or
deletion of individual pages as required.

=head2 4. Preparing The Final Output

Before the pages are written, the content items will be formatted (a hook is run to determine which
formatters to apply to any of the content instances on the pages) and after that the pages will be
rendered (another hook is run to determine which renderer backend to be used for any page).

=head2 5. Writing The Final Output

Once the formatters have been applied to the L<Newcomen::Content> instances on all the pages and the
pages themselves have been rendered, another hook is run to determine which writer backend to use
for any page. The writer backend will then be used to store the (rendered) page, for example by
writing it to a file.

=head2 6. Clean Up

Finally, a number of hooks are provided for clean up tasks, if plugins should require it.

=head1 VERSION

This is version C<2014052501>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
