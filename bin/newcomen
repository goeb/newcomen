#!/usr/bin/env perl

# Copyright 2013-2014, 2019 Stefan Goebel.
#
# This file is part of Newcomen.
#
# Newcomen is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Newcomen. If not, see
# <http://www.gnu.org/licenses/>.

use strict;
use warnings;
use lib 'lib';

use File::ChangeNotify;
use Newcomen;

my $VERSION = 2019022601;

my $monitor = ($ARGV [0] and $ARGV [0] eq '-m') ? 1 : 0;
my $runs    = 0;

while (1) {

   if ($monitor and $runs) {
      File::ChangeNotify -> instantiate_watcher ('directories' => ['.']) -> wait_for_events ();
   }

   ++ $runs;

   my $status = sprintf 'Processing #%04d... ', $runs;
   print STDERR $status if $monitor;

   my $newcomen = Newcomen -> new ();

   # If you want to change the default path, feel free to do so. Both absolute and relative paths
   # are allowed, but you should use a relative path if possible.

   $newcomen -> add_config ('perl', {'file' => './config/config.pl'});
   $newcomen -> run        ();

   print STDERR "done\n" if $monitor;

   last unless $monitor;

}

__END__

####################################################################################################

=head1 NAME

newcomen - Main script of the Newcomen static page generator.

=head1 SYNOPSIS

   $ newcomen [options]

=head1 DESCRIPTION

This is the main (executable) script of B<Newcomen>. It should be run from the project's root
directory. Currently it only initializes the configuration from the file F<config/config.pl>
(relative to the current working directory) and then it starts the processing of the project.

See below for supported command line options.

Note: The directory F<lib> is added to Perl's library path to support project-specific plugins. Do
not place any files from untrusted sources in this directory!

=head1 OPTIONS

=head2 -m

   $ newcomen -m

If the option I<-m> is specified, after the first processing of the project, the script will monitor
the current working directory (and all its subdirectories, recursively) for any changes (creation,
deletion and modification of files and directories). As soon as a change is detected, the project is
processed again (this includes initializing the configuration again) and the loop starts all over.

Some status output will be printed to C<STDERR>.

B<Notes:> Renaming a file may sometimes not trigger the build process. The I<-m> option has to be
the first command line option, else it will not be recognized. Any other command line option will be
ignored.

=head1 VERSION

This is version C<2019022601>.

=head1 AUTHOR

Stefan Goebel - newcomen {at} subtype {dot} de

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2014, 2019 Stefan Goebel.

This file is part of Newcomen.

Newcomen is free software: you can redistribute it and/or modify it under the terms of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> as published by the
L<Free Software Foundation|http://www.fsf.org/>, either version 3 of the license, or (at your
option) any later version.

Newcomen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> for more details.

You should have received a copy of the
L<GNU General Public License|http://www.gnu.org/licenses/gpl.html> along with Newcomen. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

####################################################################################################

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=100:
